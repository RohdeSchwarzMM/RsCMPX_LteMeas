from enum import Enum
# noinspection PyPep8Naming
from .Internal.RepeatedCapability import VALUE_DEFAULT as DefaultRepCap
# noinspection PyPep8Naming
from .Internal.RepeatedCapability import VALUE_EMPTY as EmptyRepCap


# noinspection SpellCheckingInspection
class Instance(Enum):
	"""Global Repeated capability Instance"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Inst1 = 1
	Inst2 = 2
	Inst3 = 3
	Inst4 = 4
	Inst5 = 5
	Inst6 = 6
	Inst7 = 7
	Inst8 = 8
	Inst9 = 9
	Inst10 = 10
	Inst11 = 11
	Inst12 = 12
	Inst13 = 13
	Inst14 = 14
	Inst15 = 15
	Inst16 = 16


# noinspection SpellCheckingInspection
class AbsMarker(Enum):
	"""Repeated capability AbsMarker"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2


# noinspection SpellCheckingInspection
class Area(Enum):
	"""Repeated capability Area"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5
	Nr6 = 6
	Nr7 = 7
	Nr8 = 8
	Nr9 = 9
	Nr10 = 10
	Nr11 = 11
	Nr12 = 12


# noinspection SpellCheckingInspection
class CarrierComponent(Enum):
	"""Repeated capability CarrierComponent"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4


# noinspection SpellCheckingInspection
class CarrierComponentB(Enum):
	"""Repeated capability CarrierComponentB"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4


# noinspection SpellCheckingInspection
class ChannelBw(Enum):
	"""Repeated capability ChannelBw"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Bw14 = 14
	Bw30 = 30
	Bw50 = 50
	Bw100 = 100
	Bw150 = 150
	Bw200 = 200


# noinspection SpellCheckingInspection
class DeltaMarker(Enum):
	"""Repeated capability DeltaMarker"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2


# noinspection SpellCheckingInspection
class Difference(Enum):
	"""Repeated capability Difference"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2


# noinspection SpellCheckingInspection
class EutraBand(Enum):
	"""Repeated capability EutraBand"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr30 = 30
	Nr50 = 50


# noinspection SpellCheckingInspection
class FirstChannelBw(Enum):
	"""Repeated capability FirstChannelBw"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Bw100 = 100
	Bw150 = 150
	Bw200 = 200


# noinspection SpellCheckingInspection
class Limit(Enum):
	"""Repeated capability Limit"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5
	Nr6 = 6
	Nr7 = 7
	Nr8 = 8
	Nr9 = 9
	Nr10 = 10
	Nr11 = 11
	Nr12 = 12


# noinspection SpellCheckingInspection
class MaxRange(Enum):
	"""Repeated capability MaxRange"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2


# noinspection SpellCheckingInspection
class MinRange(Enum):
	"""Repeated capability MinRange"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2


# noinspection SpellCheckingInspection
class Preamble(Enum):
	"""Repeated capability Preamble"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5
	Nr6 = 6
	Nr7 = 7
	Nr8 = 8
	Nr9 = 9
	Nr10 = 10
	Nr11 = 11
	Nr12 = 12
	Nr13 = 13
	Nr14 = 14
	Nr15 = 15
	Nr16 = 16
	Nr17 = 17
	Nr18 = 18
	Nr19 = 19
	Nr20 = 20
	Nr21 = 21
	Nr22 = 22
	Nr23 = 23
	Nr24 = 24
	Nr25 = 25
	Nr26 = 26
	Nr27 = 27
	Nr28 = 28
	Nr29 = 29
	Nr30 = 30
	Nr31 = 31
	Nr32 = 32
	Nr33 = 33
	Nr34 = 34
	Nr35 = 35
	Nr36 = 36
	Nr37 = 37
	Nr38 = 38
	Nr39 = 39
	Nr40 = 40
	Nr41 = 41
	Nr42 = 42
	Nr43 = 43
	Nr44 = 44
	Nr45 = 45
	Nr46 = 46
	Nr47 = 47
	Nr48 = 48
	Nr49 = 49
	Nr50 = 50
	Nr51 = 51
	Nr52 = 52
	Nr53 = 53
	Nr54 = 54
	Nr55 = 55
	Nr56 = 56
	Nr57 = 57
	Nr58 = 58
	Nr59 = 59
	Nr60 = 60
	Nr61 = 61
	Nr62 = 62
	Nr63 = 63
	Nr64 = 64
	Nr65 = 65
	Nr66 = 66
	Nr67 = 67
	Nr68 = 68
	Nr69 = 69
	Nr70 = 70
	Nr71 = 71
	Nr72 = 72
	Nr73 = 73
	Nr74 = 74
	Nr75 = 75
	Nr76 = 76
	Nr77 = 77
	Nr78 = 78
	Nr79 = 79
	Nr80 = 80
	Nr81 = 81
	Nr82 = 82
	Nr83 = 83
	Nr84 = 84
	Nr85 = 85
	Nr86 = 86
	Nr87 = 87
	Nr88 = 88
	Nr89 = 89
	Nr90 = 90
	Nr91 = 91
	Nr92 = 92
	Nr93 = 93
	Nr94 = 94
	Nr95 = 95
	Nr96 = 96
	Nr97 = 97
	Nr98 = 98
	Nr99 = 99
	Nr100 = 100
	Nr101 = 101
	Nr102 = 102
	Nr103 = 103
	Nr104 = 104
	Nr105 = 105
	Nr106 = 106
	Nr107 = 107
	Nr108 = 108
	Nr109 = 109
	Nr110 = 110
	Nr111 = 111
	Nr112 = 112
	Nr113 = 113
	Nr114 = 114
	Nr115 = 115
	Nr116 = 116
	Nr117 = 117
	Nr118 = 118
	Nr119 = 119
	Nr120 = 120
	Nr121 = 121
	Nr122 = 122
	Nr123 = 123
	Nr124 = 124
	Nr125 = 125
	Nr126 = 126
	Nr127 = 127
	Nr128 = 128
	Nr129 = 129
	Nr130 = 130
	Nr131 = 131
	Nr132 = 132
	Nr133 = 133
	Nr134 = 134
	Nr135 = 135
	Nr136 = 136
	Nr137 = 137
	Nr138 = 138
	Nr139 = 139
	Nr140 = 140
	Nr141 = 141
	Nr142 = 142
	Nr143 = 143
	Nr144 = 144
	Nr145 = 145
	Nr146 = 146
	Nr147 = 147
	Nr148 = 148
	Nr149 = 149
	Nr150 = 150
	Nr151 = 151
	Nr152 = 152
	Nr153 = 153
	Nr154 = 154
	Nr155 = 155
	Nr156 = 156
	Nr157 = 157
	Nr158 = 158
	Nr159 = 159
	Nr160 = 160
	Nr161 = 161
	Nr162 = 162
	Nr163 = 163
	Nr164 = 164
	Nr165 = 165
	Nr166 = 166
	Nr167 = 167
	Nr168 = 168
	Nr169 = 169
	Nr170 = 170
	Nr171 = 171
	Nr172 = 172
	Nr173 = 173
	Nr174 = 174
	Nr175 = 175
	Nr176 = 176
	Nr177 = 177
	Nr178 = 178
	Nr179 = 179
	Nr180 = 180
	Nr181 = 181
	Nr182 = 182
	Nr183 = 183
	Nr184 = 184
	Nr185 = 185
	Nr186 = 186
	Nr187 = 187
	Nr188 = 188
	Nr189 = 189
	Nr190 = 190
	Nr191 = 191
	Nr192 = 192
	Nr193 = 193
	Nr194 = 194
	Nr195 = 195
	Nr196 = 196
	Nr197 = 197
	Nr198 = 198
	Nr199 = 199
	Nr200 = 200
	Nr201 = 201
	Nr202 = 202
	Nr203 = 203
	Nr204 = 204
	Nr205 = 205
	Nr206 = 206
	Nr207 = 207
	Nr208 = 208
	Nr209 = 209
	Nr210 = 210
	Nr211 = 211
	Nr212 = 212
	Nr213 = 213
	Nr214 = 214
	Nr215 = 215
	Nr216 = 216
	Nr217 = 217
	Nr218 = 218
	Nr219 = 219
	Nr220 = 220
	Nr221 = 221
	Nr222 = 222
	Nr223 = 223
	Nr224 = 224
	Nr225 = 225
	Nr226 = 226
	Nr227 = 227
	Nr228 = 228
	Nr229 = 229
	Nr230 = 230
	Nr231 = 231
	Nr232 = 232
	Nr233 = 233
	Nr234 = 234
	Nr235 = 235
	Nr236 = 236
	Nr237 = 237
	Nr238 = 238
	Nr239 = 239
	Nr240 = 240
	Nr241 = 241
	Nr242 = 242
	Nr243 = 243
	Nr244 = 244
	Nr245 = 245
	Nr246 = 246
	Nr247 = 247
	Nr248 = 248
	Nr249 = 249
	Nr250 = 250
	Nr251 = 251
	Nr252 = 252
	Nr253 = 253
	Nr254 = 254
	Nr255 = 255
	Nr256 = 256
	Nr257 = 257
	Nr258 = 258
	Nr259 = 259
	Nr260 = 260
	Nr261 = 261
	Nr262 = 262
	Nr263 = 263
	Nr264 = 264
	Nr265 = 265
	Nr266 = 266
	Nr267 = 267
	Nr268 = 268
	Nr269 = 269
	Nr270 = 270
	Nr271 = 271
	Nr272 = 272
	Nr273 = 273
	Nr274 = 274
	Nr275 = 275
	Nr276 = 276
	Nr277 = 277
	Nr278 = 278
	Nr279 = 279
	Nr280 = 280
	Nr281 = 281
	Nr282 = 282
	Nr283 = 283
	Nr284 = 284
	Nr285 = 285
	Nr286 = 286
	Nr287 = 287
	Nr288 = 288
	Nr289 = 289
	Nr290 = 290
	Nr291 = 291
	Nr292 = 292
	Nr293 = 293
	Nr294 = 294
	Nr295 = 295
	Nr296 = 296
	Nr297 = 297
	Nr298 = 298
	Nr299 = 299
	Nr300 = 300
	Nr301 = 301
	Nr302 = 302
	Nr303 = 303
	Nr304 = 304
	Nr305 = 305
	Nr306 = 306
	Nr307 = 307
	Nr308 = 308
	Nr309 = 309
	Nr310 = 310
	Nr311 = 311
	Nr312 = 312
	Nr313 = 313
	Nr314 = 314
	Nr315 = 315
	Nr316 = 316
	Nr317 = 317
	Nr318 = 318
	Nr319 = 319
	Nr320 = 320
	Nr321 = 321
	Nr322 = 322
	Nr323 = 323
	Nr324 = 324
	Nr325 = 325
	Nr326 = 326
	Nr327 = 327
	Nr328 = 328
	Nr329 = 329
	Nr330 = 330
	Nr331 = 331
	Nr332 = 332
	Nr333 = 333
	Nr334 = 334
	Nr335 = 335
	Nr336 = 336
	Nr337 = 337
	Nr338 = 338
	Nr339 = 339
	Nr340 = 340
	Nr341 = 341
	Nr342 = 342
	Nr343 = 343
	Nr344 = 344
	Nr345 = 345
	Nr346 = 346
	Nr347 = 347
	Nr348 = 348
	Nr349 = 349
	Nr350 = 350
	Nr351 = 351
	Nr352 = 352
	Nr353 = 353
	Nr354 = 354
	Nr355 = 355
	Nr356 = 356
	Nr357 = 357
	Nr358 = 358
	Nr359 = 359
	Nr360 = 360
	Nr361 = 361
	Nr362 = 362
	Nr363 = 363
	Nr364 = 364
	Nr365 = 365
	Nr366 = 366
	Nr367 = 367
	Nr368 = 368
	Nr369 = 369
	Nr370 = 370
	Nr371 = 371
	Nr372 = 372
	Nr373 = 373
	Nr374 = 374
	Nr375 = 375
	Nr376 = 376
	Nr377 = 377
	Nr378 = 378
	Nr379 = 379
	Nr380 = 380
	Nr381 = 381
	Nr382 = 382
	Nr383 = 383
	Nr384 = 384
	Nr385 = 385
	Nr386 = 386
	Nr387 = 387
	Nr388 = 388
	Nr389 = 389
	Nr390 = 390
	Nr391 = 391
	Nr392 = 392
	Nr393 = 393
	Nr394 = 394
	Nr395 = 395
	Nr396 = 396
	Nr397 = 397
	Nr398 = 398
	Nr399 = 399
	Nr400 = 400


# noinspection SpellCheckingInspection
class PreambleFormat(Enum):
	"""Repeated capability PreambleFormat"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Fmt1 = 1
	Fmt2 = 2
	Fmt3 = 3
	Fmt4 = 4
	Fmt5 = 5


# noinspection SpellCheckingInspection
class QAMmodOrder(Enum):
	"""Repeated capability QAMmodOrder"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Qam16 = 16
	Qam64 = 64
	Qam256 = 256


# noinspection SpellCheckingInspection
class RBcount(Enum):
	"""Repeated capability RBcount"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2


# noinspection SpellCheckingInspection
class RBoffset(Enum):
	"""Repeated capability RBoffset"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2


# noinspection SpellCheckingInspection
class RBWkHz(Enum):
	"""Repeated capability RBWkHz"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Rbw30 = 30
	Rbw50 = 50
	Rbw100 = 100
	Rbw150 = 150
	Rbw200 = 200
	Rbw1000 = 1000


# noinspection SpellCheckingInspection
class Ripple(Enum):
	"""Repeated capability Ripple"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2


# noinspection SpellCheckingInspection
class SecondaryCC(Enum):
	"""Repeated capability SecondaryCC"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	CC1 = 1
	CC2 = 2
	CC3 = 3
	CC4 = 4
	CC5 = 5
	CC6 = 6
	CC7 = 7


# noinspection SpellCheckingInspection
class SecondChannelBw(Enum):
	"""Repeated capability SecondChannelBw"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Bw50 = 50
	Bw100 = 100
	Bw150 = 150
	Bw200 = 200


# noinspection SpellCheckingInspection
class Segment(Enum):
	"""Repeated capability Segment"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5
	Nr6 = 6
	Nr7 = 7
	Nr8 = 8
	Nr9 = 9
	Nr10 = 10
	Nr11 = 11
	Nr12 = 12
	Nr13 = 13
	Nr14 = 14
	Nr15 = 15
	Nr16 = 16
	Nr17 = 17
	Nr18 = 18
	Nr19 = 19
	Nr20 = 20
	Nr21 = 21
	Nr22 = 22
	Nr23 = 23
	Nr24 = 24
	Nr25 = 25
	Nr26 = 26
	Nr27 = 27
	Nr28 = 28
	Nr29 = 29
	Nr30 = 30
	Nr31 = 31
	Nr32 = 32
	Nr33 = 33
	Nr34 = 34
	Nr35 = 35
	Nr36 = 36
	Nr37 = 37
	Nr38 = 38
	Nr39 = 39
	Nr40 = 40
	Nr41 = 41
	Nr42 = 42
	Nr43 = 43
	Nr44 = 44
	Nr45 = 45
	Nr46 = 46
	Nr47 = 47
	Nr48 = 48
	Nr49 = 49
	Nr50 = 50
	Nr51 = 51
	Nr52 = 52
	Nr53 = 53
	Nr54 = 54
	Nr55 = 55
	Nr56 = 56
	Nr57 = 57
	Nr58 = 58
	Nr59 = 59
	Nr60 = 60
	Nr61 = 61
	Nr62 = 62
	Nr63 = 63
	Nr64 = 64
	Nr65 = 65
	Nr66 = 66
	Nr67 = 67
	Nr68 = 68
	Nr69 = 69
	Nr70 = 70
	Nr71 = 71
	Nr72 = 72
	Nr73 = 73
	Nr74 = 74
	Nr75 = 75
	Nr76 = 76
	Nr77 = 77
	Nr78 = 78
	Nr79 = 79
	Nr80 = 80
	Nr81 = 81
	Nr82 = 82
	Nr83 = 83
	Nr84 = 84
	Nr85 = 85
	Nr86 = 86
	Nr87 = 87
	Nr88 = 88
	Nr89 = 89
	Nr90 = 90
	Nr91 = 91
	Nr92 = 92
	Nr93 = 93
	Nr94 = 94
	Nr95 = 95
	Nr96 = 96
	Nr97 = 97
	Nr98 = 98
	Nr99 = 99
	Nr100 = 100
	Nr101 = 101
	Nr102 = 102
	Nr103 = 103
	Nr104 = 104
	Nr105 = 105
	Nr106 = 106
	Nr107 = 107
	Nr108 = 108
	Nr109 = 109
	Nr110 = 110
	Nr111 = 111
	Nr112 = 112
	Nr113 = 113
	Nr114 = 114
	Nr115 = 115
	Nr116 = 116
	Nr117 = 117
	Nr118 = 118
	Nr119 = 119
	Nr120 = 120
	Nr121 = 121
	Nr122 = 122
	Nr123 = 123
	Nr124 = 124
	Nr125 = 125
	Nr126 = 126
	Nr127 = 127
	Nr128 = 128
	Nr129 = 129
	Nr130 = 130
	Nr131 = 131
	Nr132 = 132
	Nr133 = 133
	Nr134 = 134
	Nr135 = 135
	Nr136 = 136
	Nr137 = 137
	Nr138 = 138
	Nr139 = 139
	Nr140 = 140
	Nr141 = 141
	Nr142 = 142
	Nr143 = 143
	Nr144 = 144
	Nr145 = 145
	Nr146 = 146
	Nr147 = 147
	Nr148 = 148
	Nr149 = 149
	Nr150 = 150
	Nr151 = 151
	Nr152 = 152
	Nr153 = 153
	Nr154 = 154
	Nr155 = 155
	Nr156 = 156
	Nr157 = 157
	Nr158 = 158
	Nr159 = 159
	Nr160 = 160
	Nr161 = 161
	Nr162 = 162
	Nr163 = 163
	Nr164 = 164
	Nr165 = 165
	Nr166 = 166
	Nr167 = 167
	Nr168 = 168
	Nr169 = 169
	Nr170 = 170
	Nr171 = 171
	Nr172 = 172
	Nr173 = 173
	Nr174 = 174
	Nr175 = 175
	Nr176 = 176
	Nr177 = 177
	Nr178 = 178
	Nr179 = 179
	Nr180 = 180
	Nr181 = 181
	Nr182 = 182
	Nr183 = 183
	Nr184 = 184
	Nr185 = 185
	Nr186 = 186
	Nr187 = 187
	Nr188 = 188
	Nr189 = 189
	Nr190 = 190
	Nr191 = 191
	Nr192 = 192
	Nr193 = 193
	Nr194 = 194
	Nr195 = 195
	Nr196 = 196
	Nr197 = 197
	Nr198 = 198
	Nr199 = 199
	Nr200 = 200
	Nr201 = 201
	Nr202 = 202
	Nr203 = 203
	Nr204 = 204
	Nr205 = 205
	Nr206 = 206
	Nr207 = 207
	Nr208 = 208
	Nr209 = 209
	Nr210 = 210
	Nr211 = 211
	Nr212 = 212
	Nr213 = 213
	Nr214 = 214
	Nr215 = 215
	Nr216 = 216
	Nr217 = 217
	Nr218 = 218
	Nr219 = 219
	Nr220 = 220
	Nr221 = 221
	Nr222 = 222
	Nr223 = 223
	Nr224 = 224
	Nr225 = 225
	Nr226 = 226
	Nr227 = 227
	Nr228 = 228
	Nr229 = 229
	Nr230 = 230
	Nr231 = 231
	Nr232 = 232
	Nr233 = 233
	Nr234 = 234
	Nr235 = 235
	Nr236 = 236
	Nr237 = 237
	Nr238 = 238
	Nr239 = 239
	Nr240 = 240
	Nr241 = 241
	Nr242 = 242
	Nr243 = 243
	Nr244 = 244
	Nr245 = 245
	Nr246 = 246
	Nr247 = 247
	Nr248 = 248
	Nr249 = 249
	Nr250 = 250
	Nr251 = 251
	Nr252 = 252
	Nr253 = 253
	Nr254 = 254
	Nr255 = 255
	Nr256 = 256
	Nr257 = 257
	Nr258 = 258
	Nr259 = 259
	Nr260 = 260
	Nr261 = 261
	Nr262 = 262
	Nr263 = 263
	Nr264 = 264
	Nr265 = 265
	Nr266 = 266
	Nr267 = 267
	Nr268 = 268
	Nr269 = 269
	Nr270 = 270
	Nr271 = 271
	Nr272 = 272
	Nr273 = 273
	Nr274 = 274
	Nr275 = 275
	Nr276 = 276
	Nr277 = 277
	Nr278 = 278
	Nr279 = 279
	Nr280 = 280
	Nr281 = 281
	Nr282 = 282
	Nr283 = 283
	Nr284 = 284
	Nr285 = 285
	Nr286 = 286
	Nr287 = 287
	Nr288 = 288
	Nr289 = 289
	Nr290 = 290
	Nr291 = 291
	Nr292 = 292
	Nr293 = 293
	Nr294 = 294
	Nr295 = 295
	Nr296 = 296
	Nr297 = 297
	Nr298 = 298
	Nr299 = 299
	Nr300 = 300
	Nr301 = 301
	Nr302 = 302
	Nr303 = 303
	Nr304 = 304
	Nr305 = 305
	Nr306 = 306
	Nr307 = 307
	Nr308 = 308
	Nr309 = 309
	Nr310 = 310
	Nr311 = 311
	Nr312 = 312
	Nr313 = 313
	Nr314 = 314
	Nr315 = 315
	Nr316 = 316
	Nr317 = 317
	Nr318 = 318
	Nr319 = 319
	Nr320 = 320
	Nr321 = 321
	Nr322 = 322
	Nr323 = 323
	Nr324 = 324
	Nr325 = 325
	Nr326 = 326
	Nr327 = 327
	Nr328 = 328
	Nr329 = 329
	Nr330 = 330
	Nr331 = 331
	Nr332 = 332
	Nr333 = 333
	Nr334 = 334
	Nr335 = 335
	Nr336 = 336
	Nr337 = 337
	Nr338 = 338
	Nr339 = 339
	Nr340 = 340
	Nr341 = 341
	Nr342 = 342
	Nr343 = 343
	Nr344 = 344
	Nr345 = 345
	Nr346 = 346
	Nr347 = 347
	Nr348 = 348
	Nr349 = 349
	Nr350 = 350
	Nr351 = 351
	Nr352 = 352
	Nr353 = 353
	Nr354 = 354
	Nr355 = 355
	Nr356 = 356
	Nr357 = 357
	Nr358 = 358
	Nr359 = 359
	Nr360 = 360
	Nr361 = 361
	Nr362 = 362
	Nr363 = 363
	Nr364 = 364
	Nr365 = 365
	Nr366 = 366
	Nr367 = 367
	Nr368 = 368
	Nr369 = 369
	Nr370 = 370
	Nr371 = 371
	Nr372 = 372
	Nr373 = 373
	Nr374 = 374
	Nr375 = 375
	Nr376 = 376
	Nr377 = 377
	Nr378 = 378
	Nr379 = 379
	Nr380 = 380
	Nr381 = 381
	Nr382 = 382
	Nr383 = 383
	Nr384 = 384
	Nr385 = 385
	Nr386 = 386
	Nr387 = 387
	Nr388 = 388
	Nr389 = 389
	Nr390 = 390
	Nr391 = 391
	Nr392 = 392
	Nr393 = 393
	Nr394 = 394
	Nr395 = 395
	Nr396 = 396
	Nr397 = 397
	Nr398 = 398
	Nr399 = 399
	Nr400 = 400
	Nr401 = 401
	Nr402 = 402
	Nr403 = 403
	Nr404 = 404
	Nr405 = 405
	Nr406 = 406
	Nr407 = 407
	Nr408 = 408
	Nr409 = 409
	Nr410 = 410
	Nr411 = 411
	Nr412 = 412
	Nr413 = 413
	Nr414 = 414
	Nr415 = 415
	Nr416 = 416
	Nr417 = 417
	Nr418 = 418
	Nr419 = 419
	Nr420 = 420
	Nr421 = 421
	Nr422 = 422
	Nr423 = 423
	Nr424 = 424
	Nr425 = 425
	Nr426 = 426
	Nr427 = 427
	Nr428 = 428
	Nr429 = 429
	Nr430 = 430
	Nr431 = 431
	Nr432 = 432
	Nr433 = 433
	Nr434 = 434
	Nr435 = 435
	Nr436 = 436
	Nr437 = 437
	Nr438 = 438
	Nr439 = 439
	Nr440 = 440
	Nr441 = 441
	Nr442 = 442
	Nr443 = 443
	Nr444 = 444
	Nr445 = 445
	Nr446 = 446
	Nr447 = 447
	Nr448 = 448
	Nr449 = 449
	Nr450 = 450
	Nr451 = 451
	Nr452 = 452
	Nr453 = 453
	Nr454 = 454
	Nr455 = 455
	Nr456 = 456
	Nr457 = 457
	Nr458 = 458
	Nr459 = 459
	Nr460 = 460
	Nr461 = 461
	Nr462 = 462
	Nr463 = 463
	Nr464 = 464
	Nr465 = 465
	Nr466 = 466
	Nr467 = 467
	Nr468 = 468
	Nr469 = 469
	Nr470 = 470
	Nr471 = 471
	Nr472 = 472
	Nr473 = 473
	Nr474 = 474
	Nr475 = 475
	Nr476 = 476
	Nr477 = 477
	Nr478 = 478
	Nr479 = 479
	Nr480 = 480
	Nr481 = 481
	Nr482 = 482
	Nr483 = 483
	Nr484 = 484
	Nr485 = 485
	Nr486 = 486
	Nr487 = 487
	Nr488 = 488
	Nr489 = 489
	Nr490 = 490
	Nr491 = 491
	Nr492 = 492
	Nr493 = 493
	Nr494 = 494
	Nr495 = 495
	Nr496 = 496
	Nr497 = 497
	Nr498 = 498
	Nr499 = 499
	Nr500 = 500
	Nr501 = 501
	Nr502 = 502
	Nr503 = 503
	Nr504 = 504
	Nr505 = 505
	Nr506 = 506
	Nr507 = 507
	Nr508 = 508
	Nr509 = 509
	Nr510 = 510
	Nr511 = 511
	Nr512 = 512
	Nr513 = 513
	Nr514 = 514
	Nr515 = 515
	Nr516 = 516
	Nr517 = 517
	Nr518 = 518
	Nr519 = 519
	Nr520 = 520
	Nr521 = 521
	Nr522 = 522
	Nr523 = 523
	Nr524 = 524
	Nr525 = 525
	Nr526 = 526
	Nr527 = 527
	Nr528 = 528
	Nr529 = 529
	Nr530 = 530
	Nr531 = 531
	Nr532 = 532
	Nr533 = 533
	Nr534 = 534
	Nr535 = 535
	Nr536 = 536
	Nr537 = 537
	Nr538 = 538
	Nr539 = 539
	Nr540 = 540
	Nr541 = 541
	Nr542 = 542
	Nr543 = 543
	Nr544 = 544
	Nr545 = 545
	Nr546 = 546
	Nr547 = 547
	Nr548 = 548
	Nr549 = 549
	Nr550 = 550
	Nr551 = 551
	Nr552 = 552
	Nr553 = 553
	Nr554 = 554
	Nr555 = 555
	Nr556 = 556
	Nr557 = 557
	Nr558 = 558
	Nr559 = 559
	Nr560 = 560
	Nr561 = 561
	Nr562 = 562
	Nr563 = 563
	Nr564 = 564
	Nr565 = 565
	Nr566 = 566
	Nr567 = 567
	Nr568 = 568
	Nr569 = 569
	Nr570 = 570
	Nr571 = 571
	Nr572 = 572
	Nr573 = 573
	Nr574 = 574
	Nr575 = 575
	Nr576 = 576
	Nr577 = 577
	Nr578 = 578
	Nr579 = 579
	Nr580 = 580
	Nr581 = 581
	Nr582 = 582
	Nr583 = 583
	Nr584 = 584
	Nr585 = 585
	Nr586 = 586
	Nr587 = 587
	Nr588 = 588
	Nr589 = 589
	Nr590 = 590
	Nr591 = 591
	Nr592 = 592
	Nr593 = 593
	Nr594 = 594
	Nr595 = 595
	Nr596 = 596
	Nr597 = 597
	Nr598 = 598
	Nr599 = 599
	Nr600 = 600
	Nr601 = 601
	Nr602 = 602
	Nr603 = 603
	Nr604 = 604
	Nr605 = 605
	Nr606 = 606
	Nr607 = 607
	Nr608 = 608
	Nr609 = 609
	Nr610 = 610
	Nr611 = 611
	Nr612 = 612
	Nr613 = 613
	Nr614 = 614
	Nr615 = 615
	Nr616 = 616
	Nr617 = 617
	Nr618 = 618
	Nr619 = 619
	Nr620 = 620
	Nr621 = 621
	Nr622 = 622
	Nr623 = 623
	Nr624 = 624
	Nr625 = 625
	Nr626 = 626
	Nr627 = 627
	Nr628 = 628
	Nr629 = 629
	Nr630 = 630
	Nr631 = 631
	Nr632 = 632
	Nr633 = 633
	Nr634 = 634
	Nr635 = 635
	Nr636 = 636
	Nr637 = 637
	Nr638 = 638
	Nr639 = 639
	Nr640 = 640
	Nr641 = 641
	Nr642 = 642
	Nr643 = 643
	Nr644 = 644
	Nr645 = 645
	Nr646 = 646
	Nr647 = 647
	Nr648 = 648
	Nr649 = 649
	Nr650 = 650
	Nr651 = 651
	Nr652 = 652
	Nr653 = 653
	Nr654 = 654
	Nr655 = 655
	Nr656 = 656
	Nr657 = 657
	Nr658 = 658
	Nr659 = 659
	Nr660 = 660
	Nr661 = 661
	Nr662 = 662
	Nr663 = 663
	Nr664 = 664
	Nr665 = 665
	Nr666 = 666
	Nr667 = 667
	Nr668 = 668
	Nr669 = 669
	Nr670 = 670
	Nr671 = 671
	Nr672 = 672
	Nr673 = 673
	Nr674 = 674
	Nr675 = 675
	Nr676 = 676
	Nr677 = 677
	Nr678 = 678
	Nr679 = 679
	Nr680 = 680
	Nr681 = 681
	Nr682 = 682
	Nr683 = 683
	Nr684 = 684
	Nr685 = 685
	Nr686 = 686
	Nr687 = 687
	Nr688 = 688
	Nr689 = 689
	Nr690 = 690
	Nr691 = 691
	Nr692 = 692
	Nr693 = 693
	Nr694 = 694
	Nr695 = 695
	Nr696 = 696
	Nr697 = 697
	Nr698 = 698
	Nr699 = 699
	Nr700 = 700
	Nr701 = 701
	Nr702 = 702
	Nr703 = 703
	Nr704 = 704
	Nr705 = 705
	Nr706 = 706
	Nr707 = 707
	Nr708 = 708
	Nr709 = 709
	Nr710 = 710
	Nr711 = 711
	Nr712 = 712
	Nr713 = 713
	Nr714 = 714
	Nr715 = 715
	Nr716 = 716
	Nr717 = 717
	Nr718 = 718
	Nr719 = 719
	Nr720 = 720
	Nr721 = 721
	Nr722 = 722
	Nr723 = 723
	Nr724 = 724
	Nr725 = 725
	Nr726 = 726
	Nr727 = 727
	Nr728 = 728
	Nr729 = 729
	Nr730 = 730
	Nr731 = 731
	Nr732 = 732
	Nr733 = 733
	Nr734 = 734
	Nr735 = 735
	Nr736 = 736
	Nr737 = 737
	Nr738 = 738
	Nr739 = 739
	Nr740 = 740
	Nr741 = 741
	Nr742 = 742
	Nr743 = 743
	Nr744 = 744
	Nr745 = 745
	Nr746 = 746
	Nr747 = 747
	Nr748 = 748
	Nr749 = 749
	Nr750 = 750
	Nr751 = 751
	Nr752 = 752
	Nr753 = 753
	Nr754 = 754
	Nr755 = 755
	Nr756 = 756
	Nr757 = 757
	Nr758 = 758
	Nr759 = 759
	Nr760 = 760
	Nr761 = 761
	Nr762 = 762
	Nr763 = 763
	Nr764 = 764
	Nr765 = 765
	Nr766 = 766
	Nr767 = 767
	Nr768 = 768
	Nr769 = 769
	Nr770 = 770
	Nr771 = 771
	Nr772 = 772
	Nr773 = 773
	Nr774 = 774
	Nr775 = 775
	Nr776 = 776
	Nr777 = 777
	Nr778 = 778
	Nr779 = 779
	Nr780 = 780
	Nr781 = 781
	Nr782 = 782
	Nr783 = 783
	Nr784 = 784
	Nr785 = 785
	Nr786 = 786
	Nr787 = 787
	Nr788 = 788
	Nr789 = 789
	Nr790 = 790
	Nr791 = 791
	Nr792 = 792
	Nr793 = 793
	Nr794 = 794
	Nr795 = 795
	Nr796 = 796
	Nr797 = 797
	Nr798 = 798
	Nr799 = 799
	Nr800 = 800
	Nr801 = 801
	Nr802 = 802
	Nr803 = 803
	Nr804 = 804
	Nr805 = 805
	Nr806 = 806
	Nr807 = 807
	Nr808 = 808
	Nr809 = 809
	Nr810 = 810
	Nr811 = 811
	Nr812 = 812
	Nr813 = 813
	Nr814 = 814
	Nr815 = 815
	Nr816 = 816
	Nr817 = 817
	Nr818 = 818
	Nr819 = 819
	Nr820 = 820
	Nr821 = 821
	Nr822 = 822
	Nr823 = 823
	Nr824 = 824
	Nr825 = 825
	Nr826 = 826
	Nr827 = 827
	Nr828 = 828
	Nr829 = 829
	Nr830 = 830
	Nr831 = 831
	Nr832 = 832
	Nr833 = 833
	Nr834 = 834
	Nr835 = 835
	Nr836 = 836
	Nr837 = 837
	Nr838 = 838
	Nr839 = 839
	Nr840 = 840
	Nr841 = 841
	Nr842 = 842
	Nr843 = 843
	Nr844 = 844
	Nr845 = 845
	Nr846 = 846
	Nr847 = 847
	Nr848 = 848
	Nr849 = 849
	Nr850 = 850
	Nr851 = 851
	Nr852 = 852
	Nr853 = 853
	Nr854 = 854
	Nr855 = 855
	Nr856 = 856
	Nr857 = 857
	Nr858 = 858
	Nr859 = 859
	Nr860 = 860
	Nr861 = 861
	Nr862 = 862
	Nr863 = 863
	Nr864 = 864
	Nr865 = 865
	Nr866 = 866
	Nr867 = 867
	Nr868 = 868
	Nr869 = 869
	Nr870 = 870
	Nr871 = 871
	Nr872 = 872
	Nr873 = 873
	Nr874 = 874
	Nr875 = 875
	Nr876 = 876
	Nr877 = 877
	Nr878 = 878
	Nr879 = 879
	Nr880 = 880
	Nr881 = 881
	Nr882 = 882
	Nr883 = 883
	Nr884 = 884
	Nr885 = 885
	Nr886 = 886
	Nr887 = 887
	Nr888 = 888
	Nr889 = 889
	Nr890 = 890
	Nr891 = 891
	Nr892 = 892
	Nr893 = 893
	Nr894 = 894
	Nr895 = 895
	Nr896 = 896
	Nr897 = 897
	Nr898 = 898
	Nr899 = 899
	Nr900 = 900
	Nr901 = 901
	Nr902 = 902
	Nr903 = 903
	Nr904 = 904
	Nr905 = 905
	Nr906 = 906
	Nr907 = 907
	Nr908 = 908
	Nr909 = 909
	Nr910 = 910
	Nr911 = 911
	Nr912 = 912
	Nr913 = 913
	Nr914 = 914
	Nr915 = 915
	Nr916 = 916
	Nr917 = 917
	Nr918 = 918
	Nr919 = 919
	Nr920 = 920
	Nr921 = 921
	Nr922 = 922
	Nr923 = 923
	Nr924 = 924
	Nr925 = 925
	Nr926 = 926
	Nr927 = 927
	Nr928 = 928
	Nr929 = 929
	Nr930 = 930
	Nr931 = 931
	Nr932 = 932
	Nr933 = 933
	Nr934 = 934
	Nr935 = 935
	Nr936 = 936
	Nr937 = 937
	Nr938 = 938
	Nr939 = 939
	Nr940 = 940
	Nr941 = 941
	Nr942 = 942
	Nr943 = 943
	Nr944 = 944
	Nr945 = 945
	Nr946 = 946
	Nr947 = 947
	Nr948 = 948
	Nr949 = 949
	Nr950 = 950
	Nr951 = 951
	Nr952 = 952
	Nr953 = 953
	Nr954 = 954
	Nr955 = 955
	Nr956 = 956
	Nr957 = 957
	Nr958 = 958
	Nr959 = 959
	Nr960 = 960
	Nr961 = 961
	Nr962 = 962
	Nr963 = 963
	Nr964 = 964
	Nr965 = 965
	Nr966 = 966
	Nr967 = 967
	Nr968 = 968
	Nr969 = 969
	Nr970 = 970
	Nr971 = 971
	Nr972 = 972
	Nr973 = 973
	Nr974 = 974
	Nr975 = 975
	Nr976 = 976
	Nr977 = 977
	Nr978 = 978
	Nr979 = 979
	Nr980 = 980
	Nr981 = 981
	Nr982 = 982
	Nr983 = 983
	Nr984 = 984
	Nr985 = 985
	Nr986 = 986
	Nr987 = 987
	Nr988 = 988
	Nr989 = 989
	Nr990 = 990
	Nr991 = 991
	Nr992 = 992
	Nr993 = 993
	Nr994 = 994
	Nr995 = 995
	Nr996 = 996
	Nr997 = 997
	Nr998 = 998
	Nr999 = 999
	Nr1000 = 1000
	Nr1001 = 1001
	Nr1002 = 1002
	Nr1003 = 1003
	Nr1004 = 1004
	Nr1005 = 1005
	Nr1006 = 1006
	Nr1007 = 1007
	Nr1008 = 1008
	Nr1009 = 1009
	Nr1010 = 1010
	Nr1011 = 1011
	Nr1012 = 1012
	Nr1013 = 1013
	Nr1014 = 1014
	Nr1015 = 1015
	Nr1016 = 1016
	Nr1017 = 1017
	Nr1018 = 1018
	Nr1019 = 1019
	Nr1020 = 1020
	Nr1021 = 1021
	Nr1022 = 1022
	Nr1023 = 1023
	Nr1024 = 1024
	Nr1025 = 1025
	Nr1026 = 1026
	Nr1027 = 1027
	Nr1028 = 1028
	Nr1029 = 1029
	Nr1030 = 1030
	Nr1031 = 1031
	Nr1032 = 1032
	Nr1033 = 1033
	Nr1034 = 1034
	Nr1035 = 1035
	Nr1036 = 1036
	Nr1037 = 1037
	Nr1038 = 1038
	Nr1039 = 1039
	Nr1040 = 1040
	Nr1041 = 1041
	Nr1042 = 1042
	Nr1043 = 1043
	Nr1044 = 1044
	Nr1045 = 1045
	Nr1046 = 1046
	Nr1047 = 1047
	Nr1048 = 1048
	Nr1049 = 1049
	Nr1050 = 1050
	Nr1051 = 1051
	Nr1052 = 1052
	Nr1053 = 1053
	Nr1054 = 1054
	Nr1055 = 1055
	Nr1056 = 1056
	Nr1057 = 1057
	Nr1058 = 1058
	Nr1059 = 1059
	Nr1060 = 1060
	Nr1061 = 1061
	Nr1062 = 1062
	Nr1063 = 1063
	Nr1064 = 1064
	Nr1065 = 1065
	Nr1066 = 1066
	Nr1067 = 1067
	Nr1068 = 1068
	Nr1069 = 1069
	Nr1070 = 1070
	Nr1071 = 1071
	Nr1072 = 1072
	Nr1073 = 1073
	Nr1074 = 1074
	Nr1075 = 1075
	Nr1076 = 1076
	Nr1077 = 1077
	Nr1078 = 1078
	Nr1079 = 1079
	Nr1080 = 1080
	Nr1081 = 1081
	Nr1082 = 1082
	Nr1083 = 1083
	Nr1084 = 1084
	Nr1085 = 1085
	Nr1086 = 1086
	Nr1087 = 1087
	Nr1088 = 1088
	Nr1089 = 1089
	Nr1090 = 1090
	Nr1091 = 1091
	Nr1092 = 1092
	Nr1093 = 1093
	Nr1094 = 1094
	Nr1095 = 1095
	Nr1096 = 1096
	Nr1097 = 1097
	Nr1098 = 1098
	Nr1099 = 1099
	Nr1100 = 1100
	Nr1101 = 1101
	Nr1102 = 1102
	Nr1103 = 1103
	Nr1104 = 1104
	Nr1105 = 1105
	Nr1106 = 1106
	Nr1107 = 1107
	Nr1108 = 1108
	Nr1109 = 1109
	Nr1110 = 1110
	Nr1111 = 1111
	Nr1112 = 1112
	Nr1113 = 1113
	Nr1114 = 1114
	Nr1115 = 1115
	Nr1116 = 1116
	Nr1117 = 1117
	Nr1118 = 1118
	Nr1119 = 1119
	Nr1120 = 1120
	Nr1121 = 1121
	Nr1122 = 1122
	Nr1123 = 1123
	Nr1124 = 1124
	Nr1125 = 1125
	Nr1126 = 1126
	Nr1127 = 1127
	Nr1128 = 1128
	Nr1129 = 1129
	Nr1130 = 1130
	Nr1131 = 1131
	Nr1132 = 1132
	Nr1133 = 1133
	Nr1134 = 1134
	Nr1135 = 1135
	Nr1136 = 1136
	Nr1137 = 1137
	Nr1138 = 1138
	Nr1139 = 1139
	Nr1140 = 1140
	Nr1141 = 1141
	Nr1142 = 1142
	Nr1143 = 1143
	Nr1144 = 1144
	Nr1145 = 1145
	Nr1146 = 1146
	Nr1147 = 1147
	Nr1148 = 1148
	Nr1149 = 1149
	Nr1150 = 1150
	Nr1151 = 1151
	Nr1152 = 1152
	Nr1153 = 1153
	Nr1154 = 1154
	Nr1155 = 1155
	Nr1156 = 1156
	Nr1157 = 1157
	Nr1158 = 1158
	Nr1159 = 1159
	Nr1160 = 1160
	Nr1161 = 1161
	Nr1162 = 1162
	Nr1163 = 1163
	Nr1164 = 1164
	Nr1165 = 1165
	Nr1166 = 1166
	Nr1167 = 1167
	Nr1168 = 1168
	Nr1169 = 1169
	Nr1170 = 1170
	Nr1171 = 1171
	Nr1172 = 1172
	Nr1173 = 1173
	Nr1174 = 1174
	Nr1175 = 1175
	Nr1176 = 1176
	Nr1177 = 1177
	Nr1178 = 1178
	Nr1179 = 1179
	Nr1180 = 1180
	Nr1181 = 1181
	Nr1182 = 1182
	Nr1183 = 1183
	Nr1184 = 1184
	Nr1185 = 1185
	Nr1186 = 1186
	Nr1187 = 1187
	Nr1188 = 1188
	Nr1189 = 1189
	Nr1190 = 1190
	Nr1191 = 1191
	Nr1192 = 1192
	Nr1193 = 1193
	Nr1194 = 1194
	Nr1195 = 1195
	Nr1196 = 1196
	Nr1197 = 1197
	Nr1198 = 1198
	Nr1199 = 1199
	Nr1200 = 1200
	Nr1201 = 1201
	Nr1202 = 1202
	Nr1203 = 1203
	Nr1204 = 1204
	Nr1205 = 1205
	Nr1206 = 1206
	Nr1207 = 1207
	Nr1208 = 1208
	Nr1209 = 1209
	Nr1210 = 1210
	Nr1211 = 1211
	Nr1212 = 1212
	Nr1213 = 1213
	Nr1214 = 1214
	Nr1215 = 1215
	Nr1216 = 1216
	Nr1217 = 1217
	Nr1218 = 1218
	Nr1219 = 1219
	Nr1220 = 1220
	Nr1221 = 1221
	Nr1222 = 1222
	Nr1223 = 1223
	Nr1224 = 1224
	Nr1225 = 1225
	Nr1226 = 1226
	Nr1227 = 1227
	Nr1228 = 1228
	Nr1229 = 1229
	Nr1230 = 1230
	Nr1231 = 1231
	Nr1232 = 1232
	Nr1233 = 1233
	Nr1234 = 1234
	Nr1235 = 1235
	Nr1236 = 1236
	Nr1237 = 1237
	Nr1238 = 1238
	Nr1239 = 1239
	Nr1240 = 1240
	Nr1241 = 1241
	Nr1242 = 1242
	Nr1243 = 1243
	Nr1244 = 1244
	Nr1245 = 1245
	Nr1246 = 1246
	Nr1247 = 1247
	Nr1248 = 1248
	Nr1249 = 1249
	Nr1250 = 1250
	Nr1251 = 1251
	Nr1252 = 1252
	Nr1253 = 1253
	Nr1254 = 1254
	Nr1255 = 1255
	Nr1256 = 1256
	Nr1257 = 1257
	Nr1258 = 1258
	Nr1259 = 1259
	Nr1260 = 1260
	Nr1261 = 1261
	Nr1262 = 1262
	Nr1263 = 1263
	Nr1264 = 1264
	Nr1265 = 1265
	Nr1266 = 1266
	Nr1267 = 1267
	Nr1268 = 1268
	Nr1269 = 1269
	Nr1270 = 1270
	Nr1271 = 1271
	Nr1272 = 1272
	Nr1273 = 1273
	Nr1274 = 1274
	Nr1275 = 1275
	Nr1276 = 1276
	Nr1277 = 1277
	Nr1278 = 1278
	Nr1279 = 1279
	Nr1280 = 1280
	Nr1281 = 1281
	Nr1282 = 1282
	Nr1283 = 1283
	Nr1284 = 1284
	Nr1285 = 1285
	Nr1286 = 1286
	Nr1287 = 1287
	Nr1288 = 1288
	Nr1289 = 1289
	Nr1290 = 1290
	Nr1291 = 1291
	Nr1292 = 1292
	Nr1293 = 1293
	Nr1294 = 1294
	Nr1295 = 1295
	Nr1296 = 1296
	Nr1297 = 1297
	Nr1298 = 1298
	Nr1299 = 1299
	Nr1300 = 1300
	Nr1301 = 1301
	Nr1302 = 1302
	Nr1303 = 1303
	Nr1304 = 1304
	Nr1305 = 1305
	Nr1306 = 1306
	Nr1307 = 1307
	Nr1308 = 1308
	Nr1309 = 1309
	Nr1310 = 1310
	Nr1311 = 1311
	Nr1312 = 1312
	Nr1313 = 1313
	Nr1314 = 1314
	Nr1315 = 1315
	Nr1316 = 1316
	Nr1317 = 1317
	Nr1318 = 1318
	Nr1319 = 1319
	Nr1320 = 1320
	Nr1321 = 1321
	Nr1322 = 1322
	Nr1323 = 1323
	Nr1324 = 1324
	Nr1325 = 1325
	Nr1326 = 1326
	Nr1327 = 1327
	Nr1328 = 1328
	Nr1329 = 1329
	Nr1330 = 1330
	Nr1331 = 1331
	Nr1332 = 1332
	Nr1333 = 1333
	Nr1334 = 1334
	Nr1335 = 1335
	Nr1336 = 1336
	Nr1337 = 1337
	Nr1338 = 1338
	Nr1339 = 1339
	Nr1340 = 1340
	Nr1341 = 1341
	Nr1342 = 1342
	Nr1343 = 1343
	Nr1344 = 1344
	Nr1345 = 1345
	Nr1346 = 1346
	Nr1347 = 1347
	Nr1348 = 1348
	Nr1349 = 1349
	Nr1350 = 1350
	Nr1351 = 1351
	Nr1352 = 1352
	Nr1353 = 1353
	Nr1354 = 1354
	Nr1355 = 1355
	Nr1356 = 1356
	Nr1357 = 1357
	Nr1358 = 1358
	Nr1359 = 1359
	Nr1360 = 1360
	Nr1361 = 1361
	Nr1362 = 1362
	Nr1363 = 1363
	Nr1364 = 1364
	Nr1365 = 1365
	Nr1366 = 1366
	Nr1367 = 1367
	Nr1368 = 1368
	Nr1369 = 1369
	Nr1370 = 1370
	Nr1371 = 1371
	Nr1372 = 1372
	Nr1373 = 1373
	Nr1374 = 1374
	Nr1375 = 1375
	Nr1376 = 1376
	Nr1377 = 1377
	Nr1378 = 1378
	Nr1379 = 1379
	Nr1380 = 1380
	Nr1381 = 1381
	Nr1382 = 1382
	Nr1383 = 1383
	Nr1384 = 1384
	Nr1385 = 1385
	Nr1386 = 1386
	Nr1387 = 1387
	Nr1388 = 1388
	Nr1389 = 1389
	Nr1390 = 1390
	Nr1391 = 1391
	Nr1392 = 1392
	Nr1393 = 1393
	Nr1394 = 1394
	Nr1395 = 1395
	Nr1396 = 1396
	Nr1397 = 1397
	Nr1398 = 1398
	Nr1399 = 1399
	Nr1400 = 1400
	Nr1401 = 1401
	Nr1402 = 1402
	Nr1403 = 1403
	Nr1404 = 1404
	Nr1405 = 1405
	Nr1406 = 1406
	Nr1407 = 1407
	Nr1408 = 1408
	Nr1409 = 1409
	Nr1410 = 1410
	Nr1411 = 1411
	Nr1412 = 1412
	Nr1413 = 1413
	Nr1414 = 1414
	Nr1415 = 1415
	Nr1416 = 1416
	Nr1417 = 1417
	Nr1418 = 1418
	Nr1419 = 1419
	Nr1420 = 1420
	Nr1421 = 1421
	Nr1422 = 1422
	Nr1423 = 1423
	Nr1424 = 1424
	Nr1425 = 1425
	Nr1426 = 1426
	Nr1427 = 1427
	Nr1428 = 1428
	Nr1429 = 1429
	Nr1430 = 1430
	Nr1431 = 1431
	Nr1432 = 1432
	Nr1433 = 1433
	Nr1434 = 1434
	Nr1435 = 1435
	Nr1436 = 1436
	Nr1437 = 1437
	Nr1438 = 1438
	Nr1439 = 1439
	Nr1440 = 1440
	Nr1441 = 1441
	Nr1442 = 1442
	Nr1443 = 1443
	Nr1444 = 1444
	Nr1445 = 1445
	Nr1446 = 1446
	Nr1447 = 1447
	Nr1448 = 1448
	Nr1449 = 1449
	Nr1450 = 1450
	Nr1451 = 1451
	Nr1452 = 1452
	Nr1453 = 1453
	Nr1454 = 1454
	Nr1455 = 1455
	Nr1456 = 1456
	Nr1457 = 1457
	Nr1458 = 1458
	Nr1459 = 1459
	Nr1460 = 1460
	Nr1461 = 1461
	Nr1462 = 1462
	Nr1463 = 1463
	Nr1464 = 1464
	Nr1465 = 1465
	Nr1466 = 1466
	Nr1467 = 1467
	Nr1468 = 1468
	Nr1469 = 1469
	Nr1470 = 1470
	Nr1471 = 1471
	Nr1472 = 1472
	Nr1473 = 1473
	Nr1474 = 1474
	Nr1475 = 1475
	Nr1476 = 1476
	Nr1477 = 1477
	Nr1478 = 1478
	Nr1479 = 1479
	Nr1480 = 1480
	Nr1481 = 1481
	Nr1482 = 1482
	Nr1483 = 1483
	Nr1484 = 1484
	Nr1485 = 1485
	Nr1486 = 1486
	Nr1487 = 1487
	Nr1488 = 1488
	Nr1489 = 1489
	Nr1490 = 1490
	Nr1491 = 1491
	Nr1492 = 1492
	Nr1493 = 1493
	Nr1494 = 1494
	Nr1495 = 1495
	Nr1496 = 1496
	Nr1497 = 1497
	Nr1498 = 1498
	Nr1499 = 1499
	Nr1500 = 1500
	Nr1501 = 1501
	Nr1502 = 1502
	Nr1503 = 1503
	Nr1504 = 1504
	Nr1505 = 1505
	Nr1506 = 1506
	Nr1507 = 1507
	Nr1508 = 1508
	Nr1509 = 1509
	Nr1510 = 1510
	Nr1511 = 1511
	Nr1512 = 1512
	Nr1513 = 1513
	Nr1514 = 1514
	Nr1515 = 1515
	Nr1516 = 1516
	Nr1517 = 1517
	Nr1518 = 1518
	Nr1519 = 1519
	Nr1520 = 1520
	Nr1521 = 1521
	Nr1522 = 1522
	Nr1523 = 1523
	Nr1524 = 1524
	Nr1525 = 1525
	Nr1526 = 1526
	Nr1527 = 1527
	Nr1528 = 1528
	Nr1529 = 1529
	Nr1530 = 1530
	Nr1531 = 1531
	Nr1532 = 1532
	Nr1533 = 1533
	Nr1534 = 1534
	Nr1535 = 1535
	Nr1536 = 1536
	Nr1537 = 1537
	Nr1538 = 1538
	Nr1539 = 1539
	Nr1540 = 1540
	Nr1541 = 1541
	Nr1542 = 1542
	Nr1543 = 1543
	Nr1544 = 1544
	Nr1545 = 1545
	Nr1546 = 1546
	Nr1547 = 1547
	Nr1548 = 1548
	Nr1549 = 1549
	Nr1550 = 1550
	Nr1551 = 1551
	Nr1552 = 1552
	Nr1553 = 1553
	Nr1554 = 1554
	Nr1555 = 1555
	Nr1556 = 1556
	Nr1557 = 1557
	Nr1558 = 1558
	Nr1559 = 1559
	Nr1560 = 1560
	Nr1561 = 1561
	Nr1562 = 1562
	Nr1563 = 1563
	Nr1564 = 1564
	Nr1565 = 1565
	Nr1566 = 1566
	Nr1567 = 1567
	Nr1568 = 1568
	Nr1569 = 1569
	Nr1570 = 1570
	Nr1571 = 1571
	Nr1572 = 1572
	Nr1573 = 1573
	Nr1574 = 1574
	Nr1575 = 1575
	Nr1576 = 1576
	Nr1577 = 1577
	Nr1578 = 1578
	Nr1579 = 1579
	Nr1580 = 1580
	Nr1581 = 1581
	Nr1582 = 1582
	Nr1583 = 1583
	Nr1584 = 1584
	Nr1585 = 1585
	Nr1586 = 1586
	Nr1587 = 1587
	Nr1588 = 1588
	Nr1589 = 1589
	Nr1590 = 1590
	Nr1591 = 1591
	Nr1592 = 1592
	Nr1593 = 1593
	Nr1594 = 1594
	Nr1595 = 1595
	Nr1596 = 1596
	Nr1597 = 1597
	Nr1598 = 1598
	Nr1599 = 1599
	Nr1600 = 1600
	Nr1601 = 1601
	Nr1602 = 1602
	Nr1603 = 1603
	Nr1604 = 1604
	Nr1605 = 1605
	Nr1606 = 1606
	Nr1607 = 1607
	Nr1608 = 1608
	Nr1609 = 1609
	Nr1610 = 1610
	Nr1611 = 1611
	Nr1612 = 1612
	Nr1613 = 1613
	Nr1614 = 1614
	Nr1615 = 1615
	Nr1616 = 1616
	Nr1617 = 1617
	Nr1618 = 1618
	Nr1619 = 1619
	Nr1620 = 1620
	Nr1621 = 1621
	Nr1622 = 1622
	Nr1623 = 1623
	Nr1624 = 1624
	Nr1625 = 1625
	Nr1626 = 1626
	Nr1627 = 1627
	Nr1628 = 1628
	Nr1629 = 1629
	Nr1630 = 1630
	Nr1631 = 1631
	Nr1632 = 1632
	Nr1633 = 1633
	Nr1634 = 1634
	Nr1635 = 1635
	Nr1636 = 1636
	Nr1637 = 1637
	Nr1638 = 1638
	Nr1639 = 1639
	Nr1640 = 1640
	Nr1641 = 1641
	Nr1642 = 1642
	Nr1643 = 1643
	Nr1644 = 1644
	Nr1645 = 1645
	Nr1646 = 1646
	Nr1647 = 1647
	Nr1648 = 1648
	Nr1649 = 1649
	Nr1650 = 1650
	Nr1651 = 1651
	Nr1652 = 1652
	Nr1653 = 1653
	Nr1654 = 1654
	Nr1655 = 1655
	Nr1656 = 1656
	Nr1657 = 1657
	Nr1658 = 1658
	Nr1659 = 1659
	Nr1660 = 1660
	Nr1661 = 1661
	Nr1662 = 1662
	Nr1663 = 1663
	Nr1664 = 1664
	Nr1665 = 1665
	Nr1666 = 1666
	Nr1667 = 1667
	Nr1668 = 1668
	Nr1669 = 1669
	Nr1670 = 1670
	Nr1671 = 1671
	Nr1672 = 1672
	Nr1673 = 1673
	Nr1674 = 1674
	Nr1675 = 1675
	Nr1676 = 1676
	Nr1677 = 1677
	Nr1678 = 1678
	Nr1679 = 1679
	Nr1680 = 1680
	Nr1681 = 1681
	Nr1682 = 1682
	Nr1683 = 1683
	Nr1684 = 1684
	Nr1685 = 1685
	Nr1686 = 1686
	Nr1687 = 1687
	Nr1688 = 1688
	Nr1689 = 1689
	Nr1690 = 1690
	Nr1691 = 1691
	Nr1692 = 1692
	Nr1693 = 1693
	Nr1694 = 1694
	Nr1695 = 1695
	Nr1696 = 1696
	Nr1697 = 1697
	Nr1698 = 1698
	Nr1699 = 1699
	Nr1700 = 1700
	Nr1701 = 1701
	Nr1702 = 1702
	Nr1703 = 1703
	Nr1704 = 1704
	Nr1705 = 1705
	Nr1706 = 1706
	Nr1707 = 1707
	Nr1708 = 1708
	Nr1709 = 1709
	Nr1710 = 1710
	Nr1711 = 1711
	Nr1712 = 1712
	Nr1713 = 1713
	Nr1714 = 1714
	Nr1715 = 1715
	Nr1716 = 1716
	Nr1717 = 1717
	Nr1718 = 1718
	Nr1719 = 1719
	Nr1720 = 1720
	Nr1721 = 1721
	Nr1722 = 1722
	Nr1723 = 1723
	Nr1724 = 1724
	Nr1725 = 1725
	Nr1726 = 1726
	Nr1727 = 1727
	Nr1728 = 1728
	Nr1729 = 1729
	Nr1730 = 1730
	Nr1731 = 1731
	Nr1732 = 1732
	Nr1733 = 1733
	Nr1734 = 1734
	Nr1735 = 1735
	Nr1736 = 1736
	Nr1737 = 1737
	Nr1738 = 1738
	Nr1739 = 1739
	Nr1740 = 1740
	Nr1741 = 1741
	Nr1742 = 1742
	Nr1743 = 1743
	Nr1744 = 1744
	Nr1745 = 1745
	Nr1746 = 1746
	Nr1747 = 1747
	Nr1748 = 1748
	Nr1749 = 1749
	Nr1750 = 1750
	Nr1751 = 1751
	Nr1752 = 1752
	Nr1753 = 1753
	Nr1754 = 1754
	Nr1755 = 1755
	Nr1756 = 1756
	Nr1757 = 1757
	Nr1758 = 1758
	Nr1759 = 1759
	Nr1760 = 1760
	Nr1761 = 1761
	Nr1762 = 1762
	Nr1763 = 1763
	Nr1764 = 1764
	Nr1765 = 1765
	Nr1766 = 1766
	Nr1767 = 1767
	Nr1768 = 1768
	Nr1769 = 1769
	Nr1770 = 1770
	Nr1771 = 1771
	Nr1772 = 1772
	Nr1773 = 1773
	Nr1774 = 1774
	Nr1775 = 1775
	Nr1776 = 1776
	Nr1777 = 1777
	Nr1778 = 1778
	Nr1779 = 1779
	Nr1780 = 1780
	Nr1781 = 1781
	Nr1782 = 1782
	Nr1783 = 1783
	Nr1784 = 1784
	Nr1785 = 1785
	Nr1786 = 1786
	Nr1787 = 1787
	Nr1788 = 1788
	Nr1789 = 1789
	Nr1790 = 1790
	Nr1791 = 1791
	Nr1792 = 1792
	Nr1793 = 1793
	Nr1794 = 1794
	Nr1795 = 1795
	Nr1796 = 1796
	Nr1797 = 1797
	Nr1798 = 1798
	Nr1799 = 1799
	Nr1800 = 1800
	Nr1801 = 1801
	Nr1802 = 1802
	Nr1803 = 1803
	Nr1804 = 1804
	Nr1805 = 1805
	Nr1806 = 1806
	Nr1807 = 1807
	Nr1808 = 1808
	Nr1809 = 1809
	Nr1810 = 1810
	Nr1811 = 1811
	Nr1812 = 1812
	Nr1813 = 1813
	Nr1814 = 1814
	Nr1815 = 1815
	Nr1816 = 1816
	Nr1817 = 1817
	Nr1818 = 1818
	Nr1819 = 1819
	Nr1820 = 1820
	Nr1821 = 1821
	Nr1822 = 1822
	Nr1823 = 1823
	Nr1824 = 1824
	Nr1825 = 1825
	Nr1826 = 1826
	Nr1827 = 1827
	Nr1828 = 1828
	Nr1829 = 1829
	Nr1830 = 1830
	Nr1831 = 1831
	Nr1832 = 1832
	Nr1833 = 1833
	Nr1834 = 1834
	Nr1835 = 1835
	Nr1836 = 1836
	Nr1837 = 1837
	Nr1838 = 1838
	Nr1839 = 1839
	Nr1840 = 1840
	Nr1841 = 1841
	Nr1842 = 1842
	Nr1843 = 1843
	Nr1844 = 1844
	Nr1845 = 1845
	Nr1846 = 1846
	Nr1847 = 1847
	Nr1848 = 1848
	Nr1849 = 1849
	Nr1850 = 1850
	Nr1851 = 1851
	Nr1852 = 1852
	Nr1853 = 1853
	Nr1854 = 1854
	Nr1855 = 1855
	Nr1856 = 1856
	Nr1857 = 1857
	Nr1858 = 1858
	Nr1859 = 1859
	Nr1860 = 1860
	Nr1861 = 1861
	Nr1862 = 1862
	Nr1863 = 1863
	Nr1864 = 1864
	Nr1865 = 1865
	Nr1866 = 1866
	Nr1867 = 1867
	Nr1868 = 1868
	Nr1869 = 1869
	Nr1870 = 1870
	Nr1871 = 1871
	Nr1872 = 1872
	Nr1873 = 1873
	Nr1874 = 1874
	Nr1875 = 1875
	Nr1876 = 1876
	Nr1877 = 1877
	Nr1878 = 1878
	Nr1879 = 1879
	Nr1880 = 1880
	Nr1881 = 1881
	Nr1882 = 1882
	Nr1883 = 1883
	Nr1884 = 1884
	Nr1885 = 1885
	Nr1886 = 1886
	Nr1887 = 1887
	Nr1888 = 1888
	Nr1889 = 1889
	Nr1890 = 1890
	Nr1891 = 1891
	Nr1892 = 1892
	Nr1893 = 1893
	Nr1894 = 1894
	Nr1895 = 1895
	Nr1896 = 1896
	Nr1897 = 1897
	Nr1898 = 1898
	Nr1899 = 1899
	Nr1900 = 1900
	Nr1901 = 1901
	Nr1902 = 1902
	Nr1903 = 1903
	Nr1904 = 1904
	Nr1905 = 1905
	Nr1906 = 1906
	Nr1907 = 1907
	Nr1908 = 1908
	Nr1909 = 1909
	Nr1910 = 1910
	Nr1911 = 1911
	Nr1912 = 1912
	Nr1913 = 1913
	Nr1914 = 1914
	Nr1915 = 1915
	Nr1916 = 1916
	Nr1917 = 1917
	Nr1918 = 1918
	Nr1919 = 1919
	Nr1920 = 1920
	Nr1921 = 1921
	Nr1922 = 1922
	Nr1923 = 1923
	Nr1924 = 1924
	Nr1925 = 1925
	Nr1926 = 1926
	Nr1927 = 1927
	Nr1928 = 1928
	Nr1929 = 1929
	Nr1930 = 1930
	Nr1931 = 1931
	Nr1932 = 1932
	Nr1933 = 1933
	Nr1934 = 1934
	Nr1935 = 1935
	Nr1936 = 1936
	Nr1937 = 1937
	Nr1938 = 1938
	Nr1939 = 1939
	Nr1940 = 1940
	Nr1941 = 1941
	Nr1942 = 1942
	Nr1943 = 1943
	Nr1944 = 1944
	Nr1945 = 1945
	Nr1946 = 1946
	Nr1947 = 1947
	Nr1948 = 1948
	Nr1949 = 1949
	Nr1950 = 1950
	Nr1951 = 1951
	Nr1952 = 1952
	Nr1953 = 1953
	Nr1954 = 1954
	Nr1955 = 1955
	Nr1956 = 1956
	Nr1957 = 1957
	Nr1958 = 1958
	Nr1959 = 1959
	Nr1960 = 1960
	Nr1961 = 1961
	Nr1962 = 1962
	Nr1963 = 1963
	Nr1964 = 1964
	Nr1965 = 1965
	Nr1966 = 1966
	Nr1967 = 1967
	Nr1968 = 1968
	Nr1969 = 1969
	Nr1970 = 1970
	Nr1971 = 1971
	Nr1972 = 1972
	Nr1973 = 1973
	Nr1974 = 1974
	Nr1975 = 1975
	Nr1976 = 1976
	Nr1977 = 1977
	Nr1978 = 1978
	Nr1979 = 1979
	Nr1980 = 1980
	Nr1981 = 1981
	Nr1982 = 1982
	Nr1983 = 1983
	Nr1984 = 1984
	Nr1985 = 1985
	Nr1986 = 1986
	Nr1987 = 1987
	Nr1988 = 1988
	Nr1989 = 1989
	Nr1990 = 1990
	Nr1991 = 1991
	Nr1992 = 1992
	Nr1993 = 1993
	Nr1994 = 1994
	Nr1995 = 1995
	Nr1996 = 1996
	Nr1997 = 1997
	Nr1998 = 1998
	Nr1999 = 1999
	Nr2000 = 2000


# noinspection SpellCheckingInspection
class Table(Enum):
	"""Repeated capability Table"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5


# noinspection SpellCheckingInspection
class ThirdChannelBw(Enum):
	"""Repeated capability ThirdChannelBw"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Bw100 = 100
	Bw150 = 150
	Bw200 = 200


# noinspection SpellCheckingInspection
class UtraAdjChannel(Enum):
	"""Repeated capability UtraAdjChannel"""
	Empty = EmptyRepCap
	Default = DefaultRepCap
	Ch1 = 1
	Ch2 = 2
