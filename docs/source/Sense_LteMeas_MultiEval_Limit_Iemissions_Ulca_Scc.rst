Scc<SecondaryCC>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: CC1 .. CC7
	rc = driver.sense.lteMeas.multiEval.limit.iemissions.ulca.scc.repcap_secondaryCC_get()
	driver.sense.lteMeas.multiEval.limit.iemissions.ulca.scc.repcap_secondaryCC_set(repcap.SecondaryCC.CC1)



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:MEASurement<Instance>:MEValuation:LIMit:IEMissions:ULCA:SCC<Nr>

.. code-block:: python

	SENSe:LTE:MEASurement<Instance>:MEValuation:LIMit:IEMissions:ULCA:SCC<Nr>



.. autoclass:: RsCMPX_LteMeas.Implementations.Sense.LteMeas.MultiEval.Limit.Iemissions.Ulca.Scc.SccCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.lteMeas.multiEval.limit.iemissions.ulca.scc.clone()