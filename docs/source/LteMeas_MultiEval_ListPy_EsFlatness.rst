EsFlatness
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.EsFlatness.EsFlatnessCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.listPy.esFlatness.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_ListPy_EsFlatness_Difference.rst
	LteMeas_MultiEval_ListPy_EsFlatness_Maxr.rst
	LteMeas_MultiEval_ListPy_EsFlatness_Minr.rst
	LteMeas_MultiEval_ListPy_EsFlatness_Ripple.rst
	LteMeas_MultiEval_ListPy_EsFlatness_ScIndex.rst