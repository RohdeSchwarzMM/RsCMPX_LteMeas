StandardDev
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:IEMission:ULCA[:PCC]:MARGin:SDEViation

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:IEMission:ULCA[:PCC]:MARGin:SDEViation



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.InbandEmission.Ulca.Pcc.Margin.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: