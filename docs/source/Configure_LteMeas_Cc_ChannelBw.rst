ChannelBw
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:CC<Nr>:CBANdwidth

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:CC<Nr>:CBANdwidth



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.Cc.ChannelBw.ChannelBwCls
	:members:
	:undoc-members:
	:noindex: