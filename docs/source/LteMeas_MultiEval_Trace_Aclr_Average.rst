Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:TRACe:ACLR:AVERage
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:ACLR:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:TRACe:ACLR:AVERage
	FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:ACLR:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Trace.Aclr.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: