ChannelBw
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:SCC<Nr>:CBANdwidth

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:SCC<Nr>:CBANdwidth



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.Scc.ChannelBw.ChannelBwCls
	:members:
	:undoc-members:
	:noindex: