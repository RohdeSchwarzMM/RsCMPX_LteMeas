Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:MAXimum
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:MAXimum
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:PMONitor:MAXimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:MAXimum
	FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:MAXimum
	CALCulate:LTE:MEASurement<Instance>:MEValuation:PMONitor:MAXimum



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Pmonitor.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: