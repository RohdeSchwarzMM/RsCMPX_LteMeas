Emtc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:EMTC:ENABle
	single: CONFigure:LTE:MEASurement<instance>:EMTC:MB<number>
	single: CONFigure:LTE:MEASurement<Instance>:EMTC:NBANd

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:EMTC:ENABle
	CONFigure:LTE:MEASurement<instance>:EMTC:MB<number>
	CONFigure:LTE:MEASurement<Instance>:EMTC:NBANd



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.Emtc.EmtcCls
	:members:
	:undoc-members:
	:noindex: