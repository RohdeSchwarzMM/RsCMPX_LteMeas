Merror
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Prach.Trace.Merror.MerrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.prach.trace.merror.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_Prach_Trace_Merror_Average.rst
	LteMeas_Prach_Trace_Merror_Current.rst
	LteMeas_Prach_Trace_Merror_Maximum.rst