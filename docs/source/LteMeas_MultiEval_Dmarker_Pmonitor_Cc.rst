Cc<CarrierComponent>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.lteMeas.multiEval.dmarker.pmonitor.cc.repcap_carrierComponent_get()
	driver.lteMeas.multiEval.dmarker.pmonitor.cc.repcap_carrierComponent_set(repcap.CarrierComponent.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:DMARker<No>:PMONitor:CC<Nr>

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:DMARker<No>:PMONitor:CC<Nr>



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Dmarker.Pmonitor.Cc.CcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.dmarker.pmonitor.cc.clone()