Segment<Segment>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2000
	rc = driver.lteMeas.multiEval.listPy.segment.repcap_segment_get()
	driver.lteMeas.multiEval.listPy.segment.repcap_segment_set(repcap.Segment.Nr1)





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Segment.SegmentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.listPy.segment.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_ListPy_Segment_Aclr.rst
	LteMeas_MultiEval_ListPy_Segment_EsFlatness.rst
	LteMeas_MultiEval_ListPy_Segment_InbandEmission.rst
	LteMeas_MultiEval_ListPy_Segment_Modulation.rst
	LteMeas_MultiEval_ListPy_Segment_Pmonitor.rst
	LteMeas_MultiEval_ListPy_Segment_Power.rst
	LteMeas_MultiEval_ListPy_Segment_SeMask.rst