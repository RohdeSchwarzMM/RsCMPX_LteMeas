Modulation
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:MODulation:LRSindex
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:MODulation:ZCZConfig
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:MODulation:EWPosition

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:PRACh:MODulation:LRSindex
	CONFigure:LTE:MEASurement<Instance>:PRACh:MODulation:ZCZConfig
	CONFigure:LTE:MEASurement<Instance>:PRACh:MODulation:EWPosition



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.Prach.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.prach.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_LteMeas_Prach_Modulation_EwLength.rst
	Configure_LteMeas_Prach_Modulation_Sindex.rst