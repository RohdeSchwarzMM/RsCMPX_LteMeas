AcSpacing
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:CAGGregation[:SCC<Nr>]:ACSPacing

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:CAGGregation[:SCC<Nr>]:ACSPacing



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.CarrierAggregation.Scc.AcSpacing.AcSpacingCls
	:members:
	:undoc-members:
	:noindex: