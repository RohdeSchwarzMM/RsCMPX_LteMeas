Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:LOW:CURRent
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:LOW:CURRent

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:LOW:CURRent
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:LOW:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Modulation.Evm.Peak.Low.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: