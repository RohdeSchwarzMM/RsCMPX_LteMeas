Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:ACLR:EUTRa:NEGativ:CURRent
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:ACLR:EUTRa:NEGativ:CURRent

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:ACLR:EUTRa:NEGativ:CURRent
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:ACLR:EUTRa:NEGativ:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Aclr.Eutra.Negativ.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: