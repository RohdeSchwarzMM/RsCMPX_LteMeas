Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:PCC:AVERage
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:PCC:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:PCC:AVERage
	FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:PCC:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Pmonitor.Pcc.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: