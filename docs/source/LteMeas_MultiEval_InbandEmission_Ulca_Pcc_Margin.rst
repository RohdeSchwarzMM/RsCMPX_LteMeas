Margin
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.InbandEmission.Ulca.Pcc.Margin.MarginCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.inbandEmission.ulca.pcc.margin.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_InbandEmission_Ulca_Pcc_Margin_Average.rst
	LteMeas_MultiEval_InbandEmission_Ulca_Pcc_Margin_Current.rst
	LteMeas_MultiEval_InbandEmission_Ulca_Pcc_Margin_Extreme.rst
	LteMeas_MultiEval_InbandEmission_Ulca_Pcc_Margin_StandardDev.rst