State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:PRACh:STATe

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:PRACh:STATe



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Prach.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.prach.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_Prach_State_All.rst