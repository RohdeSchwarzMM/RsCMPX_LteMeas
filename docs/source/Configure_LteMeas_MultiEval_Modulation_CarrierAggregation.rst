CarrierAggregation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:MODulation:CAGGregation:LLOCation

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:MODulation:CAGGregation:LLOCation



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.Modulation.CarrierAggregation.CarrierAggregationCls
	:members:
	:undoc-members:
	:noindex: