Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:CURRent
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:CURRent

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:CURRent
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Power.TxPower.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: