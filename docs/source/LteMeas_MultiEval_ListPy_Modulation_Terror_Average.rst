Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:TERRor:AVERage
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:TERRor:AVERage

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:TERRor:AVERage
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:TERRor:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Modulation.Terror.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: