PlcId
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:SCC<Nr>:PLCid

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:SCC<Nr>:PLCid



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.Scc.PlcId.PlcIdCls
	:members:
	:undoc-members:
	:noindex: