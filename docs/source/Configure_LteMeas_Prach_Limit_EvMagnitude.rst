EvMagnitude
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:LIMit:EVMagnitude

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:PRACh:LIMit:EVMagnitude



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.Prach.Limit.EvMagnitude.EvMagnitudeCls
	:members:
	:undoc-members:
	:noindex: