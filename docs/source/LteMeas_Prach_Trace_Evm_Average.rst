Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:PRACh:TRACe:EVM:AVERage
	single: FETCh:LTE:MEASurement<Instance>:PRACh:TRACe:EVM:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:PRACh:TRACe:EVM:AVERage
	FETCh:LTE:MEASurement<Instance>:PRACh:TRACe:EVM:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Prach.Trace.Evm.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: