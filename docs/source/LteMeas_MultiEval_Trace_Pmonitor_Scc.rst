Scc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:TRACe:PMONitor:SCC
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:PMONitor:SCC

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:TRACe:PMONitor:SCC
	FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:PMONitor:SCC



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Trace.Pmonitor.Scc.SccCls
	:members:
	:undoc-members:
	:noindex: