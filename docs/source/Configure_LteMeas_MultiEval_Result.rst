Result
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult[:ALL]
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult:MERRor
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult:PERRor
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult:IEMissions
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult:EVMC
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult:ESFLatness
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult:TXM
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult:IQ
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult:SEMask
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult:ACLR
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult:RBATable
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult:PMONitor
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult:PDYNamics
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult:BLER

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult[:ALL]
	CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult:MERRor
	CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult:PERRor
	CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult:IEMissions
	CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult:EVMC
	CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult:ESFLatness
	CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult:TXM
	CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult:IQ
	CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult:SEMask
	CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult:ACLR
	CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult:RBATable
	CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult:PMONitor
	CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult:PDYNamics
	CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult:BLER



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.multiEval.result.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_LteMeas_MultiEval_Result_EvMagnitude.rst