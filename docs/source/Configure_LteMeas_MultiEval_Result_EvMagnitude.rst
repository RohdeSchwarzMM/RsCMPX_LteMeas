EvMagnitude
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult:EVMagnitude

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult:EVMagnitude



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.Result.EvMagnitude.EvMagnitudeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.multiEval.result.evMagnitude.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_LteMeas_MultiEval_Result_EvMagnitude_EvmSymbol.rst