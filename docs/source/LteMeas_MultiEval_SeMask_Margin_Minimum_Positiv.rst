Positiv
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:SEMask:MARGin:MINimum:POSitiv

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:SEMask:MARGin:MINimum:POSitiv



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.SeMask.Margin.Minimum.Positiv.PositivCls
	:members:
	:undoc-members:
	:noindex: