Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:ULCA:PCC:AVERage
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:ULCA:PCC:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:ULCA:PCC:AVERage
	FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:ULCA:PCC:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Pmonitor.Ulca.Pcc.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: