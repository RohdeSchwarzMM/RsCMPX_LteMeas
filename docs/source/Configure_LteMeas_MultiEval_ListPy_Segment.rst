Segment<Segment>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2000
	rc = driver.configure.lteMeas.multiEval.listPy.segment.repcap_segment_get()
	driver.configure.lteMeas.multiEval.listPy.segment.repcap_segment_set(repcap.Segment.Nr1)





.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.ListPy.Segment.SegmentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.multiEval.listPy.segment.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_LteMeas_MultiEval_ListPy_Segment_Aclr.rst
	Configure_LteMeas_MultiEval_ListPy_Segment_CarrierAggregation.rst
	Configure_LteMeas_MultiEval_ListPy_Segment_Cc.rst
	Configure_LteMeas_MultiEval_ListPy_Segment_Cidx.rst
	Configure_LteMeas_MultiEval_ListPy_Segment_Emtc.rst
	Configure_LteMeas_MultiEval_ListPy_Segment_Modulation.rst
	Configure_LteMeas_MultiEval_ListPy_Segment_PlcId.rst
	Configure_LteMeas_MultiEval_ListPy_Segment_Pmonitor.rst
	Configure_LteMeas_MultiEval_ListPy_Segment_Power.rst
	Configure_LteMeas_MultiEval_ListPy_Segment_RbAllocation.rst
	Configure_LteMeas_MultiEval_ListPy_Segment_Scc.rst
	Configure_LteMeas_MultiEval_ListPy_Segment_SeMask.rst
	Configure_LteMeas_MultiEval_ListPy_Segment_Setup.rst
	Configure_LteMeas_MultiEval_ListPy_Segment_SingleCmw.rst
	Configure_LteMeas_MultiEval_ListPy_Segment_Tdd.rst