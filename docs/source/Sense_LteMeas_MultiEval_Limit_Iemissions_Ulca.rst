Ulca
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:MEASurement<Instance>:MEValuation:LIMit:IEMissions:ULCA[:PCC]

.. code-block:: python

	SENSe:LTE:MEASurement<Instance>:MEValuation:LIMit:IEMissions:ULCA[:PCC]



.. autoclass:: RsCMPX_LteMeas.Implementations.Sense.LteMeas.MultiEval.Limit.Iemissions.Ulca.UlcaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.lteMeas.multiEval.limit.iemissions.ulca.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_LteMeas_MultiEval_Limit_Iemissions_Ulca_Scc.rst