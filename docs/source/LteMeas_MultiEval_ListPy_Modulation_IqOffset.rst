IqOffset
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Modulation.IqOffset.IqOffsetCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.listPy.modulation.iqOffset.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_ListPy_Modulation_IqOffset_Average.rst
	LteMeas_MultiEval_ListPy_Modulation_IqOffset_Current.rst
	LteMeas_MultiEval_ListPy_Modulation_IqOffset_Extreme.rst
	LteMeas_MultiEval_ListPy_Modulation_IqOffset_StandardDev.rst