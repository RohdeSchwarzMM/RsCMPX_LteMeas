Perror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QPSK:PERRor

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QPSK:PERRor



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.Limit.Qpsk.Perror.PerrorCls
	:members:
	:undoc-members:
	:noindex: