Perror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:REFMarker:PERRor

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:REFMarker:PERRor



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ReferenceMarker.Perror.PerrorCls
	:members:
	:undoc-members:
	:noindex: