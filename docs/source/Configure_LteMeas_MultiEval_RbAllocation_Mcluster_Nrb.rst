Nrb<RBcount>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.configure.lteMeas.multiEval.rbAllocation.mcluster.nrb.repcap_rBcount_get()
	driver.configure.lteMeas.multiEval.rbAllocation.mcluster.nrb.repcap_rBcount_set(repcap.RBcount.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:RBALlocation:MCLuster:NRB<Number>

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:RBALlocation:MCLuster:NRB<Number>



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.RbAllocation.Mcluster.Nrb.NrbCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.multiEval.rbAllocation.mcluster.nrb.clone()