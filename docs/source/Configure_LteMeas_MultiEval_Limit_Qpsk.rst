Qpsk
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QPSK:FERRor
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QPSK:SFLatness
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QPSK:ESFLatness

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QPSK:FERRor
	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QPSK:SFLatness
	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QPSK:ESFLatness



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.Limit.Qpsk.QpskCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.multiEval.limit.qpsk.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_LteMeas_MultiEval_Limit_Qpsk_EvMagnitude.rst
	Configure_LteMeas_MultiEval_Limit_Qpsk_Ibe.rst
	Configure_LteMeas_MultiEval_Limit_Qpsk_IqOffset.rst
	Configure_LteMeas_MultiEval_Limit_Qpsk_Merror.rst
	Configure_LteMeas_MultiEval_Limit_Qpsk_Perror.rst