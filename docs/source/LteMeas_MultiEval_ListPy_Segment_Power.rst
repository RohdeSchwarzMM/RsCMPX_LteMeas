Power
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Segment.Power.PowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.listPy.segment.power.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_ListPy_Segment_Power_Average.rst
	LteMeas_MultiEval_ListPy_Segment_Power_Cc.rst
	LteMeas_MultiEval_ListPy_Segment_Power_Current.rst
	LteMeas_MultiEval_ListPy_Segment_Power_Maximum.rst
	LteMeas_MultiEval_ListPy_Segment_Power_Minimum.rst
	LteMeas_MultiEval_ListPy_Segment_Power_StandardDev.rst