EvmSymbol
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Trace.EvmSymbol.EvmSymbolCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.trace.evmSymbol.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_Trace_EvmSymbol_Average.rst
	LteMeas_MultiEval_Trace_EvmSymbol_Current.rst
	LteMeas_MultiEval_Trace_EvmSymbol_Maximum.rst