Modulation
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Prach.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.prach.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_Prach_Modulation_Average.rst
	LteMeas_Prach_Modulation_Current.rst
	LteMeas_Prach_Modulation_DpfOffset.rst
	LteMeas_Prach_Modulation_DsIndex.rst
	LteMeas_Prach_Modulation_Extreme.rst
	LteMeas_Prach_Modulation_Nsymbol.rst
	LteMeas_Prach_Modulation_Preamble.rst
	LteMeas_Prach_Modulation_Scorrelation.rst
	LteMeas_Prach_Modulation_StandardDev.rst