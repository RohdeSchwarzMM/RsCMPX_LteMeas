Pusch
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:MODulation:EEPeriods:PUSCh:LEADing
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:MODulation:EEPeriods:PUSCh:LAGGing

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:MODulation:EEPeriods:PUSCh:LEADing
	CONFigure:LTE:MEASurement<Instance>:MEValuation:MODulation:EEPeriods:PUSCh:LAGGing



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.Modulation.EePeriods.Pusch.PuschCls
	:members:
	:undoc-members:
	:noindex: