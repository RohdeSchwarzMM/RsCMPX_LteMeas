StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:PCC:SDEViation
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:PCC:SDEViation

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:PCC:SDEViation
	FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:PCC:SDEViation



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Pmonitor.Pcc.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: