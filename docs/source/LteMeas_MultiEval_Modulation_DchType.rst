DchType
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:MODulation:DCHType

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:MODulation:DCHType



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Modulation.DchType.DchTypeCls
	:members:
	:undoc-members:
	:noindex: