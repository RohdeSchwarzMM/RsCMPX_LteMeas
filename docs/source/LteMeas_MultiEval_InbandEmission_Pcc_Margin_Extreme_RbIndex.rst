RbIndex
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:IEMission[:PCC]:MARGin:EXTReme:RBINdex

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:IEMission[:PCC]:MARGin:EXTReme:RBINdex



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.InbandEmission.Pcc.Margin.Extreme.RbIndex.RbIndexCls
	:members:
	:undoc-members:
	:noindex: