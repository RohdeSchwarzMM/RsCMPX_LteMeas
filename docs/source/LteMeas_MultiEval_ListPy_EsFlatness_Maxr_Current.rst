Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:ESFLatness:MAXR<nr>:CURRent
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:ESFLatness:MAXR<nr>:CURRent

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:ESFLatness:MAXR<nr>:CURRent
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:ESFLatness:MAXR<nr>:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.EsFlatness.Maxr.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: