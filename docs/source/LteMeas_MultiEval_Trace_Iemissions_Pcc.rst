Pcc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:TRACe:IEMissions[:PCC]
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:IEMissions[:PCC]

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:TRACe:IEMissions[:PCC]
	FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:IEMissions[:PCC]



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Trace.Iemissions.Pcc.PccCls
	:members:
	:undoc-members:
	:noindex: