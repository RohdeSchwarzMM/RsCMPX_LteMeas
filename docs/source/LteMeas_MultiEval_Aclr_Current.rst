Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:ACLR:CURRent
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:ACLR:CURRent
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:ACLR:CURRent

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:ACLR:CURRent
	FETCh:LTE:MEASurement<Instance>:MEValuation:ACLR:CURRent
	CALCulate:LTE:MEASurement<Instance>:MEValuation:ACLR:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Aclr.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: