Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:ESFLatness:AVERage
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:ESFLatness:AVERage
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:ESFLatness:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:ESFLatness:AVERage
	FETCh:LTE:MEASurement<Instance>:MEValuation:ESFLatness:AVERage
	CALCulate:LTE:MEASurement<Instance>:MEValuation:ESFLatness:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.EsFlatness.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: