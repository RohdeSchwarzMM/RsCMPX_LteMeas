Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:MAXimum
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:MAXimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:MAXimum
	FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:MAXimum



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Trace.SeMask.Rbw.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: