Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:PRACh:MODulation:CURRent
	single: FETCh:LTE:MEASurement<Instance>:PRACh:MODulation:CURRent
	single: CALCulate:LTE:MEASurement<Instance>:PRACh:MODulation:CURRent

.. code-block:: python

	READ:LTE:MEASurement<Instance>:PRACh:MODulation:CURRent
	FETCh:LTE:MEASurement<Instance>:PRACh:MODulation:CURRent
	CALCulate:LTE:MEASurement<Instance>:PRACh:MODulation:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Prach.Modulation.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: