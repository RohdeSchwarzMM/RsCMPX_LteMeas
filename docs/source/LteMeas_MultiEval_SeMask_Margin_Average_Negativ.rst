Negativ
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:SEMask:MARGin:AVERage:NEGativ

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:SEMask:MARGin:AVERage:NEGativ



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.SeMask.Margin.Average.Negativ.NegativCls
	:members:
	:undoc-members:
	:noindex: