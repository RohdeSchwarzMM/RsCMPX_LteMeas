MultiEval
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:TOUT
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:MMODe
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:REPetition
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:SCONdition
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:ULDL
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:SSUBframe
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:MOEXception
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:CPRefix
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:CTYPe
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:SCTYpe
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:PSEarch
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:PFORmat
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:NVFilter
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:ORVFilter
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:CTVFilter
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:DSSPusch
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:GHOPping
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:MSLot
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:VIEW

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:TOUT
	CONFigure:LTE:MEASurement<Instance>:MEValuation:MMODe
	CONFigure:LTE:MEASurement<Instance>:MEValuation:REPetition
	CONFigure:LTE:MEASurement<Instance>:MEValuation:SCONdition
	CONFigure:LTE:MEASurement<Instance>:MEValuation:ULDL
	CONFigure:LTE:MEASurement<Instance>:MEValuation:SSUBframe
	CONFigure:LTE:MEASurement<Instance>:MEValuation:MOEXception
	CONFigure:LTE:MEASurement<Instance>:MEValuation:CPRefix
	CONFigure:LTE:MEASurement<Instance>:MEValuation:CTYPe
	CONFigure:LTE:MEASurement<Instance>:MEValuation:SCTYpe
	CONFigure:LTE:MEASurement<Instance>:MEValuation:PSEarch
	CONFigure:LTE:MEASurement<Instance>:MEValuation:PFORmat
	CONFigure:LTE:MEASurement<Instance>:MEValuation:NVFilter
	CONFigure:LTE:MEASurement<Instance>:MEValuation:ORVFilter
	CONFigure:LTE:MEASurement<Instance>:MEValuation:CTVFilter
	CONFigure:LTE:MEASurement<Instance>:MEValuation:DSSPusch
	CONFigure:LTE:MEASurement<Instance>:MEValuation:GHOPping
	CONFigure:LTE:MEASurement<Instance>:MEValuation:MSLot
	CONFigure:LTE:MEASurement<Instance>:MEValuation:VIEW



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_LteMeas_MultiEval_Bler.rst
	Configure_LteMeas_MultiEval_Cc.rst
	Configure_LteMeas_MultiEval_Limit.rst
	Configure_LteMeas_MultiEval_ListPy.rst
	Configure_LteMeas_MultiEval_Modulation.rst
	Configure_LteMeas_MultiEval_MsubFrames.rst
	Configure_LteMeas_MultiEval_NsValue.rst
	Configure_LteMeas_MultiEval_Pcc.rst
	Configure_LteMeas_MultiEval_Pdynamics.rst
	Configure_LteMeas_MultiEval_Power.rst
	Configure_LteMeas_MultiEval_RbAllocation.rst
	Configure_LteMeas_MultiEval_Result.rst
	Configure_LteMeas_MultiEval_Scc.rst
	Configure_LteMeas_MultiEval_Scount.rst
	Configure_LteMeas_MultiEval_Spectrum.rst
	Configure_LteMeas_MultiEval_Srs.rst
	Configure_LteMeas_MultiEval_Tmode.rst