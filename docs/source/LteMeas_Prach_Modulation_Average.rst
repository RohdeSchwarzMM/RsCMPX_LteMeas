Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:PRACh:MODulation:AVERage
	single: FETCh:LTE:MEASurement<Instance>:PRACh:MODulation:AVERage
	single: CALCulate:LTE:MEASurement<Instance>:PRACh:MODulation:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:PRACh:MODulation:AVERage
	FETCh:LTE:MEASurement<Instance>:PRACh:MODulation:AVERage
	CALCulate:LTE:MEASurement<Instance>:PRACh:MODulation:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Prach.Modulation.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: