Pdynamics
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Prach.Trace.Pdynamics.PdynamicsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.prach.trace.pdynamics.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_Prach_Trace_Pdynamics_Average.rst
	LteMeas_Prach_Trace_Pdynamics_Current.rst
	LteMeas_Prach_Trace_Pdynamics_Maximum.rst