Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:AVERage
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:AVERage
	FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Trace.SeMask.Rbw.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: