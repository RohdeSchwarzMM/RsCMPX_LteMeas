Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:ULCA:SCC<Nr>:CURRent
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:ULCA:SCC<Nr>:CURRent

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:ULCA:SCC<Nr>:CURRent
	FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:ULCA:SCC<Nr>:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Pmonitor.Ulca.Scc.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: