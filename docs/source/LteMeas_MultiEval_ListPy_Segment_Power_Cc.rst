Cc<CarrierComponentB>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.lteMeas.multiEval.listPy.segment.power.cc.repcap_carrierComponentB_get()
	driver.lteMeas.multiEval.listPy.segment.power.cc.repcap_carrierComponentB_set(repcap.CarrierComponentB.Nr1)





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Segment.Power.Cc.CcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.listPy.segment.power.cc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_ListPy_Segment_Power_Cc_Average.rst
	LteMeas_MultiEval_ListPy_Segment_Power_Cc_Current.rst
	LteMeas_MultiEval_ListPy_Segment_Power_Cc_Maximum.rst
	LteMeas_MultiEval_ListPy_Segment_Power_Cc_Minimum.rst
	LteMeas_MultiEval_ListPy_Segment_Power_Cc_StandardDev.rst