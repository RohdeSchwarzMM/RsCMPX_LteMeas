Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:PRACh:TRACe:EVM:CURRent
	single: FETCh:LTE:MEASurement<Instance>:PRACh:TRACe:EVM:CURRent

.. code-block:: python

	READ:LTE:MEASurement<Instance>:PRACh:TRACe:EVM:CURRent
	FETCh:LTE:MEASurement<Instance>:PRACh:TRACe:EVM:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Prach.Trace.Evm.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: