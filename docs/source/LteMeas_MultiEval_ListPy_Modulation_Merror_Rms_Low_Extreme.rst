Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:RMS:LOW:EXTReme
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:RMS:LOW:EXTReme

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:RMS:LOW:EXTReme
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:RMS:LOW:EXTReme



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Modulation.Merror.Rms.Low.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: