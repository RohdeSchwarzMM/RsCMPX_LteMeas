AeoPower
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:PDYNamics:AEOPower:LEADing
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:PDYNamics:AEOPower:LAGGing

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:PDYNamics:AEOPower:LEADing
	CONFigure:LTE:MEASurement<Instance>:MEValuation:PDYNamics:AEOPower:LAGGing



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.Pdynamics.AeoPower.AeoPowerCls
	:members:
	:undoc-members:
	:noindex: