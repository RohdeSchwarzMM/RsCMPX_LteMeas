Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:PCC:CURRent
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:PCC:CURRent

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:PCC:CURRent
	FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:PCC:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Pmonitor.Pcc.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: