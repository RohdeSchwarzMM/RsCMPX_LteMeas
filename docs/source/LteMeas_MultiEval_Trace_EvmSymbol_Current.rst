Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:TRACe:EVMSymbol:CURRent
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:EVMSymbol:CURRent

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:TRACe:EVMSymbol:CURRent
	FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:EVMSymbol:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Trace.EvmSymbol.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: