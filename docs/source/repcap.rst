RepCaps
=========



Instance (Global)
----------------------------------------------------

.. code-block:: python

	# Setting:
	driver.repcap_instance_set(repcap.Instance.Inst1)
	# Range:
	Inst1 .. Inst16
	# All values (16x):
	Inst1 | Inst2 | Inst3 | Inst4 | Inst5 | Inst6 | Inst7 | Inst8
	Inst9 | Inst10 | Inst11 | Inst12 | Inst13 | Inst14 | Inst15 | Inst16

AbsMarker
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.AbsMarker.Nr1
	# Values (2x):
	Nr1 | Nr2

Area
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Area.Nr1
	# Range:
	Nr1 .. Nr12
	# All values (12x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12

CarrierComponent
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.CarrierComponent.Nr1
	# Values (4x):
	Nr1 | Nr2 | Nr3 | Nr4

CarrierComponentB
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.CarrierComponentB.Nr1
	# Values (4x):
	Nr1 | Nr2 | Nr3 | Nr4

ChannelBw
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.ChannelBw.Bw14
	# Range:
	Bw14 .. Bw200
	# All values (6x):
	Bw14 | Bw30 | Bw50 | Bw100 | Bw150 | Bw200

DeltaMarker
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.DeltaMarker.Nr1
	# Values (2x):
	Nr1 | Nr2

Difference
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Difference.Nr1
	# Values (2x):
	Nr1 | Nr2

EutraBand
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.EutraBand.Nr30
	# Values (2x):
	Nr30 | Nr50

FirstChannelBw
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.FirstChannelBw.Bw100
	# Values (3x):
	Bw100 | Bw150 | Bw200

Limit
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Limit.Nr1
	# Range:
	Nr1 .. Nr12
	# All values (12x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12

MaxRange
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.MaxRange.Nr1
	# Values (2x):
	Nr1 | Nr2

MinRange
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.MinRange.Nr1
	# Values (2x):
	Nr1 | Nr2

Preamble
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Preamble.Nr1
	# Range:
	Nr1 .. Nr400
	# All values (400x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16
	Nr17 | Nr18 | Nr19 | Nr20 | Nr21 | Nr22 | Nr23 | Nr24
	Nr25 | Nr26 | Nr27 | Nr28 | Nr29 | Nr30 | Nr31 | Nr32
	Nr33 | Nr34 | Nr35 | Nr36 | Nr37 | Nr38 | Nr39 | Nr40
	Nr41 | Nr42 | Nr43 | Nr44 | Nr45 | Nr46 | Nr47 | Nr48
	Nr49 | Nr50 | Nr51 | Nr52 | Nr53 | Nr54 | Nr55 | Nr56
	Nr57 | Nr58 | Nr59 | Nr60 | Nr61 | Nr62 | Nr63 | Nr64
	Nr65 | Nr66 | Nr67 | Nr68 | Nr69 | Nr70 | Nr71 | Nr72
	Nr73 | Nr74 | Nr75 | Nr76 | Nr77 | Nr78 | Nr79 | Nr80
	Nr81 | Nr82 | Nr83 | Nr84 | Nr85 | Nr86 | Nr87 | Nr88
	Nr89 | Nr90 | Nr91 | Nr92 | Nr93 | Nr94 | Nr95 | Nr96
	Nr97 | Nr98 | Nr99 | Nr100 | Nr101 | Nr102 | Nr103 | Nr104
	Nr105 | Nr106 | Nr107 | Nr108 | Nr109 | Nr110 | Nr111 | Nr112
	Nr113 | Nr114 | Nr115 | Nr116 | Nr117 | Nr118 | Nr119 | Nr120
	Nr121 | Nr122 | Nr123 | Nr124 | Nr125 | Nr126 | Nr127 | Nr128
	Nr129 | Nr130 | Nr131 | Nr132 | Nr133 | Nr134 | Nr135 | Nr136
	Nr137 | Nr138 | Nr139 | Nr140 | Nr141 | Nr142 | Nr143 | Nr144
	Nr145 | Nr146 | Nr147 | Nr148 | Nr149 | Nr150 | Nr151 | Nr152
	Nr153 | Nr154 | Nr155 | Nr156 | Nr157 | Nr158 | Nr159 | Nr160
	Nr161 | Nr162 | Nr163 | Nr164 | Nr165 | Nr166 | Nr167 | Nr168
	Nr169 | Nr170 | Nr171 | Nr172 | Nr173 | Nr174 | Nr175 | Nr176
	Nr177 | Nr178 | Nr179 | Nr180 | Nr181 | Nr182 | Nr183 | Nr184
	Nr185 | Nr186 | Nr187 | Nr188 | Nr189 | Nr190 | Nr191 | Nr192
	Nr193 | Nr194 | Nr195 | Nr196 | Nr197 | Nr198 | Nr199 | Nr200
	Nr201 | Nr202 | Nr203 | Nr204 | Nr205 | Nr206 | Nr207 | Nr208
	Nr209 | Nr210 | Nr211 | Nr212 | Nr213 | Nr214 | Nr215 | Nr216
	Nr217 | Nr218 | Nr219 | Nr220 | Nr221 | Nr222 | Nr223 | Nr224
	Nr225 | Nr226 | Nr227 | Nr228 | Nr229 | Nr230 | Nr231 | Nr232
	Nr233 | Nr234 | Nr235 | Nr236 | Nr237 | Nr238 | Nr239 | Nr240
	Nr241 | Nr242 | Nr243 | Nr244 | Nr245 | Nr246 | Nr247 | Nr248
	Nr249 | Nr250 | Nr251 | Nr252 | Nr253 | Nr254 | Nr255 | Nr256
	Nr257 | Nr258 | Nr259 | Nr260 | Nr261 | Nr262 | Nr263 | Nr264
	Nr265 | Nr266 | Nr267 | Nr268 | Nr269 | Nr270 | Nr271 | Nr272
	Nr273 | Nr274 | Nr275 | Nr276 | Nr277 | Nr278 | Nr279 | Nr280
	Nr281 | Nr282 | Nr283 | Nr284 | Nr285 | Nr286 | Nr287 | Nr288
	Nr289 | Nr290 | Nr291 | Nr292 | Nr293 | Nr294 | Nr295 | Nr296
	Nr297 | Nr298 | Nr299 | Nr300 | Nr301 | Nr302 | Nr303 | Nr304
	Nr305 | Nr306 | Nr307 | Nr308 | Nr309 | Nr310 | Nr311 | Nr312
	Nr313 | Nr314 | Nr315 | Nr316 | Nr317 | Nr318 | Nr319 | Nr320
	Nr321 | Nr322 | Nr323 | Nr324 | Nr325 | Nr326 | Nr327 | Nr328
	Nr329 | Nr330 | Nr331 | Nr332 | Nr333 | Nr334 | Nr335 | Nr336
	Nr337 | Nr338 | Nr339 | Nr340 | Nr341 | Nr342 | Nr343 | Nr344
	Nr345 | Nr346 | Nr347 | Nr348 | Nr349 | Nr350 | Nr351 | Nr352
	Nr353 | Nr354 | Nr355 | Nr356 | Nr357 | Nr358 | Nr359 | Nr360
	Nr361 | Nr362 | Nr363 | Nr364 | Nr365 | Nr366 | Nr367 | Nr368
	Nr369 | Nr370 | Nr371 | Nr372 | Nr373 | Nr374 | Nr375 | Nr376
	Nr377 | Nr378 | Nr379 | Nr380 | Nr381 | Nr382 | Nr383 | Nr384
	Nr385 | Nr386 | Nr387 | Nr388 | Nr389 | Nr390 | Nr391 | Nr392
	Nr393 | Nr394 | Nr395 | Nr396 | Nr397 | Nr398 | Nr399 | Nr400

PreambleFormat
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.PreambleFormat.Fmt1
	# Range:
	Fmt1 .. Fmt5
	# All values (5x):
	Fmt1 | Fmt2 | Fmt3 | Fmt4 | Fmt5

QAMmodOrder
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.QAMmodOrder.Qam16
	# Values (3x):
	Qam16 | Qam64 | Qam256

RBcount
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.RBcount.Nr1
	# Values (2x):
	Nr1 | Nr2

RBoffset
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.RBoffset.Nr1
	# Values (2x):
	Nr1 | Nr2

RBWkHz
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.RBWkHz.Rbw30
	# Range:
	Rbw30 .. Rbw1000
	# All values (6x):
	Rbw30 | Rbw50 | Rbw100 | Rbw150 | Rbw200 | Rbw1000

Ripple
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Ripple.Nr1
	# Values (2x):
	Nr1 | Nr2

SecondaryCC
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.SecondaryCC.CC1
	# Range:
	CC1 .. CC7
	# All values (7x):
	CC1 | CC2 | CC3 | CC4 | CC5 | CC6 | CC7

SecondChannelBw
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.SecondChannelBw.Bw50
	# Values (4x):
	Bw50 | Bw100 | Bw150 | Bw200

Segment
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Segment.Nr1
	# Range:
	Nr1 .. Nr2000
	# All values (2000x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16
	Nr17 | Nr18 | Nr19 | Nr20 | Nr21 | Nr22 | Nr23 | Nr24
	Nr25 | Nr26 | Nr27 | Nr28 | Nr29 | Nr30 | Nr31 | Nr32
	Nr33 | Nr34 | Nr35 | Nr36 | Nr37 | Nr38 | Nr39 | Nr40
	Nr41 | Nr42 | Nr43 | Nr44 | Nr45 | Nr46 | Nr47 | Nr48
	Nr49 | Nr50 | Nr51 | Nr52 | Nr53 | Nr54 | Nr55 | Nr56
	Nr57 | Nr58 | Nr59 | Nr60 | Nr61 | Nr62 | Nr63 | Nr64
	Nr65 | Nr66 | Nr67 | Nr68 | Nr69 | Nr70 | Nr71 | Nr72
	Nr73 | Nr74 | Nr75 | Nr76 | Nr77 | Nr78 | Nr79 | Nr80
	Nr81 | Nr82 | Nr83 | Nr84 | Nr85 | Nr86 | Nr87 | Nr88
	Nr89 | Nr90 | Nr91 | Nr92 | Nr93 | Nr94 | Nr95 | Nr96
	Nr97 | Nr98 | Nr99 | Nr100 | Nr101 | Nr102 | Nr103 | Nr104
	Nr105 | Nr106 | Nr107 | Nr108 | Nr109 | Nr110 | Nr111 | Nr112
	Nr113 | Nr114 | Nr115 | Nr116 | Nr117 | Nr118 | Nr119 | Nr120
	Nr121 | Nr122 | Nr123 | Nr124 | Nr125 | Nr126 | Nr127 | Nr128
	Nr129 | Nr130 | Nr131 | Nr132 | Nr133 | Nr134 | Nr135 | Nr136
	Nr137 | Nr138 | Nr139 | Nr140 | Nr141 | Nr142 | Nr143 | Nr144
	Nr145 | Nr146 | Nr147 | Nr148 | Nr149 | Nr150 | Nr151 | Nr152
	Nr153 | Nr154 | Nr155 | Nr156 | Nr157 | Nr158 | Nr159 | Nr160
	Nr161 | Nr162 | Nr163 | Nr164 | Nr165 | Nr166 | Nr167 | Nr168
	Nr169 | Nr170 | Nr171 | Nr172 | Nr173 | Nr174 | Nr175 | Nr176
	Nr177 | Nr178 | Nr179 | Nr180 | Nr181 | Nr182 | Nr183 | Nr184
	Nr185 | Nr186 | Nr187 | Nr188 | Nr189 | Nr190 | Nr191 | Nr192
	Nr193 | Nr194 | Nr195 | Nr196 | Nr197 | Nr198 | Nr199 | Nr200
	Nr201 | Nr202 | Nr203 | Nr204 | Nr205 | Nr206 | Nr207 | Nr208
	Nr209 | Nr210 | Nr211 | Nr212 | Nr213 | Nr214 | Nr215 | Nr216
	Nr217 | Nr218 | Nr219 | Nr220 | Nr221 | Nr222 | Nr223 | Nr224
	Nr225 | Nr226 | Nr227 | Nr228 | Nr229 | Nr230 | Nr231 | Nr232
	Nr233 | Nr234 | Nr235 | Nr236 | Nr237 | Nr238 | Nr239 | Nr240
	Nr241 | Nr242 | Nr243 | Nr244 | Nr245 | Nr246 | Nr247 | Nr248
	Nr249 | Nr250 | Nr251 | Nr252 | Nr253 | Nr254 | Nr255 | Nr256
	Nr257 | Nr258 | Nr259 | Nr260 | Nr261 | Nr262 | Nr263 | Nr264
	Nr265 | Nr266 | Nr267 | Nr268 | Nr269 | Nr270 | Nr271 | Nr272
	Nr273 | Nr274 | Nr275 | Nr276 | Nr277 | Nr278 | Nr279 | Nr280
	Nr281 | Nr282 | Nr283 | Nr284 | Nr285 | Nr286 | Nr287 | Nr288
	Nr289 | Nr290 | Nr291 | Nr292 | Nr293 | Nr294 | Nr295 | Nr296
	Nr297 | Nr298 | Nr299 | Nr300 | Nr301 | Nr302 | Nr303 | Nr304
	Nr305 | Nr306 | Nr307 | Nr308 | Nr309 | Nr310 | Nr311 | Nr312
	Nr313 | Nr314 | Nr315 | Nr316 | Nr317 | Nr318 | Nr319 | Nr320
	Nr321 | Nr322 | Nr323 | Nr324 | Nr325 | Nr326 | Nr327 | Nr328
	Nr329 | Nr330 | Nr331 | Nr332 | Nr333 | Nr334 | Nr335 | Nr336
	Nr337 | Nr338 | Nr339 | Nr340 | Nr341 | Nr342 | Nr343 | Nr344
	Nr345 | Nr346 | Nr347 | Nr348 | Nr349 | Nr350 | Nr351 | Nr352
	Nr353 | Nr354 | Nr355 | Nr356 | Nr357 | Nr358 | Nr359 | Nr360
	Nr361 | Nr362 | Nr363 | Nr364 | Nr365 | Nr366 | Nr367 | Nr368
	Nr369 | Nr370 | Nr371 | Nr372 | Nr373 | Nr374 | Nr375 | Nr376
	Nr377 | Nr378 | Nr379 | Nr380 | Nr381 | Nr382 | Nr383 | Nr384
	Nr385 | Nr386 | Nr387 | Nr388 | Nr389 | Nr390 | Nr391 | Nr392
	Nr393 | Nr394 | Nr395 | Nr396 | Nr397 | Nr398 | Nr399 | Nr400
	Nr401 | Nr402 | Nr403 | Nr404 | Nr405 | Nr406 | Nr407 | Nr408
	Nr409 | Nr410 | Nr411 | Nr412 | Nr413 | Nr414 | Nr415 | Nr416
	Nr417 | Nr418 | Nr419 | Nr420 | Nr421 | Nr422 | Nr423 | Nr424
	Nr425 | Nr426 | Nr427 | Nr428 | Nr429 | Nr430 | Nr431 | Nr432
	Nr433 | Nr434 | Nr435 | Nr436 | Nr437 | Nr438 | Nr439 | Nr440
	Nr441 | Nr442 | Nr443 | Nr444 | Nr445 | Nr446 | Nr447 | Nr448
	Nr449 | Nr450 | Nr451 | Nr452 | Nr453 | Nr454 | Nr455 | Nr456
	Nr457 | Nr458 | Nr459 | Nr460 | Nr461 | Nr462 | Nr463 | Nr464
	Nr465 | Nr466 | Nr467 | Nr468 | Nr469 | Nr470 | Nr471 | Nr472
	Nr473 | Nr474 | Nr475 | Nr476 | Nr477 | Nr478 | Nr479 | Nr480
	Nr481 | Nr482 | Nr483 | Nr484 | Nr485 | Nr486 | Nr487 | Nr488
	Nr489 | Nr490 | Nr491 | Nr492 | Nr493 | Nr494 | Nr495 | Nr496
	Nr497 | Nr498 | Nr499 | Nr500 | Nr501 | Nr502 | Nr503 | Nr504
	Nr505 | Nr506 | Nr507 | Nr508 | Nr509 | Nr510 | Nr511 | Nr512
	Nr513 | Nr514 | Nr515 | Nr516 | Nr517 | Nr518 | Nr519 | Nr520
	Nr521 | Nr522 | Nr523 | Nr524 | Nr525 | Nr526 | Nr527 | Nr528
	Nr529 | Nr530 | Nr531 | Nr532 | Nr533 | Nr534 | Nr535 | Nr536
	Nr537 | Nr538 | Nr539 | Nr540 | Nr541 | Nr542 | Nr543 | Nr544
	Nr545 | Nr546 | Nr547 | Nr548 | Nr549 | Nr550 | Nr551 | Nr552
	Nr553 | Nr554 | Nr555 | Nr556 | Nr557 | Nr558 | Nr559 | Nr560
	Nr561 | Nr562 | Nr563 | Nr564 | Nr565 | Nr566 | Nr567 | Nr568
	Nr569 | Nr570 | Nr571 | Nr572 | Nr573 | Nr574 | Nr575 | Nr576
	Nr577 | Nr578 | Nr579 | Nr580 | Nr581 | Nr582 | Nr583 | Nr584
	Nr585 | Nr586 | Nr587 | Nr588 | Nr589 | Nr590 | Nr591 | Nr592
	Nr593 | Nr594 | Nr595 | Nr596 | Nr597 | Nr598 | Nr599 | Nr600
	Nr601 | Nr602 | Nr603 | Nr604 | Nr605 | Nr606 | Nr607 | Nr608
	Nr609 | Nr610 | Nr611 | Nr612 | Nr613 | Nr614 | Nr615 | Nr616
	Nr617 | Nr618 | Nr619 | Nr620 | Nr621 | Nr622 | Nr623 | Nr624
	Nr625 | Nr626 | Nr627 | Nr628 | Nr629 | Nr630 | Nr631 | Nr632
	Nr633 | Nr634 | Nr635 | Nr636 | Nr637 | Nr638 | Nr639 | Nr640
	Nr641 | Nr642 | Nr643 | Nr644 | Nr645 | Nr646 | Nr647 | Nr648
	Nr649 | Nr650 | Nr651 | Nr652 | Nr653 | Nr654 | Nr655 | Nr656
	Nr657 | Nr658 | Nr659 | Nr660 | Nr661 | Nr662 | Nr663 | Nr664
	Nr665 | Nr666 | Nr667 | Nr668 | Nr669 | Nr670 | Nr671 | Nr672
	Nr673 | Nr674 | Nr675 | Nr676 | Nr677 | Nr678 | Nr679 | Nr680
	Nr681 | Nr682 | Nr683 | Nr684 | Nr685 | Nr686 | Nr687 | Nr688
	Nr689 | Nr690 | Nr691 | Nr692 | Nr693 | Nr694 | Nr695 | Nr696
	Nr697 | Nr698 | Nr699 | Nr700 | Nr701 | Nr702 | Nr703 | Nr704
	Nr705 | Nr706 | Nr707 | Nr708 | Nr709 | Nr710 | Nr711 | Nr712
	Nr713 | Nr714 | Nr715 | Nr716 | Nr717 | Nr718 | Nr719 | Nr720
	Nr721 | Nr722 | Nr723 | Nr724 | Nr725 | Nr726 | Nr727 | Nr728
	Nr729 | Nr730 | Nr731 | Nr732 | Nr733 | Nr734 | Nr735 | Nr736
	Nr737 | Nr738 | Nr739 | Nr740 | Nr741 | Nr742 | Nr743 | Nr744
	Nr745 | Nr746 | Nr747 | Nr748 | Nr749 | Nr750 | Nr751 | Nr752
	Nr753 | Nr754 | Nr755 | Nr756 | Nr757 | Nr758 | Nr759 | Nr760
	Nr761 | Nr762 | Nr763 | Nr764 | Nr765 | Nr766 | Nr767 | Nr768
	Nr769 | Nr770 | Nr771 | Nr772 | Nr773 | Nr774 | Nr775 | Nr776
	Nr777 | Nr778 | Nr779 | Nr780 | Nr781 | Nr782 | Nr783 | Nr784
	Nr785 | Nr786 | Nr787 | Nr788 | Nr789 | Nr790 | Nr791 | Nr792
	Nr793 | Nr794 | Nr795 | Nr796 | Nr797 | Nr798 | Nr799 | Nr800
	Nr801 | Nr802 | Nr803 | Nr804 | Nr805 | Nr806 | Nr807 | Nr808
	Nr809 | Nr810 | Nr811 | Nr812 | Nr813 | Nr814 | Nr815 | Nr816
	Nr817 | Nr818 | Nr819 | Nr820 | Nr821 | Nr822 | Nr823 | Nr824
	Nr825 | Nr826 | Nr827 | Nr828 | Nr829 | Nr830 | Nr831 | Nr832
	Nr833 | Nr834 | Nr835 | Nr836 | Nr837 | Nr838 | Nr839 | Nr840
	Nr841 | Nr842 | Nr843 | Nr844 | Nr845 | Nr846 | Nr847 | Nr848
	Nr849 | Nr850 | Nr851 | Nr852 | Nr853 | Nr854 | Nr855 | Nr856
	Nr857 | Nr858 | Nr859 | Nr860 | Nr861 | Nr862 | Nr863 | Nr864
	Nr865 | Nr866 | Nr867 | Nr868 | Nr869 | Nr870 | Nr871 | Nr872
	Nr873 | Nr874 | Nr875 | Nr876 | Nr877 | Nr878 | Nr879 | Nr880
	Nr881 | Nr882 | Nr883 | Nr884 | Nr885 | Nr886 | Nr887 | Nr888
	Nr889 | Nr890 | Nr891 | Nr892 | Nr893 | Nr894 | Nr895 | Nr896
	Nr897 | Nr898 | Nr899 | Nr900 | Nr901 | Nr902 | Nr903 | Nr904
	Nr905 | Nr906 | Nr907 | Nr908 | Nr909 | Nr910 | Nr911 | Nr912
	Nr913 | Nr914 | Nr915 | Nr916 | Nr917 | Nr918 | Nr919 | Nr920
	Nr921 | Nr922 | Nr923 | Nr924 | Nr925 | Nr926 | Nr927 | Nr928
	Nr929 | Nr930 | Nr931 | Nr932 | Nr933 | Nr934 | Nr935 | Nr936
	Nr937 | Nr938 | Nr939 | Nr940 | Nr941 | Nr942 | Nr943 | Nr944
	Nr945 | Nr946 | Nr947 | Nr948 | Nr949 | Nr950 | Nr951 | Nr952
	Nr953 | Nr954 | Nr955 | Nr956 | Nr957 | Nr958 | Nr959 | Nr960
	Nr961 | Nr962 | Nr963 | Nr964 | Nr965 | Nr966 | Nr967 | Nr968
	Nr969 | Nr970 | Nr971 | Nr972 | Nr973 | Nr974 | Nr975 | Nr976
	Nr977 | Nr978 | Nr979 | Nr980 | Nr981 | Nr982 | Nr983 | Nr984
	Nr985 | Nr986 | Nr987 | Nr988 | Nr989 | Nr990 | Nr991 | Nr992
	Nr993 | Nr994 | Nr995 | Nr996 | Nr997 | Nr998 | Nr999 | Nr1000
	Nr1001 | Nr1002 | Nr1003 | Nr1004 | Nr1005 | Nr1006 | Nr1007 | Nr1008
	Nr1009 | Nr1010 | Nr1011 | Nr1012 | Nr1013 | Nr1014 | Nr1015 | Nr1016
	Nr1017 | Nr1018 | Nr1019 | Nr1020 | Nr1021 | Nr1022 | Nr1023 | Nr1024
	Nr1025 | Nr1026 | Nr1027 | Nr1028 | Nr1029 | Nr1030 | Nr1031 | Nr1032
	Nr1033 | Nr1034 | Nr1035 | Nr1036 | Nr1037 | Nr1038 | Nr1039 | Nr1040
	Nr1041 | Nr1042 | Nr1043 | Nr1044 | Nr1045 | Nr1046 | Nr1047 | Nr1048
	Nr1049 | Nr1050 | Nr1051 | Nr1052 | Nr1053 | Nr1054 | Nr1055 | Nr1056
	Nr1057 | Nr1058 | Nr1059 | Nr1060 | Nr1061 | Nr1062 | Nr1063 | Nr1064
	Nr1065 | Nr1066 | Nr1067 | Nr1068 | Nr1069 | Nr1070 | Nr1071 | Nr1072
	Nr1073 | Nr1074 | Nr1075 | Nr1076 | Nr1077 | Nr1078 | Nr1079 | Nr1080
	Nr1081 | Nr1082 | Nr1083 | Nr1084 | Nr1085 | Nr1086 | Nr1087 | Nr1088
	Nr1089 | Nr1090 | Nr1091 | Nr1092 | Nr1093 | Nr1094 | Nr1095 | Nr1096
	Nr1097 | Nr1098 | Nr1099 | Nr1100 | Nr1101 | Nr1102 | Nr1103 | Nr1104
	Nr1105 | Nr1106 | Nr1107 | Nr1108 | Nr1109 | Nr1110 | Nr1111 | Nr1112
	Nr1113 | Nr1114 | Nr1115 | Nr1116 | Nr1117 | Nr1118 | Nr1119 | Nr1120
	Nr1121 | Nr1122 | Nr1123 | Nr1124 | Nr1125 | Nr1126 | Nr1127 | Nr1128
	Nr1129 | Nr1130 | Nr1131 | Nr1132 | Nr1133 | Nr1134 | Nr1135 | Nr1136
	Nr1137 | Nr1138 | Nr1139 | Nr1140 | Nr1141 | Nr1142 | Nr1143 | Nr1144
	Nr1145 | Nr1146 | Nr1147 | Nr1148 | Nr1149 | Nr1150 | Nr1151 | Nr1152
	Nr1153 | Nr1154 | Nr1155 | Nr1156 | Nr1157 | Nr1158 | Nr1159 | Nr1160
	Nr1161 | Nr1162 | Nr1163 | Nr1164 | Nr1165 | Nr1166 | Nr1167 | Nr1168
	Nr1169 | Nr1170 | Nr1171 | Nr1172 | Nr1173 | Nr1174 | Nr1175 | Nr1176
	Nr1177 | Nr1178 | Nr1179 | Nr1180 | Nr1181 | Nr1182 | Nr1183 | Nr1184
	Nr1185 | Nr1186 | Nr1187 | Nr1188 | Nr1189 | Nr1190 | Nr1191 | Nr1192
	Nr1193 | Nr1194 | Nr1195 | Nr1196 | Nr1197 | Nr1198 | Nr1199 | Nr1200
	Nr1201 | Nr1202 | Nr1203 | Nr1204 | Nr1205 | Nr1206 | Nr1207 | Nr1208
	Nr1209 | Nr1210 | Nr1211 | Nr1212 | Nr1213 | Nr1214 | Nr1215 | Nr1216
	Nr1217 | Nr1218 | Nr1219 | Nr1220 | Nr1221 | Nr1222 | Nr1223 | Nr1224
	Nr1225 | Nr1226 | Nr1227 | Nr1228 | Nr1229 | Nr1230 | Nr1231 | Nr1232
	Nr1233 | Nr1234 | Nr1235 | Nr1236 | Nr1237 | Nr1238 | Nr1239 | Nr1240
	Nr1241 | Nr1242 | Nr1243 | Nr1244 | Nr1245 | Nr1246 | Nr1247 | Nr1248
	Nr1249 | Nr1250 | Nr1251 | Nr1252 | Nr1253 | Nr1254 | Nr1255 | Nr1256
	Nr1257 | Nr1258 | Nr1259 | Nr1260 | Nr1261 | Nr1262 | Nr1263 | Nr1264
	Nr1265 | Nr1266 | Nr1267 | Nr1268 | Nr1269 | Nr1270 | Nr1271 | Nr1272
	Nr1273 | Nr1274 | Nr1275 | Nr1276 | Nr1277 | Nr1278 | Nr1279 | Nr1280
	Nr1281 | Nr1282 | Nr1283 | Nr1284 | Nr1285 | Nr1286 | Nr1287 | Nr1288
	Nr1289 | Nr1290 | Nr1291 | Nr1292 | Nr1293 | Nr1294 | Nr1295 | Nr1296
	Nr1297 | Nr1298 | Nr1299 | Nr1300 | Nr1301 | Nr1302 | Nr1303 | Nr1304
	Nr1305 | Nr1306 | Nr1307 | Nr1308 | Nr1309 | Nr1310 | Nr1311 | Nr1312
	Nr1313 | Nr1314 | Nr1315 | Nr1316 | Nr1317 | Nr1318 | Nr1319 | Nr1320
	Nr1321 | Nr1322 | Nr1323 | Nr1324 | Nr1325 | Nr1326 | Nr1327 | Nr1328
	Nr1329 | Nr1330 | Nr1331 | Nr1332 | Nr1333 | Nr1334 | Nr1335 | Nr1336
	Nr1337 | Nr1338 | Nr1339 | Nr1340 | Nr1341 | Nr1342 | Nr1343 | Nr1344
	Nr1345 | Nr1346 | Nr1347 | Nr1348 | Nr1349 | Nr1350 | Nr1351 | Nr1352
	Nr1353 | Nr1354 | Nr1355 | Nr1356 | Nr1357 | Nr1358 | Nr1359 | Nr1360
	Nr1361 | Nr1362 | Nr1363 | Nr1364 | Nr1365 | Nr1366 | Nr1367 | Nr1368
	Nr1369 | Nr1370 | Nr1371 | Nr1372 | Nr1373 | Nr1374 | Nr1375 | Nr1376
	Nr1377 | Nr1378 | Nr1379 | Nr1380 | Nr1381 | Nr1382 | Nr1383 | Nr1384
	Nr1385 | Nr1386 | Nr1387 | Nr1388 | Nr1389 | Nr1390 | Nr1391 | Nr1392
	Nr1393 | Nr1394 | Nr1395 | Nr1396 | Nr1397 | Nr1398 | Nr1399 | Nr1400
	Nr1401 | Nr1402 | Nr1403 | Nr1404 | Nr1405 | Nr1406 | Nr1407 | Nr1408
	Nr1409 | Nr1410 | Nr1411 | Nr1412 | Nr1413 | Nr1414 | Nr1415 | Nr1416
	Nr1417 | Nr1418 | Nr1419 | Nr1420 | Nr1421 | Nr1422 | Nr1423 | Nr1424
	Nr1425 | Nr1426 | Nr1427 | Nr1428 | Nr1429 | Nr1430 | Nr1431 | Nr1432
	Nr1433 | Nr1434 | Nr1435 | Nr1436 | Nr1437 | Nr1438 | Nr1439 | Nr1440
	Nr1441 | Nr1442 | Nr1443 | Nr1444 | Nr1445 | Nr1446 | Nr1447 | Nr1448
	Nr1449 | Nr1450 | Nr1451 | Nr1452 | Nr1453 | Nr1454 | Nr1455 | Nr1456
	Nr1457 | Nr1458 | Nr1459 | Nr1460 | Nr1461 | Nr1462 | Nr1463 | Nr1464
	Nr1465 | Nr1466 | Nr1467 | Nr1468 | Nr1469 | Nr1470 | Nr1471 | Nr1472
	Nr1473 | Nr1474 | Nr1475 | Nr1476 | Nr1477 | Nr1478 | Nr1479 | Nr1480
	Nr1481 | Nr1482 | Nr1483 | Nr1484 | Nr1485 | Nr1486 | Nr1487 | Nr1488
	Nr1489 | Nr1490 | Nr1491 | Nr1492 | Nr1493 | Nr1494 | Nr1495 | Nr1496
	Nr1497 | Nr1498 | Nr1499 | Nr1500 | Nr1501 | Nr1502 | Nr1503 | Nr1504
	Nr1505 | Nr1506 | Nr1507 | Nr1508 | Nr1509 | Nr1510 | Nr1511 | Nr1512
	Nr1513 | Nr1514 | Nr1515 | Nr1516 | Nr1517 | Nr1518 | Nr1519 | Nr1520
	Nr1521 | Nr1522 | Nr1523 | Nr1524 | Nr1525 | Nr1526 | Nr1527 | Nr1528
	Nr1529 | Nr1530 | Nr1531 | Nr1532 | Nr1533 | Nr1534 | Nr1535 | Nr1536
	Nr1537 | Nr1538 | Nr1539 | Nr1540 | Nr1541 | Nr1542 | Nr1543 | Nr1544
	Nr1545 | Nr1546 | Nr1547 | Nr1548 | Nr1549 | Nr1550 | Nr1551 | Nr1552
	Nr1553 | Nr1554 | Nr1555 | Nr1556 | Nr1557 | Nr1558 | Nr1559 | Nr1560
	Nr1561 | Nr1562 | Nr1563 | Nr1564 | Nr1565 | Nr1566 | Nr1567 | Nr1568
	Nr1569 | Nr1570 | Nr1571 | Nr1572 | Nr1573 | Nr1574 | Nr1575 | Nr1576
	Nr1577 | Nr1578 | Nr1579 | Nr1580 | Nr1581 | Nr1582 | Nr1583 | Nr1584
	Nr1585 | Nr1586 | Nr1587 | Nr1588 | Nr1589 | Nr1590 | Nr1591 | Nr1592
	Nr1593 | Nr1594 | Nr1595 | Nr1596 | Nr1597 | Nr1598 | Nr1599 | Nr1600
	Nr1601 | Nr1602 | Nr1603 | Nr1604 | Nr1605 | Nr1606 | Nr1607 | Nr1608
	Nr1609 | Nr1610 | Nr1611 | Nr1612 | Nr1613 | Nr1614 | Nr1615 | Nr1616
	Nr1617 | Nr1618 | Nr1619 | Nr1620 | Nr1621 | Nr1622 | Nr1623 | Nr1624
	Nr1625 | Nr1626 | Nr1627 | Nr1628 | Nr1629 | Nr1630 | Nr1631 | Nr1632
	Nr1633 | Nr1634 | Nr1635 | Nr1636 | Nr1637 | Nr1638 | Nr1639 | Nr1640
	Nr1641 | Nr1642 | Nr1643 | Nr1644 | Nr1645 | Nr1646 | Nr1647 | Nr1648
	Nr1649 | Nr1650 | Nr1651 | Nr1652 | Nr1653 | Nr1654 | Nr1655 | Nr1656
	Nr1657 | Nr1658 | Nr1659 | Nr1660 | Nr1661 | Nr1662 | Nr1663 | Nr1664
	Nr1665 | Nr1666 | Nr1667 | Nr1668 | Nr1669 | Nr1670 | Nr1671 | Nr1672
	Nr1673 | Nr1674 | Nr1675 | Nr1676 | Nr1677 | Nr1678 | Nr1679 | Nr1680
	Nr1681 | Nr1682 | Nr1683 | Nr1684 | Nr1685 | Nr1686 | Nr1687 | Nr1688
	Nr1689 | Nr1690 | Nr1691 | Nr1692 | Nr1693 | Nr1694 | Nr1695 | Nr1696
	Nr1697 | Nr1698 | Nr1699 | Nr1700 | Nr1701 | Nr1702 | Nr1703 | Nr1704
	Nr1705 | Nr1706 | Nr1707 | Nr1708 | Nr1709 | Nr1710 | Nr1711 | Nr1712
	Nr1713 | Nr1714 | Nr1715 | Nr1716 | Nr1717 | Nr1718 | Nr1719 | Nr1720
	Nr1721 | Nr1722 | Nr1723 | Nr1724 | Nr1725 | Nr1726 | Nr1727 | Nr1728
	Nr1729 | Nr1730 | Nr1731 | Nr1732 | Nr1733 | Nr1734 | Nr1735 | Nr1736
	Nr1737 | Nr1738 | Nr1739 | Nr1740 | Nr1741 | Nr1742 | Nr1743 | Nr1744
	Nr1745 | Nr1746 | Nr1747 | Nr1748 | Nr1749 | Nr1750 | Nr1751 | Nr1752
	Nr1753 | Nr1754 | Nr1755 | Nr1756 | Nr1757 | Nr1758 | Nr1759 | Nr1760
	Nr1761 | Nr1762 | Nr1763 | Nr1764 | Nr1765 | Nr1766 | Nr1767 | Nr1768
	Nr1769 | Nr1770 | Nr1771 | Nr1772 | Nr1773 | Nr1774 | Nr1775 | Nr1776
	Nr1777 | Nr1778 | Nr1779 | Nr1780 | Nr1781 | Nr1782 | Nr1783 | Nr1784
	Nr1785 | Nr1786 | Nr1787 | Nr1788 | Nr1789 | Nr1790 | Nr1791 | Nr1792
	Nr1793 | Nr1794 | Nr1795 | Nr1796 | Nr1797 | Nr1798 | Nr1799 | Nr1800
	Nr1801 | Nr1802 | Nr1803 | Nr1804 | Nr1805 | Nr1806 | Nr1807 | Nr1808
	Nr1809 | Nr1810 | Nr1811 | Nr1812 | Nr1813 | Nr1814 | Nr1815 | Nr1816
	Nr1817 | Nr1818 | Nr1819 | Nr1820 | Nr1821 | Nr1822 | Nr1823 | Nr1824
	Nr1825 | Nr1826 | Nr1827 | Nr1828 | Nr1829 | Nr1830 | Nr1831 | Nr1832
	Nr1833 | Nr1834 | Nr1835 | Nr1836 | Nr1837 | Nr1838 | Nr1839 | Nr1840
	Nr1841 | Nr1842 | Nr1843 | Nr1844 | Nr1845 | Nr1846 | Nr1847 | Nr1848
	Nr1849 | Nr1850 | Nr1851 | Nr1852 | Nr1853 | Nr1854 | Nr1855 | Nr1856
	Nr1857 | Nr1858 | Nr1859 | Nr1860 | Nr1861 | Nr1862 | Nr1863 | Nr1864
	Nr1865 | Nr1866 | Nr1867 | Nr1868 | Nr1869 | Nr1870 | Nr1871 | Nr1872
	Nr1873 | Nr1874 | Nr1875 | Nr1876 | Nr1877 | Nr1878 | Nr1879 | Nr1880
	Nr1881 | Nr1882 | Nr1883 | Nr1884 | Nr1885 | Nr1886 | Nr1887 | Nr1888
	Nr1889 | Nr1890 | Nr1891 | Nr1892 | Nr1893 | Nr1894 | Nr1895 | Nr1896
	Nr1897 | Nr1898 | Nr1899 | Nr1900 | Nr1901 | Nr1902 | Nr1903 | Nr1904
	Nr1905 | Nr1906 | Nr1907 | Nr1908 | Nr1909 | Nr1910 | Nr1911 | Nr1912
	Nr1913 | Nr1914 | Nr1915 | Nr1916 | Nr1917 | Nr1918 | Nr1919 | Nr1920
	Nr1921 | Nr1922 | Nr1923 | Nr1924 | Nr1925 | Nr1926 | Nr1927 | Nr1928
	Nr1929 | Nr1930 | Nr1931 | Nr1932 | Nr1933 | Nr1934 | Nr1935 | Nr1936
	Nr1937 | Nr1938 | Nr1939 | Nr1940 | Nr1941 | Nr1942 | Nr1943 | Nr1944
	Nr1945 | Nr1946 | Nr1947 | Nr1948 | Nr1949 | Nr1950 | Nr1951 | Nr1952
	Nr1953 | Nr1954 | Nr1955 | Nr1956 | Nr1957 | Nr1958 | Nr1959 | Nr1960
	Nr1961 | Nr1962 | Nr1963 | Nr1964 | Nr1965 | Nr1966 | Nr1967 | Nr1968
	Nr1969 | Nr1970 | Nr1971 | Nr1972 | Nr1973 | Nr1974 | Nr1975 | Nr1976
	Nr1977 | Nr1978 | Nr1979 | Nr1980 | Nr1981 | Nr1982 | Nr1983 | Nr1984
	Nr1985 | Nr1986 | Nr1987 | Nr1988 | Nr1989 | Nr1990 | Nr1991 | Nr1992
	Nr1993 | Nr1994 | Nr1995 | Nr1996 | Nr1997 | Nr1998 | Nr1999 | Nr2000

Table
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Table.Nr1
	# Range:
	Nr1 .. Nr5
	# All values (5x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5

ThirdChannelBw
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.ThirdChannelBw.Bw100
	# Values (3x):
	Bw100 | Bw150 | Bw200

UtraAdjChannel
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.UtraAdjChannel.Ch1
	# Values (2x):
	Ch1 | Ch2

