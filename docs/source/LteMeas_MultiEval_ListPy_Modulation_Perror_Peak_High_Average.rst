Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:PEAK:HIGH:AVERage
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:PEAK:HIGH:AVERage

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:PEAK:HIGH:AVERage
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:PEAK:HIGH:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Modulation.Perror.Peak.High.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: