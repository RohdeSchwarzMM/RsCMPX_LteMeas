Spectrum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:SCOunt:SPECtrum:SEMask
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:SCOunt:SPECtrum:ACLR

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:SCOunt:SPECtrum:SEMask
	CONFigure:LTE:MEASurement<Instance>:MEValuation:SCOunt:SPECtrum:ACLR



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.Scount.Spectrum.SpectrumCls
	:members:
	:undoc-members:
	:noindex: