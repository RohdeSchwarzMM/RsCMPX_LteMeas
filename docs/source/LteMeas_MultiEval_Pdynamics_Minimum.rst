Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PDYNamics:MINimum
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PDYNamics:MINimum
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:PDYNamics:MINimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PDYNamics:MINimum
	FETCh:LTE:MEASurement<Instance>:MEValuation:PDYNamics:MINimum
	CALCulate:LTE:MEASurement<Instance>:MEValuation:PDYNamics:MINimum



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Pdynamics.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: