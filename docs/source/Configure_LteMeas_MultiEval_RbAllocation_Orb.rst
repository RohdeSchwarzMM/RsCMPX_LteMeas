Orb
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:RBALlocation:ORB:PSCCh
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:RBALlocation:ORB:PSSCh
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:RBALlocation:ORB

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:RBALlocation:ORB:PSCCh
	CONFigure:LTE:MEASurement<Instance>:MEValuation:RBALlocation:ORB:PSSCh
	CONFigure:LTE:MEASurement<Instance>:MEValuation:RBALlocation:ORB



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.RbAllocation.Orb.OrbCls
	:members:
	:undoc-members:
	:noindex: