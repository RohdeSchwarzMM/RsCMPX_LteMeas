Peak
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:AMARker<No>:EVMagnitude:PEAK

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:AMARker<No>:EVMagnitude:PEAK



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Amarker.EvMagnitude.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex: