EsFlatness
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:TRACe:ESFLatness
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:ESFLatness

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:TRACe:ESFLatness
	FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:ESFLatness



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Trace.EsFlatness.EsFlatnessCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.trace.esFlatness.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_Trace_EsFlatness_Phase.rst