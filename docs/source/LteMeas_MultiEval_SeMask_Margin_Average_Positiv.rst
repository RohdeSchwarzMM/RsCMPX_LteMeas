Positiv
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:SEMask:MARGin:AVERage:POSitiv

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:SEMask:MARGin:AVERage:POSitiv



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.SeMask.Margin.Average.Positiv.PositivCls
	:members:
	:undoc-members:
	:noindex: