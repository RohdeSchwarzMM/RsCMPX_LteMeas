Rbw
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:MEASurement<Instance>:MEValuation:SPECtrum:SEMask:RBW:USED

.. code-block:: python

	SENSe:LTE:MEASurement<Instance>:MEValuation:SPECtrum:SEMask:RBW:USED



.. autoclass:: RsCMPX_LteMeas.Implementations.Sense.LteMeas.MultiEval.Spectrum.SeMask.Rbw.RbwCls
	:members:
	:undoc-members:
	:noindex: