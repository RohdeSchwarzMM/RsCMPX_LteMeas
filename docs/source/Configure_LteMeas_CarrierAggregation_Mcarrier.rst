Mcarrier
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:CAGGregation:MCARrier:ENHanced
	single: CONFigure:LTE:MEASurement<Instance>:CAGGregation:MCARrier

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:CAGGregation:MCARrier:ENHanced
	CONFigure:LTE:MEASurement<Instance>:CAGGregation:MCARrier



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.CarrierAggregation.Mcarrier.McarrierCls
	:members:
	:undoc-members:
	:noindex: