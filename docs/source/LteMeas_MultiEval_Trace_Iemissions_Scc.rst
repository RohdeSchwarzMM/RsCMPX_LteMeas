Scc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:TRACe:IEMissions:SCC
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:IEMissions:SCC

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:TRACe:IEMissions:SCC
	FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:IEMissions:SCC



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Trace.Iemissions.Scc.SccCls
	:members:
	:undoc-members:
	:noindex: