Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:SCC:CURRent
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:SCC:CURRent

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:SCC:CURRent
	FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:SCC:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Pmonitor.Scc.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: