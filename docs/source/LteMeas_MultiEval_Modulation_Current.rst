Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:MODulation:CURRent
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:MODulation:CURRent
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:MODulation:CURRent

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:MODulation:CURRent
	FETCh:LTE:MEASurement<Instance>:MEValuation:MODulation:CURRent
	CALCulate:LTE:MEASurement<Instance>:MEValuation:MODulation:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Modulation.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: