Pcc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:TRACe:RBATable:ULCA[:PCC]
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:RBATable:ULCA[:PCC]

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:TRACe:RBATable:ULCA[:PCC]
	FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:RBATable:ULCA[:PCC]



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Trace.RbaTable.Ulca.Pcc.PccCls
	:members:
	:undoc-members:
	:noindex: