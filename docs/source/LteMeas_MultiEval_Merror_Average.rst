Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:MERRor:AVERage
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:MERRor:AVERage
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:MERRor:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:MERRor:AVERage
	FETCh:LTE:MEASurement<Instance>:MEValuation:MERRor:AVERage
	CALCulate:LTE:MEASurement<Instance>:MEValuation:MERRor:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Merror.Average.AverageCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.merror.average.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_Merror_Average_Nref.rst