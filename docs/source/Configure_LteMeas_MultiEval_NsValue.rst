NsValue
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:NSValue:CAGGregation
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:NSValue

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:NSValue:CAGGregation
	CONFigure:LTE:MEASurement<Instance>:MEValuation:NSValue



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.NsValue.NsValueCls
	:members:
	:undoc-members:
	:noindex: