Margin
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.InbandEmission.Scc.Margin.MarginCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.inbandEmission.scc.margin.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_InbandEmission_Scc_Margin_Average.rst
	LteMeas_MultiEval_InbandEmission_Scc_Margin_Current.rst
	LteMeas_MultiEval_InbandEmission_Scc_Margin_Extreme.rst
	LteMeas_MultiEval_InbandEmission_Scc_Margin_StandardDev.rst