Srs
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:LTE:MEASurement<Instance>:SRS:THReshold
	single: TRIGger:LTE:MEASurement<Instance>:SRS:SLOPe
	single: TRIGger:LTE:MEASurement<Instance>:SRS:TOUT
	single: TRIGger:LTE:MEASurement<Instance>:SRS:MGAP

.. code-block:: python

	TRIGger:LTE:MEASurement<Instance>:SRS:THReshold
	TRIGger:LTE:MEASurement<Instance>:SRS:SLOPe
	TRIGger:LTE:MEASurement<Instance>:SRS:TOUT
	TRIGger:LTE:MEASurement<Instance>:SRS:MGAP



.. autoclass:: RsCMPX_LteMeas.Implementations.Trigger.LteMeas.Srs.SrsCls
	:members:
	:undoc-members:
	:noindex: