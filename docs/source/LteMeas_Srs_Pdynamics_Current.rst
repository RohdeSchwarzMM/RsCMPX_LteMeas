Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:SRS:PDYNamics:CURRent
	single: FETCh:LTE:MEASurement<Instance>:SRS:PDYNamics:CURRent
	single: CALCulate:LTE:MEASurement<Instance>:SRS:PDYNamics:CURRent

.. code-block:: python

	READ:LTE:MEASurement<Instance>:SRS:PDYNamics:CURRent
	FETCh:LTE:MEASurement<Instance>:SRS:PDYNamics:CURRent
	CALCulate:LTE:MEASurement<Instance>:SRS:PDYNamics:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Srs.Pdynamics.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: