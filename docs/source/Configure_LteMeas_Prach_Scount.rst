Scount
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:SCOunt:MODulation
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:SCOunt:PDYNamics

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:PRACh:SCOunt:MODulation
	CONFigure:LTE:MEASurement<Instance>:PRACh:SCOunt:PDYNamics



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.Prach.Scount.ScountCls
	:members:
	:undoc-members:
	:noindex: