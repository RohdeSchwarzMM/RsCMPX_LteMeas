Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:PRACh:EVMSymbol:AVERage
	single: FETCh:LTE:MEASurement<Instance>:PRACh:EVMSymbol:AVERage
	single: CALCulate:LTE:MEASurement<Instance>:PRACh:EVMSymbol:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:PRACh:EVMSymbol:AVERage
	FETCh:LTE:MEASurement<Instance>:PRACh:EVMSymbol:AVERage
	CALCulate:LTE:MEASurement<Instance>:PRACh:EVMSymbol:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Prach.EvmSymbol.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: