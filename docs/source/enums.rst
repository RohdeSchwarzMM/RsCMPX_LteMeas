Enums
=========

Band
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Band.OB1
	# Last value:
	value = enums.Band.OB9
	# All values (63x):
	OB1 | OB10 | OB11 | OB12 | OB13 | OB14 | OB15 | OB16
	OB17 | OB18 | OB19 | OB2 | OB20 | OB21 | OB22 | OB23
	OB24 | OB25 | OB250 | OB26 | OB27 | OB28 | OB3 | OB30
	OB31 | OB33 | OB34 | OB35 | OB36 | OB37 | OB38 | OB39
	OB4 | OB40 | OB41 | OB42 | OB43 | OB44 | OB45 | OB46
	OB47 | OB48 | OB49 | OB5 | OB50 | OB51 | OB52 | OB53
	OB6 | OB65 | OB66 | OB68 | OB7 | OB70 | OB71 | OB72
	OB73 | OB74 | OB8 | OB85 | OB87 | OB88 | OB9

BandwidthNarrow
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BandwidthNarrow.M010
	# All values (4x):
	M010 | M020 | M040 | M080

CarrAggrLocalOscLocation
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CarrAggrLocalOscLocation.AUTO
	# All values (3x):
	AUTO | CACB | CECC

CarrAggrMaping
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.CarrAggrMaping.INV
	# Last value:
	value = enums.CarrAggrMaping.SCC7
	# All values (9x):
	INV | PCC | SCC1 | SCC2 | SCC3 | SCC4 | SCC5 | SCC6
	SCC7

CarrAggrMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CarrAggrMode.ICD
	# All values (4x):
	ICD | ICE | INTRaband | OFF

ChannelBandwidth
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ChannelBandwidth.B014
	# All values (6x):
	B014 | B030 | B050 | B100 | B150 | B200

ChannelTypeDetection
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ChannelTypeDetection.AUTO
	# All values (3x):
	AUTO | PUCCh | PUSCh

ChannelTypeVewFilter
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ChannelTypeVewFilter.OFF
	# All values (4x):
	OFF | ON | PUCCh | PUSCh

CmwsConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.CmwsConnector.R11
	# Last value:
	value = enums.CmwsConnector.RB8
	# All values (48x):
	R11 | R12 | R13 | R14 | R15 | R16 | R17 | R18
	R21 | R22 | R23 | R24 | R25 | R26 | R27 | R28
	R31 | R32 | R33 | R34 | R35 | R36 | R37 | R38
	R41 | R42 | R43 | R44 | R45 | R46 | R47 | R48
	RA1 | RA2 | RA3 | RA4 | RA5 | RA6 | RA7 | RA8
	RB1 | RB2 | RB3 | RB4 | RB5 | RB6 | RB7 | RB8

CyclicPrefix
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CyclicPrefix.EXTended
	# All values (2x):
	EXTended | NORMal

FrameStructure
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FrameStructure.T1
	# All values (2x):
	T1 | T2

LaggingExclPeriod
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LaggingExclPeriod.MS05
	# All values (3x):
	MS05 | MS25 | OFF

LeadingExclPeriod
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LeadingExclPeriod.MS25
	# All values (2x):
	MS25 | OFF

ListMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ListMode.ONCE
	# All values (2x):
	ONCE | SEGMent

LocalOscLocation
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LocalOscLocation.CCB
	# All values (2x):
	CCB | CN

LowHigh
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LowHigh.HIGH
	# All values (2x):
	HIGH | LOW

MeasCarrier
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasCarrier.PCC
	# All values (2x):
	PCC | SCC1

MeasCarrierB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasCarrierB.PCC
	# All values (8x):
	PCC | SCC1 | SCC2 | SCC3 | SCC4 | SCC5 | SCC6 | SCC7

MeasCarrierEnhanced
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasCarrierEnhanced.CC1
	# All values (4x):
	CC1 | CC2 | CC3 | CC4

MeasFilter
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasFilter.BANDpass
	# All values (2x):
	BANDpass | GAUSs

MeasurementMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasurementMode.MELMode
	# All values (3x):
	MELMode | NORMal | TMODe

MeasureSlot
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasureSlot.ALL
	# All values (3x):
	ALL | MS0 | MS1

MevAcquisitionMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MevAcquisitionMode.SLOT
	# All values (2x):
	SLOT | SUBFrame

Mode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Mode.FDD
	# All values (2x):
	FDD | TDD

ModScheme
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ModScheme.AUTO
	# All values (5x):
	AUTO | Q16 | Q256 | Q64 | QPSK

Modulation
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Modulation.Q16
	# All values (4x):
	Q16 | Q256 | Q64 | QPSK

NetworkSharing
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NetworkSharing.FSHared
	# All values (3x):
	FSHared | NSHared | OCONnection

NetworkSigValue
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.NetworkSigValue.NS01
	# Last value:
	value = enums.NetworkSigValue.NS32
	# All values (32x):
	NS01 | NS02 | NS03 | NS04 | NS05 | NS06 | NS07 | NS08
	NS09 | NS10 | NS11 | NS12 | NS13 | NS14 | NS15 | NS16
	NS17 | NS18 | NS19 | NS20 | NS21 | NS22 | NS23 | NS24
	NS25 | NS26 | NS27 | NS28 | NS29 | NS30 | NS31 | NS32

NetworkSigValueNoCarrAggr
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.NetworkSigValueNoCarrAggr.NS01
	# Last value:
	value = enums.NetworkSigValueNoCarrAggr.NS99
	# All values (288x):
	NS01 | NS02 | NS03 | NS04 | NS05 | NS06 | NS07 | NS08
	NS09 | NS10 | NS100 | NS101 | NS102 | NS103 | NS104 | NS105
	NS106 | NS107 | NS108 | NS109 | NS11 | NS110 | NS111 | NS112
	NS113 | NS114 | NS115 | NS116 | NS117 | NS118 | NS119 | NS12
	NS120 | NS121 | NS122 | NS123 | NS124 | NS125 | NS126 | NS127
	NS128 | NS129 | NS13 | NS130 | NS131 | NS132 | NS133 | NS134
	NS135 | NS136 | NS137 | NS138 | NS139 | NS14 | NS140 | NS141
	NS142 | NS143 | NS144 | NS145 | NS146 | NS147 | NS148 | NS149
	NS15 | NS150 | NS151 | NS152 | NS153 | NS154 | NS155 | NS156
	NS157 | NS158 | NS159 | NS16 | NS160 | NS161 | NS162 | NS163
	NS164 | NS165 | NS166 | NS167 | NS168 | NS169 | NS17 | NS170
	NS171 | NS172 | NS173 | NS174 | NS175 | NS176 | NS177 | NS178
	NS179 | NS18 | NS180 | NS181 | NS182 | NS183 | NS184 | NS185
	NS186 | NS187 | NS188 | NS189 | NS19 | NS190 | NS191 | NS192
	NS193 | NS194 | NS195 | NS196 | NS197 | NS198 | NS199 | NS20
	NS200 | NS201 | NS202 | NS203 | NS204 | NS205 | NS206 | NS207
	NS208 | NS209 | NS21 | NS210 | NS211 | NS212 | NS213 | NS214
	NS215 | NS216 | NS217 | NS218 | NS219 | NS22 | NS220 | NS221
	NS222 | NS223 | NS224 | NS225 | NS226 | NS227 | NS228 | NS229
	NS23 | NS230 | NS231 | NS232 | NS233 | NS234 | NS235 | NS236
	NS237 | NS238 | NS239 | NS24 | NS240 | NS241 | NS242 | NS243
	NS244 | NS245 | NS246 | NS247 | NS248 | NS249 | NS25 | NS250
	NS251 | NS252 | NS253 | NS254 | NS255 | NS256 | NS257 | NS258
	NS259 | NS26 | NS260 | NS261 | NS262 | NS263 | NS264 | NS265
	NS266 | NS267 | NS268 | NS269 | NS27 | NS270 | NS271 | NS272
	NS273 | NS274 | NS275 | NS276 | NS277 | NS278 | NS279 | NS28
	NS280 | NS281 | NS282 | NS283 | NS284 | NS285 | NS286 | NS287
	NS288 | NS29 | NS30 | NS31 | NS32 | NS33 | NS34 | NS35
	NS36 | NS37 | NS38 | NS39 | NS40 | NS41 | NS42 | NS43
	NS44 | NS45 | NS46 | NS47 | NS48 | NS49 | NS50 | NS51
	NS52 | NS53 | NS54 | NS55 | NS56 | NS57 | NS58 | NS59
	NS60 | NS61 | NS62 | NS63 | NS64 | NS65 | NS66 | NS67
	NS68 | NS69 | NS70 | NS71 | NS72 | NS73 | NS74 | NS75
	NS76 | NS77 | NS78 | NS79 | NS80 | NS81 | NS82 | NS83
	NS84 | NS85 | NS86 | NS87 | NS88 | NS89 | NS90 | NS91
	NS92 | NS93 | NS94 | NS95 | NS96 | NS97 | NS98 | NS99

ParameterSetMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ParameterSetMode.GLOBal
	# All values (2x):
	GLOBal | LIST

Path
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Path.NETWork
	# All values (2x):
	NETWork | STANdalone

PeriodPreamble
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PeriodPreamble.MS05
	# All values (3x):
	MS05 | MS10 | MS20

PucchFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PucchFormat.F1
	# All values (7x):
	F1 | F1A | F1B | F2 | F2A | F2B | F3

RbTableChannelType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RbTableChannelType.DL
	# All values (8x):
	DL | NONE | PSBCh | PSCCh | PSSCh | PUCCh | PUSCh | SSUB

Rbw
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Rbw.K030
	# All values (3x):
	K030 | K100 | M1

RbwExtended
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RbwExtended.K030
	# All values (6x):
	K030 | K050 | K100 | K150 | K200 | M1

Repeat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Repeat.CONTinuous
	# All values (2x):
	CONTinuous | SINGleshot

ResourceState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ResourceState.ACTive
	# All values (8x):
	ACTive | ADJusted | INValid | OFF | PENDing | QUEued | RDY | RUN

ResultStatus2
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ResultStatus2.DC
	# Last value:
	value = enums.ResultStatus2.ULEU
	# All values (10x):
	DC | INV | NAV | NCAP | OFF | OFL | OK | UFL
	ULEL | ULEU

RetriggerFlag
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RetriggerFlag.IFPNarrow
	# All values (4x):
	IFPNarrow | IFPower | OFF | ON

SegmentChannelTypeExtended
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SegmentChannelTypeExtended.AUTO
	# All values (6x):
	AUTO | PSBCh | PSCCh | PSSCh | PUCCh | PUSCh

SidelinkChannelType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SidelinkChannelType.PSBCh
	# All values (3x):
	PSBCh | PSCCh | PSSCh

SignalSlope
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SignalSlope.FEDGe
	# All values (2x):
	FEDGe | REDGe

SignalType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SignalType.SL
	# All values (2x):
	SL | UL

StopCondition
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StopCondition.NONE
	# All values (2x):
	NONE | SLFail

SyncMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SyncMode.ENHanced
	# All values (2x):
	ENHanced | NORMal

TargetStateA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TargetStateA.OFF
	# All values (3x):
	OFF | RDY | RUN

TargetSyncState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TargetSyncState.ADJusted
	# All values (2x):
	ADJusted | PENDing

TimeMask
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TimeMask.GOO
	# All values (3x):
	GOO | PPSRs | SBLanking

TraceSelect
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TraceSelect.AVERage
	# All values (3x):
	AVERage | CURRent | MAXimum

UplinkChannelType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UplinkChannelType.PUCCh
	# All values (2x):
	PUCCh | PUSCh

ViewMev
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ViewMev.ACLR
	# Last value:
	value = enums.ViewMev.TXM
	# All values (15x):
	ACLR | BLER | ESFLatness | EVMagnitude | EVMC | IEMissions | IQ | MERRor
	OVERview | PDYNamics | PERRor | PMONitor | RBATable | SEMask | TXM

ViewPrach
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ViewPrach.EVMagnitude
	# Last value:
	value = enums.ViewPrach.TXM
	# All values (10x):
	EVMagnitude | EVPReamble | EVSYmbol | IQ | MERRor | OVERview | PDYNamics | PERRor
	PVPReamble | TXM

ViewSrs
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ViewSrs.PDYNamics
	# All values (1x):
	PDYNamics

