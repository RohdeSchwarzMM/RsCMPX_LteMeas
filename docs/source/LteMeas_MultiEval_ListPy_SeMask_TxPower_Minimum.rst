Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:MINimum
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:MINimum

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:MINimum
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:MINimum



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.SeMask.TxPower.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: