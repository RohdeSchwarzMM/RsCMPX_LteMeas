Negativ
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.SeMask.Margin.Area.Negativ.NegativCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.listPy.seMask.margin.area.negativ.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_ListPy_SeMask_Margin_Area_Negativ_Average.rst
	LteMeas_MultiEval_ListPy_SeMask_Margin_Area_Negativ_Current.rst
	LteMeas_MultiEval_ListPy_SeMask_Margin_Area_Negativ_Minimum.rst