Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:ESFLatness:EXTReme
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:ESFLatness:EXTReme
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:ESFLatness:EXTReme

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:ESFLatness:EXTReme
	FETCh:LTE:MEASurement<Instance>:MEValuation:ESFLatness:EXTReme
	CALCulate:LTE:MEASurement<Instance>:MEValuation:ESFLatness:EXTReme



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.EsFlatness.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: