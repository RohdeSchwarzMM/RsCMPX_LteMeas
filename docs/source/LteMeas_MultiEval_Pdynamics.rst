Pdynamics
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Pdynamics.PdynamicsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.pdynamics.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_Pdynamics_Average.rst
	LteMeas_MultiEval_Pdynamics_Current.rst
	LteMeas_MultiEval_Pdynamics_Maximum.rst
	LteMeas_MultiEval_Pdynamics_Minimum.rst
	LteMeas_MultiEval_Pdynamics_StandardDev.rst