Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:AVERage
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:AVERage
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:AVERage
	FETCh:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:AVERage
	CALCulate:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.EvMagnitude.Average.AverageCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.evMagnitude.average.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_EvMagnitude_Average_Nref.rst