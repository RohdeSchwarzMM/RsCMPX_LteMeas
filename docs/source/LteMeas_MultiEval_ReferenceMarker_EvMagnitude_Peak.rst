Peak
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:REFMarker:EVMagnitude:PEAK

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:REFMarker:EVMagnitude:PEAK



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ReferenceMarker.EvMagnitude.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex: