Scc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:TRACe:RBATable:SCC
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:RBATable:SCC

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:TRACe:RBATable:SCC
	FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:RBATable:SCC



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Trace.RbaTable.Scc.SccCls
	:members:
	:undoc-members:
	:noindex: