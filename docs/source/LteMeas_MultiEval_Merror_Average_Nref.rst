Nref
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:MERRor:AVERage:NREF
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:MERRor:AVERage:NREF

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:MERRor:AVERage:NREF
	FETCh:LTE:MEASurement<Instance>:MEValuation:MERRor:AVERage:NREF



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Merror.Average.Nref.NrefCls
	:members:
	:undoc-members:
	:noindex: