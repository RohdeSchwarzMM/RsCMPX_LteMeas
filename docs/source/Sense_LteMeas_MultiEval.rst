MultiEval
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.Sense.LteMeas.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.lteMeas.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_LteMeas_MultiEval_Limit.rst
	Sense_LteMeas_MultiEval_Spectrum.rst