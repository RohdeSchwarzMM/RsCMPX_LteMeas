Ulca
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Trace.RbaTable.Ulca.UlcaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.trace.rbaTable.ulca.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_Trace_RbaTable_Ulca_Pcc.rst
	LteMeas_MultiEval_Trace_RbaTable_Ulca_Scc.rst