Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:HIGH:CURRent
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:HIGH:CURRent

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:HIGH:CURRent
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:HIGH:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Modulation.Evm.Rms.High.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: