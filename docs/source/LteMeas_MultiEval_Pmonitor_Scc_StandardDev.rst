StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:SCC:SDEViation
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:SCC:SDEViation

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:SCC:SDEViation
	FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:SCC:SDEViation



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Pmonitor.Scc.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: