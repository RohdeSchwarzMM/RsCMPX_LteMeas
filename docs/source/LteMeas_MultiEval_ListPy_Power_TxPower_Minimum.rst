Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:MINimum
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:MINimum

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:MINimum
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:MINimum



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Power.TxPower.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: