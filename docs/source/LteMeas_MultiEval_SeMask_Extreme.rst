Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:SEMask:EXTReme
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:SEMask:EXTReme
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:SEMask:EXTReme

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:SEMask:EXTReme
	FETCh:LTE:MEASurement<Instance>:MEValuation:SEMask:EXTReme
	CALCulate:LTE:MEASurement<Instance>:MEValuation:SEMask:EXTReme



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.SeMask.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: