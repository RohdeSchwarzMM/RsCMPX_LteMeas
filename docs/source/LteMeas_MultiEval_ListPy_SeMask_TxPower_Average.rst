Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:AVERage
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:AVERage

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:AVERage
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.SeMask.TxPower.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: