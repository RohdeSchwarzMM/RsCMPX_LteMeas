Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:CURRent
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:CURRent

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:CURRent
	FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Trace.Pdynamics.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: