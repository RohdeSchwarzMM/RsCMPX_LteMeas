Difference<Difference>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.lteMeas.multiEval.listPy.esFlatness.difference.repcap_difference_get()
	driver.lteMeas.multiEval.listPy.esFlatness.difference.repcap_difference_set(repcap.Difference.Nr1)





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.EsFlatness.Difference.DifferenceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.listPy.esFlatness.difference.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_ListPy_EsFlatness_Difference_Average.rst
	LteMeas_MultiEval_ListPy_EsFlatness_Difference_Current.rst
	LteMeas_MultiEval_ListPy_EsFlatness_Difference_Extreme.rst
	LteMeas_MultiEval_ListPy_EsFlatness_Difference_StandardDev.rst