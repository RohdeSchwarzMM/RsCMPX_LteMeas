PlcId
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:CC<Nr>:PLCid

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:CC<Nr>:PLCid



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.Cc.PlcId.PlcIdCls
	:members:
	:undoc-members:
	:noindex: