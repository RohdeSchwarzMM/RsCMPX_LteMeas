Scc<SecondaryCC>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: CC1 .. CC7
	rc = driver.configure.lteMeas.carrierAggregation.scc.repcap_secondaryCC_get()
	driver.configure.lteMeas.carrierAggregation.scc.repcap_secondaryCC_set(repcap.SecondaryCC.CC1)





.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.CarrierAggregation.Scc.SccCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.carrierAggregation.scc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_LteMeas_CarrierAggregation_Scc_AcSpacing.rst