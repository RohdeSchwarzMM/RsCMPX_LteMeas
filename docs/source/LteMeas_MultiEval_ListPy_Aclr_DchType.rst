DchType
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:ACLR:DCHType

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:ACLR:DCHType



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Aclr.DchType.DchTypeCls
	:members:
	:undoc-members:
	:noindex: