Nref
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:MERRor:CURRent:NREF
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:MERRor:CURRent:NREF

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:MERRor:CURRent:NREF
	FETCh:LTE:MEASurement<Instance>:MEValuation:MERRor:CURRent:NREF



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Merror.Current.Nref.NrefCls
	:members:
	:undoc-members:
	:noindex: