Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:ULCA:PCC:MINimum
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:ULCA:PCC:MINimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:ULCA:PCC:MINimum
	FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:ULCA:PCC:MINimum



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Pmonitor.Ulca.Pcc.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: