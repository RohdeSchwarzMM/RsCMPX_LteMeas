StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:SRS:PDYNamics:SDEViation
	single: FETCh:LTE:MEASurement<Instance>:SRS:PDYNamics:SDEViation
	single: CALCulate:LTE:MEASurement<Instance>:SRS:PDYNamics:SDEViation

.. code-block:: python

	READ:LTE:MEASurement<Instance>:SRS:PDYNamics:SDEViation
	FETCh:LTE:MEASurement<Instance>:SRS:PDYNamics:SDEViation
	CALCulate:LTE:MEASurement<Instance>:SRS:PDYNamics:SDEViation



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Srs.Pdynamics.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: