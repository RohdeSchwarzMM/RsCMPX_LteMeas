Modulation
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_Modulation_Average.rst
	LteMeas_MultiEval_Modulation_Current.rst
	LteMeas_MultiEval_Modulation_Dallocation.rst
	LteMeas_MultiEval_Modulation_DchType.rst
	LteMeas_MultiEval_Modulation_Dmodulation.rst
	LteMeas_MultiEval_Modulation_Extreme.rst
	LteMeas_MultiEval_Modulation_SchType.rst
	LteMeas_MultiEval_Modulation_StandardDev.rst