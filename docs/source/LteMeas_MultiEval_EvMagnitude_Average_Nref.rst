Nref
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:AVERage:NREF
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:AVERage:NREF

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:AVERage:NREF
	FETCh:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:AVERage:NREF



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.EvMagnitude.Average.Nref.NrefCls
	:members:
	:undoc-members:
	:noindex: