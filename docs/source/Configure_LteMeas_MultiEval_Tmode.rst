Tmode
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:TMODe:SCOunt
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:TMODe:ENPower
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:TMODe:RLEVel

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:TMODe:SCOunt
	CONFigure:LTE:MEASurement<Instance>:MEValuation:TMODe:ENPower
	CONFigure:LTE:MEASurement<Instance>:MEValuation:TMODe:RLEVel



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.Tmode.TmodeCls
	:members:
	:undoc-members:
	:noindex: