ChannelBw3rd<ThirdChannelBw>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Bw100 .. Bw200
	rc = driver.configure.lteMeas.multiEval.limit.seMask.limit.carrierAggregation.channelBw1st.channelBw2nd.channelBw3rd.repcap_thirdChannelBw_get()
	driver.configure.lteMeas.multiEval.limit.seMask.limit.carrierAggregation.channelBw1st.channelBw2nd.channelBw3rd.repcap_thirdChannelBw_set(repcap.ThirdChannelBw.Bw100)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:SEMask:LIMit<nr>:CAGGregation:CBANdwidth<Band1>:CBANdwidth<Band2>:CBANdwidth<Band3>

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:SEMask:LIMit<nr>:CAGGregation:CBANdwidth<Band1>:CBANdwidth<Band2>:CBANdwidth<Band3>



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.Limit.SeMask.Limit.CarrierAggregation.ChannelBw1st.ChannelBw2nd.ChannelBw3rd.ChannelBw3rdCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.multiEval.limit.seMask.limit.carrierAggregation.channelBw1st.channelBw2nd.channelBw3rd.clone()