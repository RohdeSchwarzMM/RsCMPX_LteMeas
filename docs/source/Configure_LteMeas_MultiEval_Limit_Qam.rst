Qam<QAMmodOrder>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Qam16 .. Qam256
	rc = driver.configure.lteMeas.multiEval.limit.qam.repcap_qAMmodOrder_get()
	driver.configure.lteMeas.multiEval.limit.qam.repcap_qAMmodOrder_set(repcap.QAMmodOrder.Qam16)





.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.Limit.Qam.QamCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.multiEval.limit.qam.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_LteMeas_MultiEval_Limit_Qam_EsFlatness.rst
	Configure_LteMeas_MultiEval_Limit_Qam_EvMagnitude.rst
	Configure_LteMeas_MultiEval_Limit_Qam_FreqError.rst
	Configure_LteMeas_MultiEval_Limit_Qam_Ibe.rst
	Configure_LteMeas_MultiEval_Limit_Qam_IqOffset.rst
	Configure_LteMeas_MultiEval_Limit_Qam_Merror.rst
	Configure_LteMeas_MultiEval_Limit_Qam_Perror.rst
	Configure_LteMeas_MultiEval_Limit_Qam_Sflatness.rst