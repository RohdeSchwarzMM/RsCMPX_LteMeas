Bler
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:BLER
	single: READ:LTE:MEASurement<Instance>:MEValuation:BLER

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:BLER
	READ:LTE:MEASurement<Instance>:MEValuation:BLER



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Bler.BlerCls
	:members:
	:undoc-members:
	:noindex: