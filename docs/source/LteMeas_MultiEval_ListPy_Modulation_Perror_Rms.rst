Rms
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Modulation.Perror.Rms.RmsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.listPy.modulation.perror.rms.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_ListPy_Modulation_Perror_Rms_High.rst
	LteMeas_MultiEval_ListPy_Modulation_Perror_Rms_Low.rst