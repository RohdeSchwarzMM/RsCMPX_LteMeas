Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:DMRS:HIGH:AVERage
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:DMRS:HIGH:AVERage

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:DMRS:HIGH:AVERage
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:DMRS:HIGH:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Modulation.Merror.Dmrs.High.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: