Spectrum
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.Spectrum.SpectrumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.multiEval.spectrum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_LteMeas_MultiEval_Spectrum_Aclr.rst
	Configure_LteMeas_MultiEval_Spectrum_SeMask.rst