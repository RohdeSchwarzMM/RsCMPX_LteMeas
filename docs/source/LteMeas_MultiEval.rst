MultiEval
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INITiate:LTE:MEASurement<Instance>:MEValuation
	single: STOP:LTE:MEASurement<Instance>:MEValuation
	single: ABORt:LTE:MEASurement<Instance>:MEValuation

.. code-block:: python

	INITiate:LTE:MEASurement<Instance>:MEValuation
	STOP:LTE:MEASurement<Instance>:MEValuation
	ABORt:LTE:MEASurement<Instance>:MEValuation



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_Aclr.rst
	LteMeas_MultiEval_Amarker.rst
	LteMeas_MultiEval_Bler.rst
	LteMeas_MultiEval_Dmarker.rst
	LteMeas_MultiEval_EsFlatness.rst
	LteMeas_MultiEval_EvMagnitude.rst
	LteMeas_MultiEval_Evmc.rst
	LteMeas_MultiEval_InbandEmission.rst
	LteMeas_MultiEval_ListPy.rst
	LteMeas_MultiEval_Merror.rst
	LteMeas_MultiEval_Modulation.rst
	LteMeas_MultiEval_Pdynamics.rst
	LteMeas_MultiEval_Perror.rst
	LteMeas_MultiEval_Pmonitor.rst
	LteMeas_MultiEval_ReferenceMarker.rst
	LteMeas_MultiEval_SeMask.rst
	LteMeas_MultiEval_State.rst
	LteMeas_MultiEval_Trace.rst
	LteMeas_MultiEval_VfThroughput.rst