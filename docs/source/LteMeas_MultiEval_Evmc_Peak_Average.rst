Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:EVMC:PEAK:AVERage
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:EVMC:PEAK:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:EVMC:PEAK:AVERage
	FETCh:LTE:MEASurement<Instance>:MEValuation:EVMC:PEAK:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Evmc.Peak.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: