Result
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:RESult[:ALL]
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:RESult:EVMagnitude
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:RESult:EVPReamble
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:RESult:MERRor
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:RESult:PERRor
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:RESult:IQ
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:RESult:PDYNamics
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:RESult:PVPReamble
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:RESult:TXM

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:PRACh:RESult[:ALL]
	CONFigure:LTE:MEASurement<Instance>:PRACh:RESult:EVMagnitude
	CONFigure:LTE:MEASurement<Instance>:PRACh:RESult:EVPReamble
	CONFigure:LTE:MEASurement<Instance>:PRACh:RESult:MERRor
	CONFigure:LTE:MEASurement<Instance>:PRACh:RESult:PERRor
	CONFigure:LTE:MEASurement<Instance>:PRACh:RESult:IQ
	CONFigure:LTE:MEASurement<Instance>:PRACh:RESult:PDYNamics
	CONFigure:LTE:MEASurement<Instance>:PRACh:RESult:PVPReamble
	CONFigure:LTE:MEASurement<Instance>:PRACh:RESult:TXM



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.Prach.Result.ResultCls
	:members:
	:undoc-members:
	:noindex: