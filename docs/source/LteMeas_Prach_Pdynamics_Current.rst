Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:PRACh:PDYNamics:CURRent
	single: FETCh:LTE:MEASurement<Instance>:PRACh:PDYNamics:CURRent
	single: CALCulate:LTE:MEASurement<Instance>:PRACh:PDYNamics:CURRent

.. code-block:: python

	READ:LTE:MEASurement<Instance>:PRACh:PDYNamics:CURRent
	FETCh:LTE:MEASurement<Instance>:PRACh:PDYNamics:CURRent
	CALCulate:LTE:MEASurement<Instance>:PRACh:PDYNamics:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Prach.Pdynamics.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: