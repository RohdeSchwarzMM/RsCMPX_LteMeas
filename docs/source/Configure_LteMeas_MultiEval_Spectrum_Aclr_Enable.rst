Enable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:SPECtrum:ACLR:ENABle

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:SPECtrum:ACLR:ENABle



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.Spectrum.Aclr.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: