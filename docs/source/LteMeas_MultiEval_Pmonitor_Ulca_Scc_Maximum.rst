Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:ULCA:SCC<Nr>:MAXimum
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:ULCA:SCC<Nr>:MAXimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:ULCA:SCC<Nr>:MAXimum
	FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:ULCA:SCC<Nr>:MAXimum



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Pmonitor.Ulca.Scc.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: