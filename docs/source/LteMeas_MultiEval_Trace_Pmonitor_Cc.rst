Cc<CarrierComponent>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.lteMeas.multiEval.trace.pmonitor.cc.repcap_carrierComponent_get()
	driver.lteMeas.multiEval.trace.pmonitor.cc.repcap_carrierComponent_set(repcap.CarrierComponent.Nr1)



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:TRACe:PMONitor:CC<Nr>
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:PMONitor:CC<Nr>

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:TRACe:PMONitor:CC<Nr>
	FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:PMONitor:CC<Nr>



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Trace.Pmonitor.Cc.CcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.trace.pmonitor.cc.clone()