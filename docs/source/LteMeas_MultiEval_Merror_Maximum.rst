Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:MERRor:MAXimum
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:MERRor:MAXimum
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:MERRor:MAXimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:MERRor:MAXimum
	FETCh:LTE:MEASurement<Instance>:MEValuation:MERRor:MAXimum
	CALCulate:LTE:MEASurement<Instance>:MEValuation:MERRor:MAXimum



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Merror.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.merror.maximum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_Merror_Maximum_Nref.rst