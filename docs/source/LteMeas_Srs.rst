Srs
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INITiate:LTE:MEASurement<Instance>:SRS
	single: STOP:LTE:MEASurement<Instance>:SRS
	single: ABORt:LTE:MEASurement<Instance>:SRS

.. code-block:: python

	INITiate:LTE:MEASurement<Instance>:SRS
	STOP:LTE:MEASurement<Instance>:SRS
	ABORt:LTE:MEASurement<Instance>:SRS



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Srs.SrsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.srs.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_Srs_Pdynamics.rst
	LteMeas_Srs_State.rst
	LteMeas_Srs_Trace.rst