Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:PRACh:EVMSymbol:MAXimum
	single: FETCh:LTE:MEASurement<Instance>:PRACh:EVMSymbol:MAXimum
	single: CALCulate:LTE:MEASurement<Instance>:PRACh:EVMSymbol:MAXimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:PRACh:EVMSymbol:MAXimum
	FETCh:LTE:MEASurement<Instance>:PRACh:EVMSymbol:MAXimum
	CALCulate:LTE:MEASurement<Instance>:PRACh:EVMSymbol:MAXimum



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Prach.EvmSymbol.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: