EePeriods
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:MODulation:EEPeriods:PUCCh

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:MODulation:EEPeriods:PUCCh



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.Modulation.EePeriods.EePeriodsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.multiEval.modulation.eePeriods.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_LteMeas_MultiEval_Modulation_EePeriods_Pusch.rst