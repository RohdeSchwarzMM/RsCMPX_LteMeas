State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:SRS:STATe

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:SRS:STATe



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Srs.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.srs.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_Srs_State_All.rst