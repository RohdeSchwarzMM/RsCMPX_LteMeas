Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:PRACh:TRACe:EVM:MAXimum
	single: FETCh:LTE:MEASurement<Instance>:PRACh:TRACe:EVM:MAXimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:PRACh:TRACe:EVM:MAXimum
	FETCh:LTE:MEASurement<Instance>:PRACh:TRACe:EVM:MAXimum



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Prach.Trace.Evm.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: