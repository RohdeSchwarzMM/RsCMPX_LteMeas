Positiv
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:SEMask:MARGin:CURRent:POSitiv

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:SEMask:MARGin:CURRent:POSitiv



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.SeMask.Margin.Current.Positiv.PositivCls
	:members:
	:undoc-members:
	:noindex: