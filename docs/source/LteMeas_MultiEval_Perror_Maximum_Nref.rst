Nref
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PERRor:MAXimum:NREF
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PERRor:MAXimum:NREF

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PERRor:MAXimum:NREF
	FETCh:LTE:MEASurement<Instance>:MEValuation:PERRor:MAXimum:NREF



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Perror.Maximum.Nref.NrefCls
	:members:
	:undoc-members:
	:noindex: