Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:SRS:PDYNamics:MINimum
	single: FETCh:LTE:MEASurement<Instance>:SRS:PDYNamics:MINimum
	single: CALCulate:LTE:MEASurement<Instance>:SRS:PDYNamics:MINimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:SRS:PDYNamics:MINimum
	FETCh:LTE:MEASurement<Instance>:SRS:PDYNamics:MINimum
	CALCulate:LTE:MEASurement<Instance>:SRS:PDYNamics:MINimum



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Srs.Pdynamics.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: