StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:MODulation:SDEViation
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:MODulation:SDEViation
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:MODulation:SDEViation

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:MODulation:SDEViation
	FETCh:LTE:MEASurement<Instance>:MEValuation:MODulation:SDEViation
	CALCulate:LTE:MEASurement<Instance>:MEValuation:MODulation:SDEViation



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Modulation.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: