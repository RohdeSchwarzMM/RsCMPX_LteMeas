Srs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:SRS:ENABle

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:SRS:ENABle



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.Srs.SrsCls
	:members:
	:undoc-members:
	:noindex: