Aclr
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Aclr.AclrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.listPy.aclr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_ListPy_Aclr_Dallocation.rst
	LteMeas_MultiEval_ListPy_Aclr_DchType.rst
	LteMeas_MultiEval_ListPy_Aclr_Eutra.rst
	LteMeas_MultiEval_ListPy_Aclr_Utra.rst