Nref
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PERRor:AVERage:NREF
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PERRor:AVERage:NREF

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PERRor:AVERage:NREF
	FETCh:LTE:MEASurement<Instance>:MEValuation:PERRor:AVERage:NREF



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Perror.Average.Nref.NrefCls
	:members:
	:undoc-members:
	:noindex: