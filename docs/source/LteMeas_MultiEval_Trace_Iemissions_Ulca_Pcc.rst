Pcc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:TRACe:IEMissions:ULCA[:PCC]
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:IEMissions:ULCA[:PCC]

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:TRACe:IEMissions:ULCA[:PCC]
	FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:IEMissions:ULCA[:PCC]



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Trace.Iemissions.Ulca.Pcc.PccCls
	:members:
	:undoc-members:
	:noindex: