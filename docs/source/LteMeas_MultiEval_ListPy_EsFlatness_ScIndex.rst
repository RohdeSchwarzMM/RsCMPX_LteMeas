ScIndex
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.EsFlatness.ScIndex.ScIndexCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.listPy.esFlatness.scIndex.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_ListPy_EsFlatness_ScIndex_Maximum.rst
	LteMeas_MultiEval_ListPy_EsFlatness_ScIndex_Minimum.rst