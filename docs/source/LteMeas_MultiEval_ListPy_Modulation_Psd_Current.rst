Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PSD:CURRent
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PSD:CURRent

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PSD:CURRent
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PSD:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Modulation.Psd.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: