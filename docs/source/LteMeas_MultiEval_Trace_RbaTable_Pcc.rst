Pcc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:TRACe:RBATable[:PCC]
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:RBATable[:PCC]

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:TRACe:RBATable[:PCC]
	FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:RBATable[:PCC]



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Trace.RbaTable.Pcc.PccCls
	:members:
	:undoc-members:
	:noindex: