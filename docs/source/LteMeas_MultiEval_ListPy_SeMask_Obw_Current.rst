Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:CURRent
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:CURRent

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:CURRent
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.SeMask.Obw.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: