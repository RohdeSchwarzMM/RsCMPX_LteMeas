CarrierAggregation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:MEASurement<Instance>:CAGGregation:FSHWare

.. code-block:: python

	SENSe:LTE:MEASurement<Instance>:CAGGregation:FSHWare



.. autoclass:: RsCMPX_LteMeas.Implementations.Sense.LteMeas.CarrierAggregation.CarrierAggregationCls
	:members:
	:undoc-members:
	:noindex: