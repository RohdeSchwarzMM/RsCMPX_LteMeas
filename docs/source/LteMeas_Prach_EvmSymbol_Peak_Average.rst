Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:PRACh:EVMSymbol:PEAK:AVERage
	single: FETCh:LTE:MEASurement<Instance>:PRACh:EVMSymbol:PEAK:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:PRACh:EVMSymbol:PEAK:AVERage
	FETCh:LTE:MEASurement<Instance>:PRACh:EVMSymbol:PEAK:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Prach.EvmSymbol.Peak.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: