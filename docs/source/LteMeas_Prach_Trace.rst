Trace
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Prach.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.prach.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_Prach_Trace_Evm.rst
	LteMeas_Prach_Trace_EvPreamble.rst
	LteMeas_Prach_Trace_Iq.rst
	LteMeas_Prach_Trace_Merror.rst
	LteMeas_Prach_Trace_Pdynamics.rst
	LteMeas_Prach_Trace_Perror.rst
	LteMeas_Prach_Trace_PvPreamble.rst