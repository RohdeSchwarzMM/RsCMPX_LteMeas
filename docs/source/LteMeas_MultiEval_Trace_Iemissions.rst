Iemissions
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Trace.Iemissions.IemissionsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.trace.iemissions.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_Trace_Iemissions_Cc.rst
	LteMeas_MultiEval_Trace_Iemissions_Pcc.rst
	LteMeas_MultiEval_Trace_Iemissions_Scc.rst
	LteMeas_MultiEval_Trace_Iemissions_Ulca.rst