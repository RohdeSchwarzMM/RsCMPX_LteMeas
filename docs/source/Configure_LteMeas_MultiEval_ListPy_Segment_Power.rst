Power
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:POWer

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:POWer



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.ListPy.Segment.Power.PowerCls
	:members:
	:undoc-members:
	:noindex: