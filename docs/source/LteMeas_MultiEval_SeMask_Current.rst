Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:SEMask:CURRent
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:SEMask:CURRent
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:SEMask:CURRent

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:SEMask:CURRent
	FETCh:LTE:MEASurement<Instance>:MEValuation:SEMask:CURRent
	CALCulate:LTE:MEASurement<Instance>:MEValuation:SEMask:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.SeMask.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: