Scount
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:SRS:SCOunt:PDYNamics

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:SRS:SCOunt:PDYNamics



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.Srs.Scount.ScountCls
	:members:
	:undoc-members:
	:noindex: