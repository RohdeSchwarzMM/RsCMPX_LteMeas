StandardDev
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:IEMission:ULCA:SCC<Nr>:MARGin:SDEViation

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:IEMission:ULCA:SCC<Nr>:MARGin:SDEViation



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.InbandEmission.Ulca.Scc.Margin.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: