AcSpacing
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:CAGGregation:ACSPacing

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:CAGGregation:ACSPacing



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.ListPy.Segment.CarrierAggregation.AcSpacing.AcSpacingCls
	:members:
	:undoc-members:
	:noindex: