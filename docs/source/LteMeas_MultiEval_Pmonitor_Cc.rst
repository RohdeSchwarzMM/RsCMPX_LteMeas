Cc<CarrierComponent>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.lteMeas.multiEval.pmonitor.cc.repcap_carrierComponent_get()
	driver.lteMeas.multiEval.pmonitor.cc.repcap_carrierComponent_set(repcap.CarrierComponent.Nr1)





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Pmonitor.Cc.CcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.pmonitor.cc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_Pmonitor_Cc_Average.rst
	LteMeas_MultiEval_Pmonitor_Cc_Current.rst
	LteMeas_MultiEval_Pmonitor_Cc_Maximum.rst
	LteMeas_MultiEval_Pmonitor_Cc_Minimum.rst
	LteMeas_MultiEval_Pmonitor_Cc_StandardDev.rst