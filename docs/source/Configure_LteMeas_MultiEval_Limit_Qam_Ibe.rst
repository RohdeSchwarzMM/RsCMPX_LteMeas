Ibe
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QAM<ModOrder>:IBE

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QAM<ModOrder>:IBE



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.Limit.Qam.Ibe.IbeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.multiEval.limit.qam.ibe.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_LteMeas_MultiEval_Limit_Qam_Ibe_IqOffset.rst