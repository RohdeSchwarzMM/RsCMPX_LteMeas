Preamble<Preamble>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr400
	rc = driver.lteMeas.prach.modulation.preamble.repcap_preamble_get()
	driver.lteMeas.prach.modulation.preamble.repcap_preamble_set(repcap.Preamble.Nr1)



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:PRACh:MODulation:PREamble<Number>
	single: FETCh:LTE:MEASurement<Instance>:PRACh:MODulation:PREamble<Number>

.. code-block:: python

	READ:LTE:MEASurement<Instance>:PRACh:MODulation:PREamble<Number>
	FETCh:LTE:MEASurement<Instance>:PRACh:MODulation:PREamble<Number>



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Prach.Modulation.Preamble.PreambleCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.prach.modulation.preamble.clone()