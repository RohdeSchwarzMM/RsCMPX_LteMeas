Evmc
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Evmc.EvmcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.evmc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_Evmc_Peak.rst