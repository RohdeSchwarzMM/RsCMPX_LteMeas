Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:SCC:AVERage
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:SCC:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:SCC:AVERage
	FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:SCC:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Pmonitor.Scc.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: