Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:PRACh:TRACe:PERRor:AVERage
	single: FETCh:LTE:MEASurement<Instance>:PRACh:TRACe:PERRor:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:PRACh:TRACe:PERRor:AVERage
	FETCh:LTE:MEASurement<Instance>:PRACh:TRACe:PERRor:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Prach.Trace.Perror.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: