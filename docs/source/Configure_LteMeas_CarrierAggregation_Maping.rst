Maping
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:CAGGregation:MAPing:PCC
	single: CONFigure:LTE:MEASurement<Instance>:CAGGregation:MAPing

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:CAGGregation:MAPing:PCC
	CONFigure:LTE:MEASurement<Instance>:CAGGregation:MAPing



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.CarrierAggregation.Maping.MapingCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.carrierAggregation.maping.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_LteMeas_CarrierAggregation_Maping_Scc.rst