RbAllocation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:RBALlocation:AUTO

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:RBALlocation:AUTO



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.RbAllocation.RbAllocationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.multiEval.rbAllocation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_LteMeas_MultiEval_RbAllocation_Mcluster.rst
	Configure_LteMeas_MultiEval_RbAllocation_Nrb.rst
	Configure_LteMeas_MultiEval_RbAllocation_Orb.rst