Peak
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Evmc.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.evmc.peak.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_Evmc_Peak_Average.rst
	LteMeas_MultiEval_Evmc_Peak_Current.rst
	LteMeas_MultiEval_Evmc_Peak_Maximum.rst
	LteMeas_MultiEval_Evmc_Peak_StandardDev.rst