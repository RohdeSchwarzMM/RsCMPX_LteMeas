Trace
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_Trace_Aclr.rst
	LteMeas_MultiEval_Trace_EsFlatness.rst
	LteMeas_MultiEval_Trace_Evmc.rst
	LteMeas_MultiEval_Trace_EvmSymbol.rst
	LteMeas_MultiEval_Trace_Iemissions.rst
	LteMeas_MultiEval_Trace_Iq.rst
	LteMeas_MultiEval_Trace_Pdynamics.rst
	LteMeas_MultiEval_Trace_Pmonitor.rst
	LteMeas_MultiEval_Trace_RbaTable.rst
	LteMeas_MultiEval_Trace_SeMask.rst