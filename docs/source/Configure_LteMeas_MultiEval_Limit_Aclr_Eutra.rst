Eutra
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.Limit.Aclr.Eutra.EutraCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.multiEval.limit.aclr.eutra.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_LteMeas_MultiEval_Limit_Aclr_Eutra_CarrierAggregation.rst
	Configure_LteMeas_MultiEval_Limit_Aclr_Eutra_ChannelBw.rst