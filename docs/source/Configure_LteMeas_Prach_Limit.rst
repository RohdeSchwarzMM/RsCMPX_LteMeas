Limit
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:LIMit:FERRor

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:PRACh:LIMit:FERRor



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.Prach.Limit.LimitCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.prach.limit.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_LteMeas_Prach_Limit_EvMagnitude.rst
	Configure_LteMeas_Prach_Limit_Merror.rst
	Configure_LteMeas_Prach_Limit_Pdynamics.rst
	Configure_LteMeas_Prach_Limit_Perror.rst