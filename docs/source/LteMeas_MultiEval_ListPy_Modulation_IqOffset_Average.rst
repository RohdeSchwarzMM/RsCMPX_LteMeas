Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:IQOFfset:AVERage
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:IQOFfset:AVERage

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:IQOFfset:AVERage
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:IQOFfset:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Modulation.IqOffset.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: