LteMeas
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:BAND
	single: CONFigure:LTE:MEASurement<Instance>:SPATh
	single: CONFigure:LTE:MEASurement<Instance>:STYPe
	single: CONFigure:LTE:MEASurement<Instance>:DMODe
	single: CONFigure:LTE:MEASurement<Instance>:FSTRucture

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:BAND
	CONFigure:LTE:MEASurement<Instance>:SPATh
	CONFigure:LTE:MEASurement<Instance>:STYPe
	CONFigure:LTE:MEASurement<Instance>:DMODe
	CONFigure:LTE:MEASurement<Instance>:FSTRucture



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.LteMeasCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_LteMeas_CarrierAggregation.rst
	Configure_LteMeas_Cc.rst
	Configure_LteMeas_Emtc.rst
	Configure_LteMeas_MultiEval.rst
	Configure_LteMeas_Network.rst
	Configure_LteMeas_Pcc.rst
	Configure_LteMeas_Prach.rst
	Configure_LteMeas_RfSettings.rst
	Configure_LteMeas_Scc.rst
	Configure_LteMeas_Srs.rst