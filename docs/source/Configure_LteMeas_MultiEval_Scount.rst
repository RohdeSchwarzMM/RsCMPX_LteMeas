Scount
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:SCOunt:MODulation
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:SCOunt:POWer

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:SCOunt:MODulation
	CONFigure:LTE:MEASurement<Instance>:MEValuation:SCOunt:POWer



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.Scount.ScountCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.multiEval.scount.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_LteMeas_MultiEval_Scount_Spectrum.rst