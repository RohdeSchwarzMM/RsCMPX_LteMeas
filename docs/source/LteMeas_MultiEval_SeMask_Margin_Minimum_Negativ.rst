Negativ
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:SEMask:MARGin:MINimum:NEGativ

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:SEMask:MARGin:MINimum:NEGativ



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.SeMask.Margin.Minimum.Negativ.NegativCls
	:members:
	:undoc-members:
	:noindex: