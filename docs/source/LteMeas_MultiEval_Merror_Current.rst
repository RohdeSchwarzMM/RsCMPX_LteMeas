Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:MERRor:CURRent
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:MERRor:CURRent
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:MERRor:CURRent

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:MERRor:CURRent
	FETCh:LTE:MEASurement<Instance>:MEValuation:MERRor:CURRent
	CALCulate:LTE:MEASurement<Instance>:MEValuation:MERRor:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Merror.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.merror.current.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_Merror_Current_Nref.rst