Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:PCC:MINimum
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:PCC:MINimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:PCC:MINimum
	FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:PCC:MINimum



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Pmonitor.Pcc.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: