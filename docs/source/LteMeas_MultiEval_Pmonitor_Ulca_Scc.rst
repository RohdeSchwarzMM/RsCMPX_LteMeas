Scc<SecondaryCC>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: CC1 .. CC7
	rc = driver.lteMeas.multiEval.pmonitor.ulca.scc.repcap_secondaryCC_get()
	driver.lteMeas.multiEval.pmonitor.ulca.scc.repcap_secondaryCC_set(repcap.SecondaryCC.CC1)





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Pmonitor.Ulca.Scc.SccCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.pmonitor.ulca.scc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_Pmonitor_Ulca_Scc_Average.rst
	LteMeas_MultiEval_Pmonitor_Ulca_Scc_Current.rst
	LteMeas_MultiEval_Pmonitor_Ulca_Scc_Maximum.rst
	LteMeas_MultiEval_Pmonitor_Ulca_Scc_Minimum.rst
	LteMeas_MultiEval_Pmonitor_Ulca_Scc_StandardDev.rst