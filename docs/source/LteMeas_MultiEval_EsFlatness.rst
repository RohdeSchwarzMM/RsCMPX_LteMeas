EsFlatness
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.EsFlatness.EsFlatnessCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.esFlatness.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_EsFlatness_Average.rst
	LteMeas_MultiEval_EsFlatness_Current.rst
	LteMeas_MultiEval_EsFlatness_Extreme.rst
	LteMeas_MultiEval_EsFlatness_StandardDev.rst