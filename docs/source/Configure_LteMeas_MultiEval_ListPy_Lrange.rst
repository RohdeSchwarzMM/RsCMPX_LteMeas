Lrange
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:LRANge

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:LRANge



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.ListPy.Lrange.LrangeCls
	:members:
	:undoc-members:
	:noindex: