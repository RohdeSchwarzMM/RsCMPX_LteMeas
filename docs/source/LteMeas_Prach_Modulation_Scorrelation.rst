Scorrelation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:PRACh:MODulation:SCORrelation

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:PRACh:MODulation:SCORrelation



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Prach.Modulation.Scorrelation.ScorrelationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.prach.modulation.scorrelation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_Prach_Modulation_Scorrelation_Preamble.rst