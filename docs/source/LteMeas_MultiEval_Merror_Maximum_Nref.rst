Nref
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:MERRor:MAXimum:NREF
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:MERRor:MAXimum:NREF

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:MERRor:MAXimum:NREF
	FETCh:LTE:MEASurement<Instance>:MEValuation:MERRor:MAXimum:NREF



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Merror.Maximum.Nref.NrefCls
	:members:
	:undoc-members:
	:noindex: