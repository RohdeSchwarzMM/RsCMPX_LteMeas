Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:RFSettings:CC<Nr>:FREQuency

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:RFSettings:CC<Nr>:FREQuency



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.RfSettings.Cc.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: