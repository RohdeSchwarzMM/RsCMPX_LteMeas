Srs
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:SRS:VIEW
	single: CONFigure:LTE:MEASurement<Instance>:SRS:TOUT
	single: CONFigure:LTE:MEASurement<Instance>:SRS:REPetition
	single: CONFigure:LTE:MEASurement<Instance>:SRS:SCONdition
	single: CONFigure:LTE:MEASurement<Instance>:SRS:MOEXception
	single: CONFigure:LTE:MEASurement<Instance>:SRS:HDMode

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:SRS:VIEW
	CONFigure:LTE:MEASurement<Instance>:SRS:TOUT
	CONFigure:LTE:MEASurement<Instance>:SRS:REPetition
	CONFigure:LTE:MEASurement<Instance>:SRS:SCONdition
	CONFigure:LTE:MEASurement<Instance>:SRS:MOEXception
	CONFigure:LTE:MEASurement<Instance>:SRS:HDMode



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.Srs.SrsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.srs.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_LteMeas_Srs_Limit.rst
	Configure_LteMeas_Srs_Scount.rst