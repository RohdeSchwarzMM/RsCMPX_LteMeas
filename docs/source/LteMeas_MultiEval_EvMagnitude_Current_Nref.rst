Nref
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:CURRent:NREF
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:CURRent:NREF

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:CURRent:NREF
	FETCh:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:CURRent:NREF



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.EvMagnitude.Current.Nref.NrefCls
	:members:
	:undoc-members:
	:noindex: