Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:SCC:MAXimum
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:SCC:MAXimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:SCC:MAXimum
	FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:SCC:MAXimum



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Pmonitor.Scc.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: