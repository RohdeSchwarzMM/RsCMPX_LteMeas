Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:EXTReme
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:EXTReme

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:EXTReme
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:EXTReme



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.SeMask.Obw.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: