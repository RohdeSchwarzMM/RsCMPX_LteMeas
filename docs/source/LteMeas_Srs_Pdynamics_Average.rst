Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:SRS:PDYNamics:AVERage
	single: FETCh:LTE:MEASurement<Instance>:SRS:PDYNamics:AVERage
	single: CALCulate:LTE:MEASurement<Instance>:SRS:PDYNamics:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:SRS:PDYNamics:AVERage
	FETCh:LTE:MEASurement<Instance>:SRS:PDYNamics:AVERage
	CALCulate:LTE:MEASurement<Instance>:SRS:PDYNamics:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Srs.Pdynamics.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: