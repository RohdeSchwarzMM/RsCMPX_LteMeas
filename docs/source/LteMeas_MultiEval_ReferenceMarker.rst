ReferenceMarker
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ReferenceMarker.ReferenceMarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.referenceMarker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_ReferenceMarker_EvMagnitude.rst
	LteMeas_MultiEval_ReferenceMarker_Merror.rst
	LteMeas_MultiEval_ReferenceMarker_Pdynamics.rst
	LteMeas_MultiEval_ReferenceMarker_Perror.rst
	LteMeas_MultiEval_ReferenceMarker_Pmonitor.rst