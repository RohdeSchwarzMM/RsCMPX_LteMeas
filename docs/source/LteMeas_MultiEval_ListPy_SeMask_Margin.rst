Margin
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.SeMask.Margin.MarginCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.listPy.seMask.margin.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_ListPy_SeMask_Margin_Area.rst