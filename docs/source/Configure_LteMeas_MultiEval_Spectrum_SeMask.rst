SeMask
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:SPECtrum:SEMask:MFILter

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:SPECtrum:SEMask:MFILter



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.Spectrum.SeMask.SeMaskCls
	:members:
	:undoc-members:
	:noindex: