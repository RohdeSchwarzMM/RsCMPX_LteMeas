Phase
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:TRACe:ESFLatness:PHASe
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:ESFLatness:PHASe

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:TRACe:ESFLatness:PHASe
	FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:ESFLatness:PHASe



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Trace.EsFlatness.Phase.PhaseCls
	:members:
	:undoc-members:
	:noindex: