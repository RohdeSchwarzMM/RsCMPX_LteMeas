ChannelBw1st<FirstChannelBw>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Bw100 .. Bw200
	rc = driver.configure.lteMeas.multiEval.limit.aclr.eutra.carrierAggregation.channelBw1st.repcap_firstChannelBw_get()
	driver.configure.lteMeas.multiEval.limit.aclr.eutra.carrierAggregation.channelBw1st.repcap_firstChannelBw_set(repcap.FirstChannelBw.Bw100)





.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.Limit.Aclr.Eutra.CarrierAggregation.ChannelBw1st.ChannelBw1stCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.multiEval.limit.aclr.eutra.carrierAggregation.channelBw1st.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_LteMeas_MultiEval_Limit_Aclr_Eutra_CarrierAggregation_ChannelBw1st_ChannelBw2nd.rst