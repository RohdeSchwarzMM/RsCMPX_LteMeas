RbIndex
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:IEMission:MARGin:EXTReme:RBINdex

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:IEMission:MARGin:EXTReme:RBINdex



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Segment.InbandEmission.Margin.Extreme.RbIndex.RbIndexCls
	:members:
	:undoc-members:
	:noindex: