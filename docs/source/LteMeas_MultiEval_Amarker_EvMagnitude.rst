EvMagnitude
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:AMARker<No>:EVMagnitude

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:AMARker<No>:EVMagnitude



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Amarker.EvMagnitude.EvMagnitudeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.amarker.evMagnitude.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_Amarker_EvMagnitude_Peak.rst