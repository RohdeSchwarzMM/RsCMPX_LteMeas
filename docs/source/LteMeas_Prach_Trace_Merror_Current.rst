Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:PRACh:TRACe:MERRor:CURRent
	single: FETCh:LTE:MEASurement<Instance>:PRACh:TRACe:MERRor:CURRent

.. code-block:: python

	READ:LTE:MEASurement<Instance>:PRACh:TRACe:MERRor:CURRent
	FETCh:LTE:MEASurement<Instance>:PRACh:TRACe:MERRor:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Prach.Trace.Merror.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: