ChannelBw<ChannelBw>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Bw14 .. Bw200
	rc = driver.configure.lteMeas.multiEval.modulation.ewLength.channelBw.repcap_channelBw_get()
	driver.configure.lteMeas.multiEval.modulation.ewLength.channelBw.repcap_channelBw_set(repcap.ChannelBw.Bw14)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:MODulation:EWLength:CBANdwidth<Band>

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:MODulation:EWLength:CBANdwidth<Band>



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.Modulation.EwLength.ChannelBw.ChannelBwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.multiEval.modulation.ewLength.channelBw.clone()