Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:SCC:MINimum
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:SCC:MINimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:SCC:MINimum
	FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:SCC:MINimum



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Pmonitor.Scc.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: