Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:CURRent
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:CURRent

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:CURRent
	FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Trace.SeMask.Rbw.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: