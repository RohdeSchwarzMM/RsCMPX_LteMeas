DsIndex
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:PRACh:MODulation:DSINdex

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:PRACh:MODulation:DSINdex



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Prach.Modulation.DsIndex.DsIndexCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.prach.modulation.dsIndex.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_Prach_Modulation_DsIndex_Preamble.rst