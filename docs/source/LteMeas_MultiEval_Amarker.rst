Amarker<AbsMarker>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.lteMeas.multiEval.amarker.repcap_absMarker_get()
	driver.lteMeas.multiEval.amarker.repcap_absMarker_set(repcap.AbsMarker.Nr1)





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Amarker.AmarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.amarker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_Amarker_EvMagnitude.rst
	LteMeas_MultiEval_Amarker_Merror.rst
	LteMeas_MultiEval_Amarker_Pdynamics.rst
	LteMeas_MultiEval_Amarker_Perror.rst
	LteMeas_MultiEval_Amarker_Pmonitor.rst