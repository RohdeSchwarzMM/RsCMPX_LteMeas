Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:MAXimum
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:MAXimum
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:MAXimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:MAXimum
	FETCh:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:MAXimum
	CALCulate:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:MAXimum



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.EvMagnitude.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.evMagnitude.maximum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_EvMagnitude_Maximum_Nref.rst