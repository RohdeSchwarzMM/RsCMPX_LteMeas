Scc
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Pmonitor.Scc.SccCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.pmonitor.scc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_Pmonitor_Scc_Average.rst
	LteMeas_MultiEval_Pmonitor_Scc_Current.rst
	LteMeas_MultiEval_Pmonitor_Scc_Maximum.rst
	LteMeas_MultiEval_Pmonitor_Scc_Minimum.rst
	LteMeas_MultiEval_Pmonitor_Scc_StandardDev.rst