Nref
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:MAXimum:NREF
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:MAXimum:NREF

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:MAXimum:NREF
	FETCh:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:MAXimum:NREF



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.EvMagnitude.Maximum.Nref.NrefCls
	:members:
	:undoc-members:
	:noindex: