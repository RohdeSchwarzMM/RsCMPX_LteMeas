Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:PRACh:TRACe:PDYNamics:CURRent
	single: FETCh:LTE:MEASurement<Instance>:PRACh:TRACe:PDYNamics:CURRent

.. code-block:: python

	READ:LTE:MEASurement<Instance>:PRACh:TRACe:PDYNamics:CURRent
	FETCh:LTE:MEASurement<Instance>:PRACh:TRACe:PDYNamics:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Prach.Trace.Pdynamics.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: