CarrierAggregation
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.ListPy.Segment.CarrierAggregation.CarrierAggregationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.multiEval.listPy.segment.carrierAggregation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_LteMeas_MultiEval_ListPy_Segment_CarrierAggregation_AcSpacing.rst
	Configure_LteMeas_MultiEval_ListPy_Segment_CarrierAggregation_Mcarrier.rst