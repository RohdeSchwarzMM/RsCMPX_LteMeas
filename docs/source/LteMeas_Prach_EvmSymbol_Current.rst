Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:PRACh:EVMSymbol:CURRent
	single: FETCh:LTE:MEASurement<Instance>:PRACh:EVMSymbol:CURRent
	single: CALCulate:LTE:MEASurement<Instance>:PRACh:EVMSymbol:CURRent

.. code-block:: python

	READ:LTE:MEASurement<Instance>:PRACh:EVMSymbol:CURRent
	FETCh:LTE:MEASurement<Instance>:PRACh:EVMSymbol:CURRent
	CALCulate:LTE:MEASurement<Instance>:PRACh:EVMSymbol:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Prach.EvmSymbol.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: