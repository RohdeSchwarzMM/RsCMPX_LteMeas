Eutra
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Aclr.Eutra.EutraCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.listPy.aclr.eutra.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_ListPy_Aclr_Eutra_Average.rst
	LteMeas_MultiEval_ListPy_Aclr_Eutra_Current.rst
	LteMeas_MultiEval_ListPy_Aclr_Eutra_Negativ.rst
	LteMeas_MultiEval_ListPy_Aclr_Eutra_Positiv.rst