Nrb
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:RBALlocation:NRB:PSCCh
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:RBALlocation:NRB:PSSCh
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:RBALlocation:NRB

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:RBALlocation:NRB:PSCCh
	CONFigure:LTE:MEASurement<Instance>:MEValuation:RBALlocation:NRB:PSSCh
	CONFigure:LTE:MEASurement<Instance>:MEValuation:RBALlocation:NRB



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.RbAllocation.Nrb.NrbCls
	:members:
	:undoc-members:
	:noindex: