Dmarker<DeltaMarker>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.lteMeas.multiEval.dmarker.repcap_deltaMarker_get()
	driver.lteMeas.multiEval.dmarker.repcap_deltaMarker_set(repcap.DeltaMarker.Nr1)





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Dmarker.DmarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.dmarker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_Dmarker_EvMagnitude.rst
	LteMeas_MultiEval_Dmarker_Merror.rst
	LteMeas_MultiEval_Dmarker_Pdynamics.rst
	LteMeas_MultiEval_Dmarker_Perror.rst
	LteMeas_MultiEval_Dmarker_Pmonitor.rst