Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:SEMask:MAXimum
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:SEMask:MAXimum
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:SEMask:MAXimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:SEMask:MAXimum
	FETCh:LTE:MEASurement<Instance>:MEValuation:SEMask:MAXimum
	CALCulate:LTE:MEASurement<Instance>:MEValuation:SEMask:MAXimum



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.SeMask.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: