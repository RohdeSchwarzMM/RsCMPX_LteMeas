Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PPOWer:MINimum
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PPOWer:MINimum

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PPOWer:MINimum
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PPOWer:MINimum



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Modulation.Ppower.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: