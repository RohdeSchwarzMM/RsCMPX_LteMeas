Minr<MinRange>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.lteMeas.multiEval.listPy.esFlatness.minr.repcap_minRange_get()
	driver.lteMeas.multiEval.listPy.esFlatness.minr.repcap_minRange_set(repcap.MinRange.Nr1)





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.EsFlatness.Minr.MinrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.listPy.esFlatness.minr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_ListPy_EsFlatness_Minr_Average.rst
	LteMeas_MultiEval_ListPy_EsFlatness_Minr_Current.rst
	LteMeas_MultiEval_ListPy_EsFlatness_Minr_Extreme.rst
	LteMeas_MultiEval_ListPy_EsFlatness_Minr_StandardDev.rst