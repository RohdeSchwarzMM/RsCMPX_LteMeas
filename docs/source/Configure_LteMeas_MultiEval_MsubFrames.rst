MsubFrames
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:MSUBframes

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:MSUBframes



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.MsubFrames.MsubFramesCls
	:members:
	:undoc-members:
	:noindex: