Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:AVERage
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:AVERage

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:AVERage
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Power.TxPower.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: