Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:ESFLatness:RIPPle<nr>:AVERage
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:ESFLatness:RIPPle<nr>:AVERage

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:ESFLatness:RIPPle<nr>:AVERage
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:ESFLatness:RIPPle<nr>:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.EsFlatness.Ripple.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: