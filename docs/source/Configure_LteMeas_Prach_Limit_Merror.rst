Merror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:LIMit:MERRor

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:PRACh:LIMit:MERRor



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.Prach.Limit.Merror.MerrorCls
	:members:
	:undoc-members:
	:noindex: