Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:ACLR:AVERage
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:ACLR:AVERage
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:ACLR:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:ACLR:AVERage
	FETCh:LTE:MEASurement<Instance>:MEValuation:ACLR:AVERage
	CALCulate:LTE:MEASurement<Instance>:MEValuation:ACLR:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Aclr.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: