Pdynamics
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Srs.Pdynamics.PdynamicsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.srs.pdynamics.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_Srs_Pdynamics_Average.rst
	LteMeas_Srs_Pdynamics_Current.rst
	LteMeas_Srs_Pdynamics_Maximum.rst
	LteMeas_Srs_Pdynamics_Minimum.rst
	LteMeas_Srs_Pdynamics_StandardDev.rst