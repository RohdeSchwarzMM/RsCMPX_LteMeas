Pcc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation[:PCC]:PLCid

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation[:PCC]:PLCid



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.Pcc.PccCls
	:members:
	:undoc-members:
	:noindex: