Perror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:AMARker<No>:PERRor

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:AMARker<No>:PERRor



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Amarker.Perror.PerrorCls
	:members:
	:undoc-members:
	:noindex: