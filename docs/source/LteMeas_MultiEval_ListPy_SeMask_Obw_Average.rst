Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:AVERage
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:AVERage

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:AVERage
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.SeMask.Obw.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: