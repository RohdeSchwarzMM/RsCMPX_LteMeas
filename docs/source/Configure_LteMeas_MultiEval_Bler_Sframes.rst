Sframes
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:BLER:SFRames

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:BLER:SFRames



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.Bler.Sframes.SframesCls
	:members:
	:undoc-members:
	:noindex: