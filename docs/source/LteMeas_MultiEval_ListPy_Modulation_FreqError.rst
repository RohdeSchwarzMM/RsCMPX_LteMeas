FreqError
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Modulation.FreqError.FreqErrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.listPy.modulation.freqError.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_ListPy_Modulation_FreqError_Average.rst
	LteMeas_MultiEval_ListPy_Modulation_FreqError_Current.rst
	LteMeas_MultiEval_ListPy_Modulation_FreqError_Extreme.rst
	LteMeas_MultiEval_ListPy_Modulation_FreqError_StandardDev.rst