Merror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:AMARker<No>:MERRor

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:AMARker<No>:MERRor



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Amarker.Merror.MerrorCls
	:members:
	:undoc-members:
	:noindex: