Pcc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:RFSettings[:PCC]:FREQuency

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:RFSettings[:PCC]:FREQuency



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.RfSettings.Pcc.PccCls
	:members:
	:undoc-members:
	:noindex: