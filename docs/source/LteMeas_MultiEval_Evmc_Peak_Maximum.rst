Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:EVMC:PEAK:MAXimum
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:EVMC:PEAK:MAXimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:EVMC:PEAK:MAXimum
	FETCh:LTE:MEASurement<Instance>:MEValuation:EVMC:PEAK:MAXimum



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Evmc.Peak.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: