Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:CURRent
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:CURRent

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:CURRent
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.SeMask.TxPower.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: