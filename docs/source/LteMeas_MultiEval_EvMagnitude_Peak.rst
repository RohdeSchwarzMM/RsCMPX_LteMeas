Peak
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.EvMagnitude.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.evMagnitude.peak.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_EvMagnitude_Peak_Average.rst
	LteMeas_MultiEval_EvMagnitude_Peak_Current.rst
	LteMeas_MultiEval_EvMagnitude_Peak_Maximum.rst