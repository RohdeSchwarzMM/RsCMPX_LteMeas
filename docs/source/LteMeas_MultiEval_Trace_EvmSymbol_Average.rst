Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:TRACe:EVMSymbol:AVERage
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:EVMSymbol:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:TRACe:EVMSymbol:AVERage
	FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:EVMSymbol:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Trace.EvmSymbol.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: