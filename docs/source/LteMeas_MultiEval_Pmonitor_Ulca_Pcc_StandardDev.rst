StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:ULCA:PCC:SDEViation
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:ULCA:PCC:SDEViation

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:ULCA:PCC:SDEViation
	FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:ULCA:PCC:SDEViation



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Pmonitor.Ulca.Pcc.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: