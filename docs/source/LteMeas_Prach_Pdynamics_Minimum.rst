Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:PRACh:PDYNamics:MINimum
	single: FETCh:LTE:MEASurement<Instance>:PRACh:PDYNamics:MINimum
	single: CALCulate:LTE:MEASurement<Instance>:PRACh:PDYNamics:MINimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:PRACh:PDYNamics:MINimum
	FETCh:LTE:MEASurement<Instance>:PRACh:PDYNamics:MINimum
	CALCulate:LTE:MEASurement<Instance>:PRACh:PDYNamics:MINimum



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Prach.Pdynamics.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: