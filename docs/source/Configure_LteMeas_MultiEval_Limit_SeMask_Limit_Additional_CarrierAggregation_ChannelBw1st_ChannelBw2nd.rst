ChannelBw2nd<SecondChannelBw>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Bw50 .. Bw200
	rc = driver.configure.lteMeas.multiEval.limit.seMask.limit.additional.carrierAggregation.channelBw1st.channelBw2nd.repcap_secondChannelBw_get()
	driver.configure.lteMeas.multiEval.limit.seMask.limit.additional.carrierAggregation.channelBw1st.channelBw2nd.repcap_secondChannelBw_set(repcap.SecondChannelBw.Bw50)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:SEMask:LIMit<nr>:ADDitional<Table>:CAGGregation:CBANdwidth<Band1>:CBANdwidth<Band2>

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:SEMask:LIMit<nr>:ADDitional<Table>:CAGGregation:CBANdwidth<Band1>:CBANdwidth<Band2>



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.Limit.SeMask.Limit.Additional.CarrierAggregation.ChannelBw1st.ChannelBw2nd.ChannelBw2ndCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.multiEval.limit.seMask.limit.additional.carrierAggregation.channelBw1st.channelBw2nd.clone()