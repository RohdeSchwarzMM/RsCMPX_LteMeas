Scc<SecondaryCC>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: CC1 .. CC7
	rc = driver.configure.lteMeas.multiEval.listPy.segment.scc.repcap_secondaryCC_get()
	driver.configure.lteMeas.multiEval.listPy.segment.scc.repcap_secondaryCC_set(repcap.SecondaryCC.CC1)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SCC<c>

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SCC<c>



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.ListPy.Segment.Scc.SccCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.multiEval.listPy.segment.scc.clone()