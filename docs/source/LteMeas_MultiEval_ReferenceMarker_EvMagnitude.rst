EvMagnitude
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:REFMarker:EVMagnitude

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:REFMarker:EVMagnitude



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ReferenceMarker.EvMagnitude.EvMagnitudeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.referenceMarker.evMagnitude.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_ReferenceMarker_EvMagnitude_Peak.rst