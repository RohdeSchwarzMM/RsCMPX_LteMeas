Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:PEAK:AVERage
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:PEAK:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:PEAK:AVERage
	FETCh:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:PEAK:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.EvMagnitude.Peak.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: