CarrierAggregation
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.CarrierAggregation.CarrierAggregationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.carrierAggregation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_LteMeas_CarrierAggregation_ChannelBw.rst
	Configure_LteMeas_CarrierAggregation_Frequency.rst
	Configure_LteMeas_CarrierAggregation_Maping.rst
	Configure_LteMeas_CarrierAggregation_Mcarrier.rst
	Configure_LteMeas_CarrierAggregation_Mode.rst
	Configure_LteMeas_CarrierAggregation_Scc.rst