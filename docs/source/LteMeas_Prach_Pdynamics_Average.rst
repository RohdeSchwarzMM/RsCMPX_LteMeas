Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:PRACh:PDYNamics:AVERage
	single: FETCh:LTE:MEASurement<Instance>:PRACh:PDYNamics:AVERage
	single: CALCulate:LTE:MEASurement<Instance>:PRACh:PDYNamics:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:PRACh:PDYNamics:AVERage
	FETCh:LTE:MEASurement<Instance>:PRACh:PDYNamics:AVERage
	CALCulate:LTE:MEASurement<Instance>:PRACh:PDYNamics:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Prach.Pdynamics.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: