Trace
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Srs.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.srs.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_Srs_Trace_Pdynamics.rst