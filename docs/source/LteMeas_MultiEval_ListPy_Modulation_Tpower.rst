Tpower
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Modulation.Tpower.TpowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.listPy.modulation.tpower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_ListPy_Modulation_Tpower_Average.rst
	LteMeas_MultiEval_ListPy_Modulation_Tpower_Current.rst
	LteMeas_MultiEval_ListPy_Modulation_Tpower_Maximum.rst
	LteMeas_MultiEval_ListPy_Modulation_Tpower_Minimum.rst
	LteMeas_MultiEval_ListPy_Modulation_Tpower_StandardDev.rst