StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:ESFLatness:SDEViation
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:ESFLatness:SDEViation
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:ESFLatness:SDEViation

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:ESFLatness:SDEViation
	FETCh:LTE:MEASurement<Instance>:MEValuation:ESFLatness:SDEViation
	CALCulate:LTE:MEASurement<Instance>:MEValuation:ESFLatness:SDEViation



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.EsFlatness.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: