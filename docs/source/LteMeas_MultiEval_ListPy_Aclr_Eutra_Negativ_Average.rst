Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:ACLR:EUTRa:NEGativ:AVERage
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:ACLR:EUTRa:NEGativ:AVERage

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:ACLR:EUTRa:NEGativ:AVERage
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:ACLR:EUTRa:NEGativ:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Aclr.Eutra.Negativ.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: