Merror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:REFMarker:MERRor

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:REFMarker:MERRor



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ReferenceMarker.Merror.MerrorCls
	:members:
	:undoc-members:
	:noindex: