Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:PRACh:TRACe:PERRor:CURRent
	single: FETCh:LTE:MEASurement<Instance>:PRACh:TRACe:PERRor:CURRent

.. code-block:: python

	READ:LTE:MEASurement<Instance>:PRACh:TRACe:PERRor:CURRent
	FETCh:LTE:MEASurement<Instance>:PRACh:TRACe:PERRor:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Prach.Trace.Perror.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: