ListPy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:OSINdex
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:PLCMode
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:CMODe
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:NCONnections
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:OSINdex
	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:PLCMode
	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:CMODe
	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:NCONnections
	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.ListPy.ListPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.multiEval.listPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_LteMeas_MultiEval_ListPy_Lrange.rst
	Configure_LteMeas_MultiEval_ListPy_Segment.rst
	Configure_LteMeas_MultiEval_ListPy_SingleCmw.rst