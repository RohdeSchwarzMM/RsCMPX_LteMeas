Aclr
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Aclr.AclrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.aclr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_Aclr_Average.rst
	LteMeas_MultiEval_Aclr_Current.rst
	LteMeas_MultiEval_Aclr_Dallocation.rst
	LteMeas_MultiEval_Aclr_DchType.rst