Scc<SecondaryCC>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: CC1 .. CC7
	rc = driver.lteMeas.multiEval.trace.pmonitor.ulca.scc.repcap_secondaryCC_get()
	driver.lteMeas.multiEval.trace.pmonitor.ulca.scc.repcap_secondaryCC_set(repcap.SecondaryCC.CC1)



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:TRACe:PMONitor:ULCA:SCC<Nr>
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:PMONitor:ULCA:SCC<Nr>

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:TRACe:PMONitor:ULCA:SCC<Nr>
	FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:PMONitor:ULCA:SCC<Nr>



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Trace.Pmonitor.Ulca.Scc.SccCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.trace.pmonitor.ulca.scc.clone()