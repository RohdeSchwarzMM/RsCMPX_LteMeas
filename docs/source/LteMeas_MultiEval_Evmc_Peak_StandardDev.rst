StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:EVMC:PEAK:SDEViation
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:EVMC:PEAK:SDEViation

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:EVMC:PEAK:SDEViation
	FETCh:LTE:MEASurement<Instance>:MEValuation:EVMC:PEAK:SDEViation



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Evmc.Peak.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: