Iemissions
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:MEASurement<Instance>:MEValuation:LIMit:IEMissions:SCC
	single: SENSe:LTE:MEASurement<Instance>:MEValuation:LIMit:IEMissions[:PCC]

.. code-block:: python

	SENSe:LTE:MEASurement<Instance>:MEValuation:LIMit:IEMissions:SCC
	SENSe:LTE:MEASurement<Instance>:MEValuation:LIMit:IEMissions[:PCC]



.. autoclass:: RsCMPX_LteMeas.Implementations.Sense.LteMeas.MultiEval.Limit.Iemissions.IemissionsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.lteMeas.multiEval.limit.iemissions.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_LteMeas_MultiEval_Limit_Iemissions_Ulca.rst