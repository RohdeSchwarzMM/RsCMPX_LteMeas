DpfOffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:PRACh:MODulation:DPFoffset

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:PRACh:MODulation:DPFoffset



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Prach.Modulation.DpfOffset.DpfOffsetCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.prach.modulation.dpfOffset.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_Prach_Modulation_DpfOffset_Preamble.rst