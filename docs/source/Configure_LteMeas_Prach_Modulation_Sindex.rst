Sindex
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:MODulation:SINDex:AUTO
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:MODulation:SINDex

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:PRACh:MODulation:SINDex:AUTO
	CONFigure:LTE:MEASurement<Instance>:PRACh:MODulation:SINDex



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.Prach.Modulation.Sindex.SindexCls
	:members:
	:undoc-members:
	:noindex: