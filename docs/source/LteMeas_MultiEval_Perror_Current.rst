Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PERRor:CURRent
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PERRor:CURRent
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:PERRor:CURRent

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PERRor:CURRent
	FETCh:LTE:MEASurement<Instance>:MEValuation:PERRor:CURRent
	CALCulate:LTE:MEASurement<Instance>:MEValuation:PERRor:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Perror.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.perror.current.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_Perror_Current_Nref.rst