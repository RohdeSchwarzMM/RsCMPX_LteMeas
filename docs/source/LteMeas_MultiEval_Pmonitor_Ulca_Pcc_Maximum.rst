Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:ULCA:PCC:MAXimum
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:ULCA:PCC:MAXimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:ULCA:PCC:MAXimum
	FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:ULCA:PCC:MAXimum



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Pmonitor.Ulca.Pcc.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: