Pdynamics
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:REFMarker:PDYNamics

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:REFMarker:PDYNamics



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ReferenceMarker.Pdynamics.PdynamicsCls
	:members:
	:undoc-members:
	:noindex: