Perror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:LIMit:PERRor

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:PRACh:LIMit:PERRor



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.Prach.Limit.Perror.PerrorCls
	:members:
	:undoc-members:
	:noindex: