Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:LOW:AVERage
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:LOW:AVERage

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:LOW:AVERage
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:LOW:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Modulation.Evm.Rms.Low.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: