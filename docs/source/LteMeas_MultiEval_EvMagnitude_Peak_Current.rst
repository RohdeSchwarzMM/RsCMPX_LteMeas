Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:PEAK:CURRent
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:PEAK:CURRent

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:PEAK:CURRent
	FETCh:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:PEAK:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.EvMagnitude.Peak.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: