Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PDYNamics:AVERage
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PDYNamics:AVERage
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:PDYNamics:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PDYNamics:AVERage
	FETCh:LTE:MEASurement<Instance>:MEValuation:PDYNamics:AVERage
	CALCulate:LTE:MEASurement<Instance>:MEValuation:PDYNamics:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Pdynamics.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: