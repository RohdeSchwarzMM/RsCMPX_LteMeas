Average
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:IEMission:SCC<c>:MARGin:AVERage

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:IEMission:SCC<c>:MARGin:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Segment.InbandEmission.Scc.Margin.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: