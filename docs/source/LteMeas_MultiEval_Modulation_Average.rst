Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:MODulation:AVERage
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:MODulation:AVERage
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:MODulation:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:MODulation:AVERage
	FETCh:LTE:MEASurement<Instance>:MEValuation:MODulation:AVERage
	CALCulate:LTE:MEASurement<Instance>:MEValuation:MODulation:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Modulation.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: