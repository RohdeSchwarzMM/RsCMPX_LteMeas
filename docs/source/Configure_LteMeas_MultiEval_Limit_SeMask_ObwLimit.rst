ObwLimit
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.Limit.SeMask.ObwLimit.ObwLimitCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.multiEval.limit.seMask.obwLimit.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_LteMeas_MultiEval_Limit_SeMask_ObwLimit_CarrierAggregation.rst
	Configure_LteMeas_MultiEval_Limit_SeMask_ObwLimit_ChannelBw.rst