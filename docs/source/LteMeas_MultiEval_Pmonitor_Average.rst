Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:AVERage
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:AVERage
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:PMONitor:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:AVERage
	FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:AVERage
	CALCulate:LTE:MEASurement<Instance>:MEValuation:PMONitor:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Pmonitor.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: