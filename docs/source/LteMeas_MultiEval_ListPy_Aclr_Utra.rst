Utra<UtraAdjChannel>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Ch1 .. Ch2
	rc = driver.lteMeas.multiEval.listPy.aclr.utra.repcap_utraAdjChannel_get()
	driver.lteMeas.multiEval.listPy.aclr.utra.repcap_utraAdjChannel_set(repcap.UtraAdjChannel.Ch1)





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Aclr.Utra.UtraCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.listPy.aclr.utra.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_ListPy_Aclr_Utra_Negativ.rst
	LteMeas_MultiEval_ListPy_Aclr_Utra_Positiv.rst