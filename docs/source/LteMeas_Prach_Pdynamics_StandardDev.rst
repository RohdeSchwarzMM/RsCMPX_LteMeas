StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:PRACh:PDYNamics:SDEViation
	single: FETCh:LTE:MEASurement<Instance>:PRACh:PDYNamics:SDEViation
	single: CALCulate:LTE:MEASurement<Instance>:PRACh:PDYNamics:SDEViation

.. code-block:: python

	READ:LTE:MEASurement<Instance>:PRACh:PDYNamics:SDEViation
	FETCh:LTE:MEASurement<Instance>:PRACh:PDYNamics:SDEViation
	CALCulate:LTE:MEASurement<Instance>:PRACh:PDYNamics:SDEViation



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Prach.Pdynamics.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: