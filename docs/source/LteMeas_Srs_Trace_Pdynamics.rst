Pdynamics
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Srs.Trace.Pdynamics.PdynamicsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.srs.trace.pdynamics.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_Srs_Trace_Pdynamics_Average.rst
	LteMeas_Srs_Trace_Pdynamics_Current.rst
	LteMeas_Srs_Trace_Pdynamics_Maximum.rst