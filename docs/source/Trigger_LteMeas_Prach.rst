Prach
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:LTE:MEASurement<Instance>:PRACh:THReshold
	single: TRIGger:LTE:MEASurement<Instance>:PRACh:SLOPe
	single: TRIGger:LTE:MEASurement<Instance>:PRACh:TOUT
	single: TRIGger:LTE:MEASurement<Instance>:PRACh:MGAP

.. code-block:: python

	TRIGger:LTE:MEASurement<Instance>:PRACh:THReshold
	TRIGger:LTE:MEASurement<Instance>:PRACh:SLOPe
	TRIGger:LTE:MEASurement<Instance>:PRACh:TOUT
	TRIGger:LTE:MEASurement<Instance>:PRACh:MGAP



.. autoclass:: RsCMPX_LteMeas.Implementations.Trigger.LteMeas.Prach.PrachCls
	:members:
	:undoc-members:
	:noindex: