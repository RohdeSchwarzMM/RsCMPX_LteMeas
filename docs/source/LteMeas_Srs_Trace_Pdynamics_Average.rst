Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:SRS:TRACe:PDYNamics:AVERage
	single: FETCh:LTE:MEASurement<Instance>:SRS:TRACe:PDYNamics:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:SRS:TRACe:PDYNamics:AVERage
	FETCh:LTE:MEASurement<Instance>:SRS:TRACe:PDYNamics:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Srs.Trace.Pdynamics.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: