Negativ
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Aclr.Utra.Negativ.NegativCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.listPy.aclr.utra.negativ.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_ListPy_Aclr_Utra_Negativ_Average.rst
	LteMeas_MultiEval_ListPy_Aclr_Utra_Negativ_Current.rst