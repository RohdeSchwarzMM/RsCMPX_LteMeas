Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:MODulation:EXTReme
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:MODulation:EXTReme
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:MODulation:EXTReme

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:MODulation:EXTReme
	FETCh:LTE:MEASurement<Instance>:MEValuation:MODulation:EXTReme
	CALCulate:LTE:MEASurement<Instance>:MEValuation:MODulation:EXTReme



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Modulation.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: