Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:TRACe:ACLR:CURRent
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:ACLR:CURRent

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:TRACe:ACLR:CURRent
	FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:ACLR:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Trace.Aclr.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: