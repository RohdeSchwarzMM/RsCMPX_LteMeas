Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:RMS:HIGH:CURRent
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:RMS:HIGH:CURRent

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:RMS:HIGH:CURRent
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:RMS:HIGH:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Modulation.Merror.Rms.High.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: