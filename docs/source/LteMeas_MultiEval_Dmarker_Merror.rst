Merror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:DMARker<No>:MERRor

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:DMARker<No>:MERRor



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Dmarker.Merror.MerrorCls
	:members:
	:undoc-members:
	:noindex: