Maxr<MaxRange>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.lteMeas.multiEval.listPy.esFlatness.maxr.repcap_maxRange_get()
	driver.lteMeas.multiEval.listPy.esFlatness.maxr.repcap_maxRange_set(repcap.MaxRange.Nr1)





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.EsFlatness.Maxr.MaxrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.listPy.esFlatness.maxr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_ListPy_EsFlatness_Maxr_Average.rst
	LteMeas_MultiEval_ListPy_EsFlatness_Maxr_Current.rst
	LteMeas_MultiEval_ListPy_EsFlatness_Maxr_Extreme.rst
	LteMeas_MultiEval_ListPy_EsFlatness_Maxr_StandardDev.rst