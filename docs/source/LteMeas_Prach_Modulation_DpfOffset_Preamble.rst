Preamble<Preamble>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr400
	rc = driver.lteMeas.prach.modulation.dpfOffset.preamble.repcap_preamble_get()
	driver.lteMeas.prach.modulation.dpfOffset.preamble.repcap_preamble_set(repcap.Preamble.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:PRACh:MODulation:DPFoffset:PREamble<Number>

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:PRACh:MODulation:DPFoffset:PREamble<Number>



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Prach.Modulation.DpfOffset.Preamble.PreambleCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.prach.modulation.dpfOffset.preamble.clone()