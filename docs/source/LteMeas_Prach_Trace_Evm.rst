Evm
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Prach.Trace.Evm.EvmCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.prach.trace.evm.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_Prach_Trace_Evm_Average.rst
	LteMeas_Prach_Trace_Evm_Current.rst
	LteMeas_Prach_Trace_Evm_Maximum.rst