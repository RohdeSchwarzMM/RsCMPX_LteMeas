Pformat<PreambleFormat>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Fmt1 .. Fmt5
	rc = driver.configure.lteMeas.prach.modulation.ewLength.pformat.repcap_preambleFormat_get()
	driver.configure.lteMeas.prach.modulation.ewLength.pformat.repcap_preambleFormat_set(repcap.PreambleFormat.Fmt1)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:MODulation:EWLength:PFORmat<PreambleFormat>

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:PRACh:MODulation:EWLength:PFORmat<PreambleFormat>



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.Prach.Modulation.EwLength.Pformat.PformatCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.prach.modulation.ewLength.pformat.clone()