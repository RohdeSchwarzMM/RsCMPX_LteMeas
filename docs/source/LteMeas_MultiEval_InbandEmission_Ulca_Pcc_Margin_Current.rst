Current
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:IEMission:ULCA[:PCC]:MARGin:CURRent

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:IEMission:ULCA[:PCC]:MARGin:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.InbandEmission.Ulca.Pcc.Margin.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.inbandEmission.ulca.pcc.margin.current.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_InbandEmission_Ulca_Pcc_Margin_Current_RbIndex.rst