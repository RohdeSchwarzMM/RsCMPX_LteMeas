Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:SEMask:MINimum
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:SEMask:MINimum
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:SEMask:MINimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:SEMask:MINimum
	FETCh:LTE:MEASurement<Instance>:MEValuation:SEMask:MINimum
	CALCulate:LTE:MEASurement<Instance>:MEValuation:SEMask:MINimum



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.SeMask.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: