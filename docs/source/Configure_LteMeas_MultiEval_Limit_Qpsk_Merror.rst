Merror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QPSK:MERRor

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QPSK:MERRor



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.Limit.Qpsk.Merror.MerrorCls
	:members:
	:undoc-members:
	:noindex: