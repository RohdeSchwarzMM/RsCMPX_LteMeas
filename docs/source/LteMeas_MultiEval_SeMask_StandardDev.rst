StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:SEMask:SDEViation
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:SEMask:SDEViation
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:SEMask:SDEViation

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:SEMask:SDEViation
	FETCh:LTE:MEASurement<Instance>:MEValuation:SEMask:SDEViation
	CALCulate:LTE:MEASurement<Instance>:MEValuation:SEMask:SDEViation



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.SeMask.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: