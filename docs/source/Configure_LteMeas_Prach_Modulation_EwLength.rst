EwLength
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:MODulation:EWLength

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:PRACh:MODulation:EWLength



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.Prach.Modulation.EwLength.EwLengthCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.prach.modulation.ewLength.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_LteMeas_Prach_Modulation_EwLength_Pformat.rst