Margin
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.InbandEmission.Margin.MarginCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.listPy.inbandEmission.margin.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_ListPy_InbandEmission_Margin_Average.rst
	LteMeas_MultiEval_ListPy_InbandEmission_Margin_Current.rst
	LteMeas_MultiEval_ListPy_InbandEmission_Margin_Extreme.rst
	LteMeas_MultiEval_ListPy_InbandEmission_Margin_RbIndex.rst
	LteMeas_MultiEval_ListPy_InbandEmission_Margin_StandardDev.rst