CarrierAggregation
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.Limit.SeMask.Limit.Additional.CarrierAggregation.CarrierAggregationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.multiEval.limit.seMask.limit.additional.carrierAggregation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_LteMeas_MultiEval_Limit_SeMask_Limit_Additional_CarrierAggregation_ChannelBw1st.rst
	Configure_LteMeas_MultiEval_Limit_SeMask_Limit_Additional_CarrierAggregation_Ocombination.rst