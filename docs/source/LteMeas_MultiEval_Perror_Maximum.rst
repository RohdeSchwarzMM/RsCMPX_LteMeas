Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PERRor:MAXimum
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PERRor:MAXimum
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:PERRor:MAXimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PERRor:MAXimum
	FETCh:LTE:MEASurement<Instance>:MEValuation:PERRor:MAXimum
	CALCulate:LTE:MEASurement<Instance>:MEValuation:PERRor:MAXimum



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Perror.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.perror.maximum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_Perror_Maximum_Nref.rst