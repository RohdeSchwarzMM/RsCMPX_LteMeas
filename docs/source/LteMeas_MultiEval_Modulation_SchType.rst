SchType
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:MODulation:SCHType

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:MODulation:SCHType



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Modulation.SchType.SchTypeCls
	:members:
	:undoc-members:
	:noindex: