Dmodulation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:MODulation:DMODulation

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:MODulation:DMODulation



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Modulation.Dmodulation.DmodulationCls
	:members:
	:undoc-members:
	:noindex: