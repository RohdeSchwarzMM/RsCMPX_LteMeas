Network
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:NETWork:RFPSharing
	single: CONFigure:LTE:MEASurement<Instance>:NETWork:BAND
	single: CONFigure:LTE:MEASurement<Instance>:NETWork:DMODe

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:NETWork:RFPSharing
	CONFigure:LTE:MEASurement<Instance>:NETWork:BAND
	CONFigure:LTE:MEASurement<Instance>:NETWork:DMODe



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.Network.NetworkCls
	:members:
	:undoc-members:
	:noindex: