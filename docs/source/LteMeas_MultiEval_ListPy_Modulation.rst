Modulation
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.listPy.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_ListPy_Modulation_Dallocation.rst
	LteMeas_MultiEval_ListPy_Modulation_DchType.rst
	LteMeas_MultiEval_ListPy_Modulation_Dmodulation.rst
	LteMeas_MultiEval_ListPy_Modulation_Evm.rst
	LteMeas_MultiEval_ListPy_Modulation_FreqError.rst
	LteMeas_MultiEval_ListPy_Modulation_IqOffset.rst
	LteMeas_MultiEval_ListPy_Modulation_Merror.rst
	LteMeas_MultiEval_ListPy_Modulation_Perror.rst
	LteMeas_MultiEval_ListPy_Modulation_Ppower.rst
	LteMeas_MultiEval_ListPy_Modulation_Psd.rst
	LteMeas_MultiEval_ListPy_Modulation_SchType.rst
	LteMeas_MultiEval_ListPy_Modulation_Terror.rst
	LteMeas_MultiEval_ListPy_Modulation_Tpower.rst