Pcc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>[:PCC]:CBANdwidth

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>[:PCC]:CBANdwidth



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.Pcc.PccCls
	:members:
	:undoc-members:
	:noindex: