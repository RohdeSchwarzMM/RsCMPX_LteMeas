Modulation
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:MODulation:EQUalizer
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:MODulation:MSCHeme
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:MODulation:LLOCation

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:MODulation:EQUalizer
	CONFigure:LTE:MEASurement<Instance>:MEValuation:MODulation:MSCHeme
	CONFigure:LTE:MEASurement<Instance>:MEValuation:MODulation:LLOCation



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.multiEval.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_LteMeas_MultiEval_Modulation_CarrierAggregation.rst
	Configure_LteMeas_MultiEval_Modulation_EePeriods.rst
	Configure_LteMeas_MultiEval_Modulation_EwLength.rst