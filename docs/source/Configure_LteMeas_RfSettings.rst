RfSettings
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:RFSettings:EATTenuation
	single: CONFigure:LTE:MEASurement<Instance>:RFSettings:UMARgin
	single: CONFigure:LTE:MEASurement<Instance>:RFSettings:ENPower
	single: CONFigure:LTE:MEASurement<Instance>:RFSettings:FOFFset
	single: CONFigure:LTE:MEASurement<Instance>:RFSettings:MLOFfset

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:RFSettings:EATTenuation
	CONFigure:LTE:MEASurement<Instance>:RFSettings:UMARgin
	CONFigure:LTE:MEASurement<Instance>:RFSettings:ENPower
	CONFigure:LTE:MEASurement<Instance>:RFSettings:FOFFset
	CONFigure:LTE:MEASurement<Instance>:RFSettings:MLOFfset



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.rfSettings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_LteMeas_RfSettings_Cc.rst
	Configure_LteMeas_RfSettings_Pcc.rst
	Configure_LteMeas_RfSettings_Scc.rst