Pcc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:PMONitor[:PCC]
	single: READ:LTE:MEASurement<Instance>:MEValuation:TRACe:PMONitor[:PCC]

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:PMONitor[:PCC]
	READ:LTE:MEASurement<Instance>:MEValuation:TRACe:PMONitor[:PCC]



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Trace.Pmonitor.Pcc.PccCls
	:members:
	:undoc-members:
	:noindex: