Power
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:POWer:HDMode

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:PRACh:POWer:HDMode



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.Prach.Power.PowerCls
	:members:
	:undoc-members:
	:noindex: