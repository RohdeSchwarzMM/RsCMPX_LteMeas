Obw
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.SeMask.Obw.ObwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.listPy.seMask.obw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_ListPy_SeMask_Obw_Average.rst
	LteMeas_MultiEval_ListPy_SeMask_Obw_Current.rst
	LteMeas_MultiEval_ListPy_SeMask_Obw_Extreme.rst
	LteMeas_MultiEval_ListPy_SeMask_Obw_StandardDev.rst