EvmSymbol
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult:EVMagnitude:EVMSymbol

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult:EVMagnitude:EVMSymbol



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.Result.EvMagnitude.EvmSymbol.EvmSymbolCls
	:members:
	:undoc-members:
	:noindex: