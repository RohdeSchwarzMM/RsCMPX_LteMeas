Average
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:IEMission:ULCA:SCC<Nr>:MARGin:AVERage

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:IEMission:ULCA:SCC<Nr>:MARGin:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.InbandEmission.Ulca.Scc.Margin.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: