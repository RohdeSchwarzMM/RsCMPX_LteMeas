Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:CC<Nr>:MAXimum
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:CC<Nr>:MAXimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:CC<Nr>:MAXimum
	FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:CC<Nr>:MAXimum



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Pmonitor.Cc.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: