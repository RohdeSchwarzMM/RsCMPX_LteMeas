Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:PRACh:TRACe:PERRor:MAXimum
	single: FETCh:LTE:MEASurement<Instance>:PRACh:TRACe:PERRor:MAXimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:PRACh:TRACe:PERRor:MAXimum
	FETCh:LTE:MEASurement<Instance>:PRACh:TRACe:PERRor:MAXimum



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Prach.Trace.Perror.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: