Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:CC<Nr>:CURRent
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:CC<Nr>:CURRent

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:CC<Nr>:CURRent
	FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:CC<Nr>:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Pmonitor.Cc.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: