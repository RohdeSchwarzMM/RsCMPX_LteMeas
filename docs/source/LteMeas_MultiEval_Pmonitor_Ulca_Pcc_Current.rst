Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:ULCA:PCC:CURRent
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:ULCA:PCC:CURRent

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:ULCA:PCC:CURRent
	FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:ULCA:PCC:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Pmonitor.Ulca.Pcc.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: