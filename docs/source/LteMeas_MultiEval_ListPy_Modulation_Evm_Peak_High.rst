High
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Modulation.Evm.Peak.High.HighCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.listPy.modulation.evm.peak.high.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_ListPy_Modulation_Evm_Peak_High_Average.rst
	LteMeas_MultiEval_ListPy_Modulation_Evm_Peak_High_Current.rst
	LteMeas_MultiEval_ListPy_Modulation_Evm_Peak_High_Extreme.rst
	LteMeas_MultiEval_ListPy_Modulation_Evm_Peak_High_StandardDev.rst