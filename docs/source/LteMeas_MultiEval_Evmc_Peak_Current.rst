Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:EVMC:PEAK:CURRent
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:EVMC:PEAK:CURRent

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:EVMC:PEAK:CURRent
	FETCh:LTE:MEASurement<Instance>:MEValuation:EVMC:PEAK:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Evmc.Peak.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: