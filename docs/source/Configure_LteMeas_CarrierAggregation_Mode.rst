Mode
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:CAGGregation:MODE:CSPath
	single: CONFigure:LTE:MEASurement<Instance>:CAGGregation:MODE

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:CAGGregation:MODE:CSPath
	CONFigure:LTE:MEASurement<Instance>:CAGGregation:MODE



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.CarrierAggregation.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: