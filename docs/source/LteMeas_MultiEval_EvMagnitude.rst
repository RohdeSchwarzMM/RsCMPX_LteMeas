EvMagnitude
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.EvMagnitude.EvMagnitudeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.evMagnitude.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_EvMagnitude_Average.rst
	LteMeas_MultiEval_EvMagnitude_Current.rst
	LteMeas_MultiEval_EvMagnitude_Maximum.rst
	LteMeas_MultiEval_EvMagnitude_Peak.rst