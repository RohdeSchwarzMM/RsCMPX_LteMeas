Ppower
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Modulation.Ppower.PpowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.listPy.modulation.ppower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_ListPy_Modulation_Ppower_Average.rst
	LteMeas_MultiEval_ListPy_Modulation_Ppower_Current.rst
	LteMeas_MultiEval_ListPy_Modulation_Ppower_Maximum.rst
	LteMeas_MultiEval_ListPy_Modulation_Ppower_Minimum.rst
	LteMeas_MultiEval_ListPy_Modulation_Ppower_StandardDev.rst