Pcc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:PMONitor:ULCA[:PCC]
	single: READ:LTE:MEASurement<Instance>:MEValuation:TRACe:PMONitor:ULCA[:PCC]

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:PMONitor:ULCA[:PCC]
	READ:LTE:MEASurement<Instance>:MEValuation:TRACe:PMONitor:ULCA[:PCC]



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Trace.Pmonitor.Ulca.Pcc.PccCls
	:members:
	:undoc-members:
	:noindex: