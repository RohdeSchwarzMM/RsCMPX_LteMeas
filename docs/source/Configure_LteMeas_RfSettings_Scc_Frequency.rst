Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:RFSettings:SCC<Nr>:FREQuency

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:RFSettings:SCC<Nr>:FREQuency



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.RfSettings.Scc.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: