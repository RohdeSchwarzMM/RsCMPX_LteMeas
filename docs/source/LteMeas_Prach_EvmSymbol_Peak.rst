Peak
----------------------------------------





.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.Prach.EvmSymbol.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.prach.evmSymbol.peak.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_Prach_EvmSymbol_Peak_Average.rst
	LteMeas_Prach_EvmSymbol_Peak_Current.rst
	LteMeas_Prach_EvmSymbol_Peak_Maximum.rst