Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PERRor:AVERage
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PERRor:AVERage
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:PERRor:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PERRor:AVERage
	FETCh:LTE:MEASurement<Instance>:MEValuation:PERRor:AVERage
	CALCulate:LTE:MEASurement<Instance>:MEValuation:PERRor:AVERage



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Perror.Average.AverageCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lteMeas.multiEval.perror.average.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LteMeas_MultiEval_Perror_Average_Nref.rst