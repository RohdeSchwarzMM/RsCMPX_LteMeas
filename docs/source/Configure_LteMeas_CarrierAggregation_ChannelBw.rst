ChannelBw
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:CAGGregation:CBANdwidth:AGGRegated

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:CAGGregation:CBANdwidth:AGGRegated



.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.CarrierAggregation.ChannelBw.ChannelBwCls
	:members:
	:undoc-members:
	:noindex: