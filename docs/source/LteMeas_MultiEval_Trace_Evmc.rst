Evmc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:TRACe:EVMC
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:EVMC

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:TRACe:EVMC
	FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:EVMC



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Trace.Evmc.EvmcCls
	:members:
	:undoc-members:
	:noindex: