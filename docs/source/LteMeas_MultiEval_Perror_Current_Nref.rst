Nref
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PERRor:CURRent:NREF
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PERRor:CURRent:NREF

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PERRor:CURRent:NREF
	FETCh:LTE:MEASurement<Instance>:MEValuation:PERRor:CURRent:NREF



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.Perror.Current.Nref.NrefCls
	:members:
	:undoc-members:
	:noindex: