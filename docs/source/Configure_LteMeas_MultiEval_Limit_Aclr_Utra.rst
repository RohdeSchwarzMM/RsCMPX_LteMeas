Utra<UtraAdjChannel>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Ch1 .. Ch2
	rc = driver.configure.lteMeas.multiEval.limit.aclr.utra.repcap_utraAdjChannel_get()
	driver.configure.lteMeas.multiEval.limit.aclr.utra.repcap_utraAdjChannel_set(repcap.UtraAdjChannel.Ch1)





.. autoclass:: RsCMPX_LteMeas.Implementations.Configure.LteMeas.MultiEval.Limit.Aclr.Utra.UtraCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.lteMeas.multiEval.limit.aclr.utra.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_LteMeas_MultiEval_Limit_Aclr_Utra_CarrierAggregation.rst
	Configure_LteMeas_MultiEval_Limit_Aclr_Utra_ChannelBw.rst