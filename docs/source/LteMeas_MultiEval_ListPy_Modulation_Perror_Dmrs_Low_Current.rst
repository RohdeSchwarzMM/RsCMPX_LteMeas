Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:DMRS:LOW:CURRent
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:DMRS:LOW:CURRent

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:DMRS:LOW:CURRent
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:DMRS:LOW:CURRent



.. autoclass:: RsCMPX_LteMeas.Implementations.LteMeas.MultiEval.ListPy.Modulation.Perror.Dmrs.Low.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: